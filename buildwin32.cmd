rem Dependency paths:
set CMAKE=C:\Program Files\CMake 2.6\bin\cmake.exe
rem used for headers only; we get the lib from the libpng build
rem set ZLIBDIR=c:\developer\lib\zlib
rem provides both png and zlib .libs
rem set PNGDIR=C:\developer\lib\lpng1220-vc\
set QTDIR=C:\Qt\4.8.6\
set QWT_ROOT=C:\qwt-5.2.3
set QT_PLUGIN_PATH="${QWT_ROOT}\designer\plugins\designer:$QT_PLUGIN_PATH"
rem set CAIRODIR=C:\developer\lib\cairo-1.6.0
rem set LCMSDIR=c:\developer\lib\lcms-1.16-vc
rem set JPEGDIR=c:\developer\lib\jpeg-6b-vc
rem set FREETYPEDIR=c:\developer\lib\freetype-2.3.5
rem set TIFFDIR=c:\developer\lib\tiff-3.8.2-vc
rem set PODOFODIR=c:\developer\podofo\050-vs-inst
rem set PODOFOSHARED=1
rem set LIBXMLDIR=c:\developer\lib\libxml2-2.6.30.win32
rem set SCRIBUSINSTALLDIR=c:\developer\scribus-bin
rem
rem
set PATH=C:\mingw32\bin;C:\Qt\4.8.6\bin;C:\Qt\4.8.6\plugins\designer;%PATH%
set 
rem set PATH=%SYSTEMROOT%;%SYSTEMROOT%\system32;%QTDIR%\bin
set INCLUDE=
set LIB=
rem set SCPNGPROJ=%PNGDIR%\projects\visualc71\
del cmakecache.txt
rem %CMAKE%" -G "MinGW Makefiles" -DCMAKE_INCLUDE_PATH:PATH=%QTDIR%\src\3rdparty\zlib;%QTDIR%\src\3rdparty\libpng;%ZLIBDIR%;%ZLIBDIR%\include;%PNGDIR%;%PNGDIR%\include;%CAIRODIR%;%CAIRODIR%\include;%LCMSDIR%\include;%JPEGDIR%;%JPEGDIR%\include;%TIFFDIR%\libtiff;%TIFFDIR%\include;%FREETYPEDIR%\include;%FREETYPEDIR%\include\freetype2;%PODOFODIR%\include;%LIBXMLDIR%\include -DCMAKE_LIBRARY_PATH:PATH=%ZLIBDIR%\lib;%SCPNGPROJ%\Win32_DLL_Debug\Zlib;%SCPNGPROJ%\Win32_DLL_Release\Zlib;%PNGDIR%\lib;%SCPNGPROJ%\Win32_DLL_Debug;%SCPNGPROJ%\Win32_DLL_Release;%LCMSDIR%\lib;%LCMSDIR%\Lib\MS;%LCMSDIR%\Lib\MS\dll;%JPEGDIR%;%JPEGDIR%\lib;%TIFFDIR%\libtiff;%TIFFDIR%\lib;%FREETYPEDIR%\objs;%FREETYPEDIR%\lib;%PODOFODIR%\lib;%LIBXMLDIR%\lib -DCMAKE_INSTALL_PREFIX=c:\