#!/bin/bash

#for i in $@ ; do

#FILES=*.svg
FILES=$@

for f in $FILES
do
  echo " file : $f"
  inkscape --without-gui --export-pdf="${f%.svg}.pdf" $f
done