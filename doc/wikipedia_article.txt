
=== MassChroQ ===
MassChroQ
<ref>
{{cite journal |last1=Valot |first1=Benoît |last2=Langella |first2=Olivier |last3=Nano |first3=Edlira |last4=Zivy |first4=Michel
 |title=MassChroQ: A versatile tool for mass spectrometry quantification |journal=PROTEOMICS |volume=11 |issue=17 |year=2011 |pmid=21751374 |doi=10.1002/pmic.201100120 |pages=3572–3577}}</ref>
 </ref>
 is a [[free software]] for peptide quantification developed by [http://pappso.inra.fr/ PAPPSO] (Gif sur Yvette, France).

 The software is written in C++ and released under the [[GNU General Public License]].

It allows the analysis of [[Label-free quantification|label free]] or various [[isotopic labeling]] methods ([[Stable isotope labeling by amino acids in cell culture|SILAC]], ICAT, N-15, C-13 ...), works with high and low resolution spectrometer systems, supports complex data treatments as peptide or protein fractionation prior to LC-MS analysis (SCX, SDS-PAGE, etc.).
