#!/usr/bin/python

import argparse
import sys
import os

#Defined command line
desc = 'Splits a MassChroQml file into small pieces in a subdirectory.'
command = argparse.ArgumentParser(prog='mcq-split', \
    description=desc, usage='%(prog)s [options]')
command.add_argument('-i', '--infile', \
    type=argparse.FileType("r"), \
    help='Open a MassChroQml file', required=True)
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${MASSCHROQ_VERSION}')

#Read arguments of command line
args = command.parse_args()
sys.stderr.write("input file is :'" + args.infile.name + "'\n")

mcqmlFile = args.infile
mcqmlSplitDir = args.infile.name + '.d'

if not os.path.exists(mcqmlSplitDir) :
    os.mkdir(mcqmlSplitDir)

os.system('xmlstarlet sel -t -c "/masschroq/rawdata" '+mcqmlFile.name+' > '+mcqmlSplitDir+'/rawdata.xml')

#masschroq/groups
os.system('xmlstarlet sel -t -c "/masschroq/groups" '+mcqmlFile.name+' > '+mcqmlSplitDir+'/groups.xml')

#masschroq/align
os.system('xmlstarlet sel -t -c "/masschroq/align" '+mcqmlFile.name+' > '+mcqmlSplitDir+'/align.xml')

#masschroq/quantification_methods
os.system('xmlstarlet sel -t -c "/masschroq/quantification_methods" '+mcqmlFile.name+' > '+mcqmlSplitDir+'/quantification_methods.xml')

#masschroq/quantification/quantification_results
os.system('xmlstarlet sel -t -c "/masschroq/quantification/quantification_results" '+mcqmlFile.name+' > '+mcqmlSplitDir+'/quantification_results.xml')

#masschroq/quantification/quantification_traces
os.system('xmlstarlet sel -t -c "/masschroq/quantification/quantification_traces" '+mcqmlFile.name+' > '+mcqmlSplitDir+'/quantification_traces.xml')
