#!/usr/bin/perl -w

# This script converts a large .gnumeric file (more than 65000 lines so 
# gnumeric cannot read them) produced by MassChroQ into two tabulated 
# text files : one for the peptides (named filename.pep), one for the proteins 
# (named filename.prot). 
# Contact in case of problem : edlira.nano[at]moulon.inra.fr


use strict;

die "Usage: gnm2txt <file>" if @ARGV != 1;
my $filename = shift @ARGV;
open my $file, "<$filename" or die "Can't open $filename: $!";

sub parse {
  my ($suffix, $cols)  = @_;

  open my $out_file,
    ">$filename.$suffix" or die "Can't write $filename.$suffix: $!";
  
  while (<$file>) {
    last if /gnm:Cells/;
  }

  while (<$file>) {
      last if /gnm:Cells/;
      if (/Col="(\d+)".*>([^<]*)</) {
	  if ($2 eq "") {
	      print $out_file "";
	  } else {
	      print $out_file "$2";
	  }
	  if ($1 != $cols) {
	      print $out_file "\t";
	  } else {
	      print $out_file "\n";
	  }
      }
  }
  close $out_file;
}

parse ("pep", 12);
parse ("prot", 2);
