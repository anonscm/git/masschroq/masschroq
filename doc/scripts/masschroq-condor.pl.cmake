#!/usr/bin/perl -w

# This script creates a masschroq condor job submit file
# condor_submit_masschroq.txt. It takes the same arguments than
# masschroq and puts them in this condor submit file as
# arguments to the masschroq job to be launched. It also creates a
# temporary directory for where condor will put all
# the log, out err ... output files for this job.

use strict;
use warnings;
use File::Temp qw/ tempdir /;
use Getopt::Long;

display_help() if ( @ARGV < 1
	or $ARGV[0] =~ m/^(-h|--help)$/ );

my %mcq_param = ();
$mcq_param{temp}          = '/tmp';
$mcq_param{file}          = pop @ARGV;
$mcq_param{memory}        = 6;
$mcq_param{parse_peptide} = 0;
$mcq_param{cpu}           = 8;
$mcq_param{ondisk}        = 0;

print $mcq_param{file};

#options of the program
GetOptions(
	"mem:i"          => \$mcq_param{memory},
	"cpus:i"         => \$mcq_param{cpu},
	"parse-peptides" => \$mcq_param{parse_peptide},
	"ondisk" => \$mcq_param{ondisk}
  )
  or die("Error in command line arguments\n");

my ( $tempdir_location, $tempdir_template, $tempdir );

# temporary directory filename (by default it is the system's one)
$tempdir_location = "/gorgone/pappso/tmp";

# template name for the temporary directories we will create
$tempdir_template = "temp_masschroq_condor_jobXXXX";

# create a temporary directory in the choosen temporary directory
$tempdir = tempdir( $tempdir_template, DIR => $tempdir_location );

#create condor file in current directory
open my $condor_file, ">" . $tempdir . "/submit_condor_masschroq.txt"
  or die "Can't write condor_submit.txt: $!";

print $condor_file "Universe   = vanilla\n";
print $condor_file "notification   = Always\n";

print $condor_file
  "Executable = @CMAKE_INSTALL_PREFIX@/bin/masschroq\n";

print $condor_file "Log        = $tempdir/submit_condor.log\n";

print $condor_file "Output     = $tempdir/masschroq.out\n";

print $condor_file "Error      = $tempdir/masschroq.error\n";

print $condor_file "request_cpus = $mcq_param{cpu}\n";
print $condor_file "request_memory = $mcq_param{memory}G\n";

print $condor_file "\n";

print $condor_file "Arguments  = -c $mcq_param{cpu} -t /tmp ";
if ( $mcq_param{parse_peptide} ) {
	print $condor_file " -p";
}
if ( $mcq_param{ondisk} ) {
	print $condor_file " --ondisk";
}
print $condor_file " $mcq_param{file}";
print $condor_file "\n";

print $condor_file "Queue\n";

close $condor_file;

print "masschroq condor submission file submit_condor_masschroq.txt created\n";
print "condor job temporary directory $tempdir created\n";

print "Starting masschroq in condor\n";

system( "condor_submit " . $tempdir . "/submit_condor_masschroq.txt" );

#system( "cat " . $tempdir . "/submit_condor_masschroq.txt" );

#eval "system( "condor_submit " . $tempdir . "/submit_condor_masschroq.txt" );

# help function
#    print "  -t, --tmpdir DIRECTORY\tuse DIRECTORY as temporary working direcory for masschroq : temporary files generated by masschroq during execution time (one file or several slices at a time per xml data file being analysed) will go there. By default DIRECTORY is the current working directory of masschroq. Use this option if you want to set another one.\n";

sub display_help {
	print "Unknown option: @_\n" if (@_);
	print "Usage: masschroq-condor.pl [OPTION] [FILE]\n";
	print
"Perform Mass Chromatogram Quantification as indicated in XML input FILE via masschroq on a condor system.\n";
	print "Options:\n";
	print "  -m, --mem\t\t\trequired memory in gigabytes\n";
	print "  -c, --cpus\t\t\trequired number of cpus\n";
	print "  -h, --help\t\t\tdisplay this help and exit\n";
	print
"  -p, --parse-peptides\t\tperform peptide text file parsing only, no quantification. If the identified peptides are given to masschroq via peptide text files (defined in FILE), he parses them, creates a new file called parsed-peptides_FILE which contains the FILE content (minus the lines relative to thr peptide text files to parse) plus the parsed peptides information. By default masschroq continues analysis and quantification on this new file. Use this option if you want masschroq only to parse the petide files but not to continue quantification on them. This allows you to check the parsed peptides and to add trace information on specific peptides before quantification.\n";
	print "  --ondisk\t\tdump unmatched peak collections to the temporary directory to save memory.\n";
    print "For additional information, see the MassChroQ Homepage :\n";
	print "http://pappso.inra.fr/bioinfo/masschroq/\n";
	exit;
}
