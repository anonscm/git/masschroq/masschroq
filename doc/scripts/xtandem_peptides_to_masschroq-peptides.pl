#!/usr/bin/perl -w

#Prend le fichier peptide.txt produit par le pipeline X!Tandem
#renvoie la peptide liste comme fichier d'entrée de MassChroQ

use strict;
use File::Basename;
use utf8;

use Wx;
use Wx qw(:filedialog wxID_CANCEL);


my $file;
my $line;
my $dirin         = "";
my $dirout        = "";
my $peptidefile    = "";
my @elementline = ();
my %msrunliste = ();



#prend les erreurs de sortie et les envoie dans la fenetre courante
eval {
	print STDOUT "Transform peptide result from  : Version 0.01\n";
	print STDOUT "Export peptide list for MassChroQ\n\n";

#### Aller chercher des fichiers######################
	my ($this) = shift;
	my $filemzxmldialog = Wx::FileDialog->new(
		$this, "Select peptide_txt file to analyse",
		'', '',
		"txt files (*.txt)|*.txt|tsv files (*.tsv)|*.tsv|All files (*.*)|*.*",
		wxOPEN | wxMULTIPLE
	);
	( $filemzxmldialog->ShowModal != wxID_CANCEL ) || die("Analysis canceled");
	$peptidefile = $filemzxmldialog->GetPath;
	my ( $filename, $dirname ) =
	  &File::Basename::fileparse( $filemzxmldialog->GetPath );
	$dirin = $dirname;
	$filemzxmldialog->Destroy;

#### Aller chercher un dossier de sortie ######################
	my $savedirdialog = Wx::DirDialog->new(
	    $this,    # Parent du Dialog, ici la Frame
	    "Select the folder were to save the results",
		$dirin,
		wxOPEN
	);
	( $savedirdialog->ShowModal != wxID_CANCEL ) || die("Analysis canceled");
	$dirout = $savedirdialog->GetPath;
	$dirout .= "/";
	$savedirdialog->Destroy;
###########################
	print STDOUT "Getting the sample names\n";
	
	open INFILE, '<' . $peptidefile;
	$line = <INFILE>;
	while(defined($line = <INFILE>)){
	    @elementline = &parse($line);
	    if( (@elementline>4) and ($elementline[2] ne "Sample")) {
		$msrunliste{$elementline[2]}="ok";
	    }
	}	
	close INFILE;
	foreach my $msrun ( sort { $a cmp $b } ( keys %msrunliste ) ){
	    print STDOUT $msrun ."\n";
	    open INFILE, '<' . $peptidefile;
	    open OUTFILE, '>'.$dirout.$msrun."_peptide_list.txt";
	    print OUTFILE "scan"."\t"."sequence"."\t"."mh"."\t"."z"."\t"."protein"."\n"; #scan sequence mh z protein
	    while(defined($line = <INFILE>)){
		@elementline = &parse($line);
		if((@elementline > 15) and ($elementline[2] eq $msrun)
		and ($elementline[7] eq "yes")){
		    my @protein = &parse_prot($elementline[10]);
		    foreach my $prot (@protein){
			print OUTFILE $elementline[3]."\t".$elementline[5]."\t".$elementline[14]."\t".$elementline[12]."\t".$prot."\n";
		    }
		}
	    }
	    
	    
	    close INFILE;
	    close OUTFILE;
	}
	

##choix du models

	print STDOUT "\nAnalysis finished.\n";
	#<STDIN>;
};

if ($@) {
	my $error = $@;
	$error =~ s/at .*\.$//s;
	print STDOUT $error;
	#<STDIN>;
}

#sous-routine qui parse un fichier txt tabule
sub parse() {
	my ($string) = @_;
	chomp $string;
	my @strings = split( /\t/, $string);
	return (@strings);
	<STDIN>;
}

sub parse_prot() {
	my ($string) = @_;
	chomp $string;
	my @strings = split( /\s/, $string);
	return (@strings);
	<STDIN>;
}
