\documentclass{beamer}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{default}

\mode<presentation>

\usepackage{listings}
\usepackage{amsmath}
\usepackage{enumerate}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{graphicx}

\title[PAPPSO]{MassChroQ - Mass Chromatogram Quantification}
\subtitle{Logiciel de quantification par spectrométrie de masse} 
\author{Edlira Nano\\ \small{Plateforme d'Analyse Protéomique de Paris Sud-Ouest}}
\institute{\includegraphics[width=0.2\textwidth]{images/masschroq.pdf}}
\date[]{Rencontres bioinformaticiens et statisticiens de l'INRA\\ 
  \small{24 Mars 2011}}


\usecolortheme{lily}
\usetheme[width=0pt]{Goettingen}
\useinnertheme[shadow]{rounded}
\setbeamercovered{transparent=30}
\setbeamercolor{alerted text}{fg=brown!70!black}
\setbeamercolor{block title}{bg=structure!15!white}
\setbeamercolor{title}{bg=structure!10!white}
\setbeamercolor{block title alerted}{bg=brown!20!white}
\setbeamercolor{block body alerted}{bg=brown!10!white}
\setbeamertemplate{background canvas}[vertical shading]%
[top=structure!20,middle=white,bottom=white,midpoint=0.8]
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
\color{structure!50!white}
\hspace{0.1cm}\includegraphics[width=0.1\textwidth]{images/pappso.pdf}
\hfill 
%\insertframenumber/\inserttotalframenumber \hfill 
\includegraphics[width=0.1\textwidth]{images/moulon.pdf} \hspace{0.1cm}}

\setbeamercolor{postit}{fg=structure!50!black,bg=structure!20!white}

\setbeamertemplate{headline}{
\begin{beamercolorbox}[ht=2.5ex,dp=1ex,wd=\paperwidth]{postit}
\hspace{0.2cm} \insertsection
\hfill \insertsubsection \hspace{0.2cm}
\end{beamercolorbox}
}

%\setbeamertemplate{footline}{
%  \hfill \insertframenumber/\inserttotalframenumber}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Plan}
\tableofcontents[section]
\end{frame}

\section{Problématique}
\subsection{Contexte}

\begin{frame}
  \frametitle{En protéomique :}
  \begin{block}{}
    \begin{itemize}[<+->]
    \item Besoin d'identifier et de quantifier les protéines et les
      peptides contenus dans les échantillons.
    \item Pour ceci on utilise la \textit{spectrométrie de masse} (MS) :
      technique physique d'analyse permettant de mesurer le rapport
      masse sur charge des molécules d'un composé.
    \item Mais aussi la \textit{chromatographie liquide} (LC) :
      technique chimique d'analyse permettant de séparer les molécules
      d'un composé.
    \item Ces deux techniques ont une utilité à la fois qualitative
      (identification de molécules)  et quantitative (intensité de présence). 
    \item L'utilisation de la MS en couple avec une
      phase préalable de LC (LC-MS) permet d'identifier et de
      quantifier finement les peptides et les protéines d'un composé.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{LC-MS/MS}
  %\begin{block}{}
    \begin{itemize}[<+->]
      \small 
    \item MS : séparation des peptides en fonction de leur rapport 
      masse/charge $\Rightarrow$ \textbf{spectres de masse} (intensité en fonction de masse/charge).
    \item MS/MS : sélection de peptides au cours de la MS et
      fragmentation $\Rightarrow$ permet leur identification.
    \item LC : séparation des peptides au cours du temps
      $\Rightarrow$ chromatogrammes (intensité en
      fonction du temps).  
    \item LC-MS/MS $\Rightarrow$ spectres MS plus pour chaque peptide
      sélectionné en MS/MS des chromatogrammes.
    \end{itemize}
  %\end{block}
  \only<1-4>{\visible<2-4>{
      \center \includegraphics[height=0.5\textheight]{images/ms1_ms2.pdf}\\
    }}
\end{frame}

\subsection{Besoins}

\begin{frame}
\frametitle{Problématique}
\begin{alertblock}{But}
\center
Associer à nos peptides identifiés une valeur quantitative déterminée
en MS.
\end{alertblock}
\begin{block}<2>{Contraintes}
\begin{itemize}
\item Développement continuel des méthodes de quantification
par spectrométrie de masse.
\item Augmentation significative de la quantité et de la complexité des
données produites.
\item Indispensable de disposer d'outils informatiques pour analyser
automatiquement les données afin d'en permettre l'exploitation.
\end{itemize}
\end{block}
\end{frame}

\subsection{Pourquoi développer MassChroQ?}
\begin{frame}
\frametitle{Autres logiciels de quantification}
  De nombreux logiciels sont disponibles, mais présentent :

\begin{block}{des lacunes biologiques :}
\begin{itemize}[<+->]
\small 
\item Spécifiques au matériel utilisé : spectromètres de faible
  résolution (LR) ou de haute résolution (HR).
\item Spécifiques au type de données quantifiées : avec ou sans
  marquage isotopique.
\item Peu/pas de prise en compte du dispositif expérimental
  (par exemple pré-fractionnements).
\end{itemize}
\end{block}

\begin{block}{des lacunes informatiques :}
\begin{itemize}[<+->]
\small
\item Peu/pas paramétrables.
\item Dépendants de la plateforme informatique: du système
  d'exploitation, de l'interface graphique ou de formats de données
  fermés non transparents.
\item Peu/pas intégrables dans des pipelines.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
  \frametitle{}
\begin{block}{Notre objectif}
  \begin{itemize}[<+->]
  \item Quantifier des données HR aussi bien que LR.
  \item Quantifier des données sans marquage aussi bien qu'avec marquage
    isotopique.
  \item Gérer de grandes quantités de données rapidement et automatiquement.
  \item Être aussi indépendants que possible des dispositifs
    expérimentaux tout en prenant en compte leurs spécificités.
  \item Paramétrabilité et traçabilité.
  \end{itemize}
\end{block}
\end{frame}


\section{MassChroQ version 1.0}
\subsection{Historique}
\begin{frame}
\frametitle{\small Historique}
\begin{block}<1->{\small 2006}
  \small Premiers tests de quantification sans marquage.
  Extraction des valeurs quantitatives manuellement.
\end{block}

\begin{block}<2->{\small 2007 - 2008}
\small Échantillons plus complexes. Automatisation par des scripts
Perl (B. Valot). 
\end{block}

\begin{block}<3->{\small 2009}

\small Besoin de rapidité et d'ajout de fonctionnalités. Traduction
des scripts Perl en $\verb!C++!$ : QuantiMsCpp (O. Langella et B.Valot).
\end{block}

\begin{block}<4->{\small 2010}
\small
QuantiMsCpp deviens MassChroQ. Première version de release 1.0 prête.
(O. Langella, B. Valot et E.Nano). 
\end{block}
\end{frame}

\subsection{Fonctionnement de MassChroQ}

\begin{frame}
  \frametitle{Fonctionnalités de MassChroQ}
\begin{block}{}
\begin{itemize}
\item Parsing des échantillons LC-MS/MS sous formats ouverts standards : mzXML
  ou mzML. 
\item Définition des entités à quantifier : peptides identifiés,
  isotopes ou liste de masses.
\item Alignement des échantillons LC-MS/MS semblables si nécessaire.
\item Traitement du signal : filtrage du bruit de fond, du bruit de
  la ligne de base, élimination de spikes.
\item Détection puis quantification des entités définies.
\item Export des résultats sous divers formats ouverts (tsv, gnumeric,
  xml ou xhtmltable) prêts à être exploités statistiquement.
\end{itemize}  
\end{block}
\end{frame}

\subsection{Peptides et protéines identifiés}
  \begin{frame}
  \frametitle{Identification de peptides/protéines}
\begin{block}{}
\begin{itemize}[<+->]
\item Les peptides présents dans les échantillons sont identifiés via
  des logiciels d'identification (X!Tandem, Mascot, ...).
\item Les résultats d'identification sont automatiquement intégrables 
dans MassChroQ de deux façons :
\begin{itemize}
\item directement en utilisant notre X!Tandem pipeline;
\item à partir de fichiers tsv/csv (produits par la plupart des
  logiciels d'identification).
\end{itemize}  
\end{itemize}  
\end{block}
\end{frame}

\subsection{Détection et quantification}
\begin{frame}
  \frametitle{Détection et quantification des pics sur les XICs}
  \begin{block}{\small eXtracted Ion Chromatogram (XIC)}
    \begin{itemize}
    \small
    \item A partir des données MS, on extrait les intensités des m/z
      d'intérêt au cours du temps chromatographique.
    \item Filtrage des XICs.
    \end{itemize}
  \end{block}
  \begin{columns}[c]
  \column{0.45\textwidth}
  \begin{center}
    \includegraphics[height=0.6\textheight]{images/decalages.pdf}
  \end{center}
  \column{0.45\textwidth}
  \begin{itemize}
  \small
  \item Détection des pics d'intensité.
  \item Calcul de l'aire sous les pics $\Rightarrow$ valeur quantitative.
  \item La LC génère des décalages de temps de rétention : besoin
    d'alignement des runs LC-MS pour éviter les biais expérimentaux.  
  \end{itemize}
\end{columns}
\end{frame}

\subsection{Groupes et alignement}
\begin{frame}
   \frametitle{\small Groupes de runs}
   \small
   L'utilisateur regroupe des runs LC-MS techniquement similaires pour
   pouvoir les traiter ensemble et de façon spécifique.
   \begin{exampleblock}<2->{Ex : pré-fractionnement SDS PAGE des échantillons}
     \small
     Seuls les runs de la même bande doivent être alignés entre eux.
   \end{exampleblock}
   \begin{columns}[c]
     \column{0.4\textwidth}
     \begin{center}
       \only<1,2,4>{\visible<2,4>{
       \includegraphics[height=0.5\textheight]{images/sds.pdf}
     }} \only<3>{
     \includegraphics[height=0.5\textheight]{images/decalages.pdf}
   }
     \end{center}   
     \column{0.4\textwidth}
     \small
     \begin{itemize}
     \small
     \item<2-> On aligne ensemble les runs d'un même groupe.
     \item<3-> Accès à l'intensité d'un peptide même s'il n'a pas été identifié dans un run.
     \item<4-> Chaque groupe peut avoir des paramètres
       d'alignement différents.
     \end{itemize}
   \end{columns}
\end{frame}

\begin{frame}
\frametitle{Deux méthodes d'alignements :}
\begin{block}{}
\begin{description}[MS/MS]
\item[MS] Méthodes dérivées des analyses de gel 2D ou\\
de \alert{traces chromatographiques}\\
$\Rightarrow$ intégration du logiciel \textit{open-source}
\href{http://obi-warp.sourceforge.net/}{\beamerbutton{OBI-Warp}}
\item[MS/MS] Méthodes utilisant les résultats d'identification\\
$\Rightarrow$ algorithme développé en interne
\end{description}
\end{block}
\begin{columns}[c]
\column{0.6\textwidth}
\center
\includegraphics[height=0.6\textheight]{images/figure1A.pdf}
\column{0.4\textwidth}
\small Exemple d'alignement
\end{columns}
\end{frame}

\subsection{masschroqML}
\begin{frame}
  \frametitle{masschroqML}
  \begin{block}<1->{En entrée}
    Format XML du fichier d'entrée de MassChroQ. On y définit :
    \begin{itemize}
    \item toutes les instructions d'analyse souhaitées;
    \item les différents paramètres d'alignement et de détection des pics;
    \item les formats des résultats et les traces souhaitées.
    \end{itemize}
  \end{block}
  \begin{alertblock}<2>{En sortie}
    \begin{itemize}
    \item Les résultats contiennent une valeur quantitative associée à
      chaque peptide, dans chaque échantillon. 
    \item Les formats résultats disponibles sont : tsv, gnumeric,
      et xhtmltable (pour exploitation directe) et masschroqML (pour
      intégration dans des bases de données telle que
      \href{http://moulon.inra.fr/index.php/fr/equipestransversales/atelier-de-bioinformatique/projects/77}{\beamerbutton{PROTICdb}}).
    \end{itemize}
  \end{alertblock}
  \end{frame}

\section{Applications et développement}
\subsection{Applications}
\begin{frame}
\frametitle{Résultats}
\begin{block}<1->{Publication soumise}
 Analyse LC-MS/MS en HR et LR d'un protéome complexe avec
injection croissante de protéine BSA (6 répétitions techniques HR et 6
LR). A montré :
\begin{itemize}
\item la reproductibilité de la quantification et de la détection (CV
inférieurs à 1.4\% en LR et à 1.3\% en HR), 
\item des données quantitatives HR et LR très similaires, 
\item une amélioration significative des résultats quantitatifs apportée par
  l'alignement.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
  \frametitle{Résultats}
  \begin{block}<1>{Projet HeterosYeast : Mélisande Blein}
    \begin{itemize}
      \small
    \item Traité :
      \begin{itemize}
      \item Analyse de 17 souches de levure de
        \textit{S. Cerevisiae} et \textit{S. Uvarum} : étude protéomique
        de la fermentation chez les hybrides.
      \item Total de 52 échantillons analysés en LC-MS/MS HR.  
      \item 250 Go de fichiers mzXML traités par MassChroQ.
      \item Temps d'analyse MassChroQ : 30 minutes.
      \item Peptides quantifiés : $\simeq7000$.
      \end{itemize}
    \item À venir :
      \begin{itemize}
      \item Analyse sur des hybrides de 12 souches.
      \item 450 échantillons en LC-MS/MS HR.
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Résultats}
  \begin{block}<1>{Projet Dromadair : Ludovic Bonhomme}
    \begin{itemize}
    \item Analyse de 40 injections d'une même lignée
      de mais chacune pré-fractionnée en 10 par SCX-IMAC selon 10
      régimes hydriques différents pour une étude de la cinétique
      de phosphorylation des protéines durant un laps de temps de
      quelques minutes.
    \item Marquage isotopique par dimethylation.
    \item Total de 400 échantillons analysés en LC-MS$^3$ LR.
    \item 450 Go de fichiers mzXML traités.
    \item Temps d'analyse MassChroQ : $\simeq48$h.
    \item Peptides phosphorylés quantifiés : $\simeq4000$.
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Développement}
\begin{frame}
  \frametitle{Implémentation}
\begin{block}{Modularité}
  \begin{itemize}[<+->]
\item MassChroQ est écrit en $\verb!C++!$ et utilise la bibliothèque
  Qt 
  \begin{itemize}
  \item gestion fine de la mémoire, modularité et portabilité.
  \end{itemize}
\item C'est un logiciel indépendant en ligne de commande et aussi
  une librairie directement intégrable dans des pipelines protéomiques
  connues comme la TPP ou la TOPP.
\item Conçu pour une intégration immédiate de nouvelles
  fonctionnalités ou de librairies externes :
\begin{itemize}
\item intégration de la librairie externe d'alignement OBI-Warp et
  implémentation de l'algorithme interne d'alignement MS/MS. 
\end{itemize}  
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
  \frametitle{Implémentation}
\begin{block}<1->{Ouverture}
\begin{itemize}
\item MassChroQ et son code source seront (très prochainement)
  diffusés sous licence libre GPL version 3.
\item Il est disponible pour Windows et Linux.
\item Il utilise et produit uniquement des données en formats
  ouverts standards.
\end{itemize}  
\end{block}
\begin{block}<2>{Transparence}
  \begin{itemize}
  \item MassChroQ est entièrement paramétrable via un fichier d'entrée XML.
  \item Chaque étape de son traitement est entièrement traçable
    (alignement, XICs, filtrage, détection de pics).
  \item Une documentation complète, des exemples d'analyses courantes
    prêts à l'emploi, un dépôt subversion, une gestion de bugs et des
    forums utilisateur sont disponibles (SourceSup).
  \end{itemize}  
\end{block}

\end{frame}

\subsection{Pour conclure} 

\begin{frame}
\frametitle{Futurs développements en 2011}
\begin{itemize}
\item Gestion plus fine de la mémoire vive.
\item Gestion des analyses SRM.
\item Nouvelle version avec interface graphique complète.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{}
\begin{block}{Remerciements}
  \begin{itemize}
  \item Olivier Langella : conception et réalisation du logiciel, modularité,
    ouverture et direction de mes travaux.   
  \item Benoît Valot : création et conception du logiciel, premier
    utilisateur, premier dénicheur de bugs, éternel exigeant de
    fonctionnalités.
\item L'équipe PAPPSO : Michel Zivy notre directeur et redoutable
  concepteur d'algorithmes; Mélisande Blein et Ludovic Bonhomme les
  premiers et si indispensables cobayes de MassChroQ.
\item SourceSup : service de gestion de projets du Comité Réseau des
  Universités, pour l'hébérgement du dépôt subversion, du gestionnaire
  de bugs et des forums de MassChroQ ainsi que pour leur réactivité. 
\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\frametitle{}
\begin{block}{Liens}
  \begin{description}
  \item[Page principale de MassChroQ] \hfill \\
    \url{http://pappso.inra.fr/bioinfo/masschroq/}
  \item[Page du logiciel sur SourceSup] \hfill \\
    \url{http://sourcesup.cru.fr/projects/masschroq/}
  \item[Auteurs de MassChroQ]  \hfill \\
    O. Langella : \url{olivier.langella@moulon.inra.fr}\\
    B. Valot : \url{benoit.valot@moulon.inra.fr} \\
    E. Nano : \url{edlira.nano@moulon.inra.fr} \\
    M. Zivy : \url{michel.zivy@moulon.inra.fr}.
  \end{description}
\end{block}

\begin{alertblock}{}
  Ce document est distribué sous licence
  \href{http://creativecommons.org/licenses/by-nc-nd/2.0/fr/}
  {Creative Commons CC-BY-NC-ND 2.0 France}.
\end{alertblock}
\end{frame}

\end{document}
