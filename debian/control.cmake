Source: masschroq
Section: science
Priority: optional
Maintainer: Olivier Langella <olivier.langella@u-psud.fr>
Uploaders: Olivier Langella <olivier.langella@u-psud.fr>
DM-Upload-Allowed: yes
Standards-Version: 3.9.1.0
Build-Depends: debhelper (>= 5),
               cmake (>= 2.6),
               libx11-dev,
               libc6-dev,
               texlive-latex-extra,
               texlive-latex-recommended,
               texlive-fonts-recommended,
               texlive-science,
               hevea,
               inkscape,
               doxygen,
               libpappsomspp-dev (= @LIBPAPPSOMSPP_VERSION@),
               libpappsomspp-widget-dev (= @LIBPAPPSOMSPP_VERSION@),
               libpappsomspp-widget0 (= @LIBPAPPSOMSPP_VERSION@),
               libodsstream-qt5-dev (>= 0.7.3),
               qtbase5-dev,
               libqcustomplot-dev,
               libqt5xmlpatterns5-dev
Build-Conflicts: qt3-dev-tools
Homepage: http://pappso.inra.fr/bioinfo/masschroq/



Package: masschroq-common
Architecture: linux-any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqt5core5a,
         libqt5concurrent5,
         libpappsomspp0  (= @LIBPAPPSOMSPP_VERSION@),
         libodsstream-qt5-0 (>= 0.7.3)
Replaces: masschroq (<< 1.5.0)
Conflicts: masschroq (<< 1.5.0)
Suggests: masschroq, masschroq-gui, masschroq-studio
Description: Mass Chromatogram Quantification shared libraries
 MassChroQ (Mass Chromatogram Quantification) software performs quantification 
 of data obtained from LC-MS (Liquid Chromatography - Mass Spectrometry) techniques. 
 In particular it performs : retention-time alignment of runs, XIC extraction and filtering, 
 peak detection and quantification.



Package: masschroq-doc
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: masschroq,
          masschroq-gui,
          masschroq-studio
Description: Mass Chromatogram Quantification documentation
 Installs PDF documentation for MassChroQ in /usr/share/doc/masschroq



Package: masschroq
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         masschroq-common (= ${binary:Version}),
         libqt5core5a,
         libqt5xml5
Suggests: xtpcpp,
          masschroq-doc,
          masschroq-gui,
          masschroq-studio
Description: Mass Chromatogram Quantification CLI (Command Line Interface)
 MassChroQ (Mass Chromatogram Quantification) software performs quantification 
 of data obtained from LC-MS (Liquid Chromatography - Mass Spectrometry) techniques. 
 In particular it performs : retention-time alignment of runs, XIC extraction and filtering, 
 peak detection and quantification.



Package: masschroq-gui
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         masschroq-common (= ${binary:Version}),
         libqt5core5a,
         libqt5gui5,
         libqt5concurrent5,
         libqt5network5
Suggests: masschroq-doc, masschroq-studio
Description: Mass Chromatogram Quantification graphical user interface
 MassChroQ (Mass Chromatogram Quantification) software performs quantification 
 of data obtained from LC-MS (Liquid Chromatography - Mass Spectrometry) techniques. 
 In particular it performs : retention-time alignment of runs, XIC extraction and filtering, 
 peak detection and quantification.



Package: masschroq-studio
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         masschroq-common (= ${binary:Version}),
         libpappsomspp-widget0 (= @LIBPAPPSOMSPP_VERSION@),
         libqt5core5a,
         libqt5gui5,
         libqt5xml5
Suggests: masschroq-doc, masschroq-gui
Description: Mass Chromatogram Quantification graphical user interface to edit parameters
 MassChroQ (Mass Chromatogram Quantification) software performs quantification 
 of data obtained from LC-MS (Liquid Chromatography - Mass Spectrometry) techniques. 
 In particular it performs : retention-time alignment of runs, XIC extraction and filtering, 
 peak detection and quantification.


Package: masschroq-condor
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         masschroq,
         htcondor
Suggests: masschroq-doc,
          masschroq-gui,
          masschroq-studio
Description: Perl script to launch MassChroQ via condor job queue
 Perl script to launch MassChroQ via condor job queue
