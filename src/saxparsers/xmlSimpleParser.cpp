/**
 * \file xmlSimpleParser.cpp
 * \date November 23, 2010
 * \author Edlira Nano
 */

#include "xmlSimpleParser.h"
#include <QDebug>
#include <pappsomspp/utils.h>

XmlSimpleParserHandlerInterface::XmlSimpleParserHandlerInterface(Msrun *p_msrun)
{
  _p_msrun = p_msrun;
}
bool
XmlSimpleParserHandlerInterface::needPeakList() const
{
  return false;
}
void
XmlSimpleParserHandlerInterface::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qspectrum)
{

  if(qspectrum.getMsLevel() == 1)
    {
      // else if (qName == "peaks" && _ms_level == 1 && _scan_type == "Full") {
      _p_msrun->addOriginalRetentionTime(qspectrum.getRtInSeconds());
    }
  if(qspectrum.getMsLevel() > 1)
    {

      /// creating the new precursor object and setting its parameters
      Precursor *precursor =
        new Precursor(qspectrum.getPrecursorSpectrumIndex(),
                      qspectrum.getRtInSeconds(),
                      qspectrum.getPrecursorIntensity(),
                      qspectrum.getPrecursorMz());

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " scan idx="
               << qspectrum.getMassSpectrumId().getSpectrumIndex()
               << " precursor_idx=" << qspectrum.getPrecursorSpectrumIndex();
      /// adding this new precursor to the msrun's hash map of scan_num ->
      /// precursor
      _p_msrun->mapPrecursor(pappso::Utils::extractScanNumberFromMzmlNativeId(
                               qspectrum.getMassSpectrumId().getNativeId()),
                             precursor);
    }
}
