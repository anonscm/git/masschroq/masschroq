/**
 * \file masschroqmlParser.cpp
 * \date September 21, 2009
 * \author Olivier Langella
 */

#include "masschroqmlParser.h"
#include "../lib/alignments/alignment_ms2.h"
#include "../lib/alignments/alignment_obiwarp.h"
#include "../lib/alignments/monitors/monitor_alignment_time.h"
#include "../lib/msrun/msrun.h"
//#include "../lib/xicExtractionMethods/xicExtractionMethodMzRange.h"
//#include "../lib/xicExtractionMethods/xicExtractionMethodPpmRange.h"
#include "../lib/consoleout.h"
#include "../lib/monitors/masschroqWriter.h"
#include "../lib/monitors/monitorcomparoutput.h"
#include "../lib/monitors/monitorodsoutput.h"
#include <odsstream/tsvdirectorywriter.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/utils.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/processing/filters/filterinterface.h>
#include <pappsomspp/processing/filters/filtermorpho.h>
#include <pappsomspp/processing/detection/tracedetectionzivy.h>
#include <pappsomspp/processing/detection/tracedetectionmoulon.h>


MasschroqmlParser::MasschroqmlParser(MassChroq *p_my_chroq)
{

  _p_my_chroq = p_my_chroq;

  _p_current_alignment_method      = 0;
  _p_current_quantification_method = 0;
  _read_time_values                = false;

  _p_current_monitor_speed_list = new MonitorSpeedList();
}

MasschroqmlParser::~MasschroqmlParser()
{

  delete _p_current_monitor_speed_list;
}

bool
MasschroqmlParser::startElement(const QString &namespaceURI,
                                const QString &localName,
                                const QString &qName,
                                const QXmlAttributes &attributes)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << qName;
  _tag_stack.push_back(qName);
  bool is_ok = true;

  try
    {
      // startElement_rawdata
      if(qName == "rawdata")
        {
          is_ok = startElement_rawdata(attributes);
        }

      // startElement_data_file
      else if(qName == "data_file")
        {
          is_ok = startElement_data_file(attributes);
        }
      // startElement_group
      else if(qName == "group")
        {
          is_ok = startElement_group(attributes);
        }
      // startElement_protein
      else if(qName == "protein")
        {
          is_ok = startElement_protein(attributes);
        }
      // startElement_peptide
      else if(qName == "peptide")
        {
          is_ok = startElement_peptide(attributes);
        }
      else if(qName == "psimod")
        {
          is_ok = startElement_psimod(attributes);
        }
      // startElement_observed_in
      else if(qName == "observed_in")
        {
          is_ok = startElement_observed_in(attributes);
        }
      // startElement_isotope_label
      else if(qName == "isotope_label")
        {
          is_ok = startElement_isotope_label(attributes);
        }
      // startElement_mod
      else if(qName == "mod")
        {
          is_ok = startElement_mod(attributes);
        }
      // startElement_alignment_method
      else if(qName == "alignment_method")
        {
          is_ok = startElement_alignment_method(attributes);
        }
      // startElement_obiwarp
      else if(qName == "obiwarp")
        {
          is_ok = startElement_obiwarp(attributes);
        }
      // startElement_ms2
      else if(qName == "ms2")
        {
          is_ok = startElement_ms2(attributes);
        }
      // startElement_align
      else if(qName == "align")
        {
          is_ok = startElement_align(attributes);
        }
      // startElement_quantification_method
      else if(qName == "quantification_method")
        {
          is_ok = startElement_quantification_method(attributes);
        }
      // startElement_xic_extraction
      else if(qName == "xic_extraction")
        {
          is_ok = startElement_xic_extraction(attributes);
        }
      // startElement_mz_range
      else if(qName == "mz_range")
        {
          is_ok = startElement_mz_range(attributes);
        }
      // startElement_ppm_range
      else if(qName == "ppm_range")
        {
          is_ok = startElement_ppm_range(attributes);
        }
      // startElement_anti_spike
      else if(qName == "anti_spike")
        {
          is_ok = startElement_anti_spike(attributes);
        }
      // startElement_background
      else if(qName == "background")
        {
          is_ok = startElement_background(attributes);
        }
      // startElement_smoothing
      else if(qName == "smoothing")
        {
          is_ok = startElement_smoothing(attributes);
        }
      // startElement_detection_moulon
      else if(qName == "detection_moulon")
        {
          is_ok = startElement_detection_moulon(attributes);
        }
      // startElement_detection_zivy
      else if(qName == "detection_zivy")
        {
          is_ok = startElement_detection_zivy(attributes);
        }
      // startElement_quantification_results
      else if(qName == "quantification_results")
        {
          is_ok = startElement_quantification_results(attributes);
        }
      // startElement_quantification_result
      else if(qName == "quantification_result")
        {
          is_ok = startElement_quantification_result(attributes);
        }
      // startElement_compar_result
      else if(qName == "compar_result")
        {
          is_ok = startElement_compar_result(attributes);
        }
      // startElement_all_xics_traces
      else if(qName == "all_xics_traces")
        {
          is_ok = startElement_all_xics_traces(attributes);
        }
      // startElement_peptide_traces
      else if(qName == "peptide_traces")
        {
          is_ok = startElement_peptide_traces(attributes);
        }
      // startElement_mz_traces
      else if(qName == "mz_traces")
        {
          is_ok = startElement_mz_traces(attributes);
        }
      // startElement_mzrt_traces
      else if(qName == "mzrt_traces")
        {
          is_ok = startElement_mzrt_traces(attributes);
        }
      // startElement_mzrt_value
      else if(qName == "mzrt_value")
        {
          is_ok = startElement_mzrt_value(attributes);
        }
      // startElement_quantify
      else if(qName == "quantify")
        {
          is_ok = startElement_quantify(attributes);
        }
      // startElement_peptides_in_peptide_list
      else if(qName == "peptides_in_peptide_list")
        {
          is_ok = startElement_peptides_in_peptide_list(attributes);
        }
      // startElement_mzrt
      else if(qName == "mzrt")
        {
          is_ok = startElement_mzrt(attributes);
        }

      _currentText.clear();
    }
  catch(mcqError exception_mcq)
    {
      _errorStr = QObject::tr(
                    "ERROR in MasschroqmlParser::startElement tag %1, "
                    "MCQ exception:\n%2")
                    .arg(qName)
                    .arg(exception_mcq.qwhat());
      return false;
    }
  catch(pappso::PappsoException exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in MasschroqmlParser::startElement tag %1, "
                    "PAPPSO exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in MasschroqmlParser::startElement tag %1, "
                    "std exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " " << qName
           << " is_ok=" << is_ok;
  return is_ok;
}

bool
MasschroqmlParser::endElement(const QString &namespaceURI,
                              const QString &localName,
                              const QString &qName)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << qName;
  bool is_ok = true;
  try
    {
      // endElement_peptide_list
      if(qName == "peptide_list")
        {
          is_ok = endElement_peptide_list();
        }
      // endElement_isotope_label
      else if(qName == "isotope_label")
        {
          is_ok = endElement_isotope_label();
        }
      // end of alignment method obiwarp
      else if((_tag_stack.size() > 1) &&
              (_tag_stack[_tag_stack.size() - 2] == "obiwarp"))
        {
          // startElement_lmat_precision
          if(qName == "lmat_precision")
            {
              is_ok = endElement_lmat_precision();
            }
          // endElement_mz_start
          else if(qName == "mz_start")
            {
              is_ok = endElement_mz_start();
            } // endElement_mz_stop
          else if(qName == "mz_stop")
            {
              is_ok = endElement_mz_stop();
            }
        }
      // end of alignment method ms2
      else if((_tag_stack.size() > 1) &&
              (_tag_stack[_tag_stack.size() - 2] == "ms2"))
        {
          if(qName == "ms2_tendency_halfwindow")
            {
              is_ok = endElement_ms2_tendency_halfwindow();
            }
          else if(qName == "ms2_smoothing_halfwindow")
            {
              is_ok = endElement_ms2_smoothing_halfwindow();
            }
          else if(qName == "ms1_smoothing_halfwindow")
            {
              is_ok = endElement_ms1_smoothing_halfwindow();
            }
        }
      // endElement_alignment_method
      else if(qName == "alignment_method")
        {
          is_ok = endElement_alignment_method();
        }
      // end of detection_zivy
      else if((_tag_stack.size() > 1) &&
              (_tag_stack[_tag_stack.size() - 2] == "detection_zivy"))
        {
          if(qName == "minmax_half_edge")
            {
              is_ok = endElement_minmax_half_edge();
            }
          else if(qName == "maxmin_half_edge")
            {
              is_ok = endElement_maxmin_half_edge();
            }
          else if(qName == "detection_threshold_on_max")
            {
              is_ok = endElement_detection_threshold_on_max();
            }
          else if(qName == "detection_threshold_on_min")
            {
              is_ok = endElement_detection_threshold_on_min();
            }
          else if(qName == "mean_filter_half_edge")
            {
              is_ok = endElement_mean_filter_half_edge();
            }
        }
      // end of detection_moulon
      else if((_tag_stack.size() > 1) &&
              (_tag_stack[_tag_stack.size() - 2] == "detection_moulon"))
        {
          if(qName == "smoothing_point")
            {
              is_ok = endElement_smoothing_point();
            }
          else if(qName == "TIC_start")
            {
              is_ok = endElement_TIC_start();
            }
          else if(qName == "TIC_stop")
            {
              is_ok = endElement_TIC_stop();
            }
        }
      // endElement_peak_detection
      else if(qName == "detection_moulon")
        {
          is_ok = endElement_detection_moulon();
        }
      else if(qName == "detection_zivy")
        {
          is_ok = endElement_detection_zivy();
        }
      // endElement_quantification_method
      else if(qName == "quantification_method")
        {
          is_ok = endElement_quantification_method();
        }
      // endElement_mz_list
      else if(qName == "mz_list")
        {
          is_ok = endElement_mz_list();
        }
      // endElement_mzrt_traces
      else if(qName == "mzrt_traces")
        {
          is_ok = endElement_mzrt_traces();
        }
      // endElement_quantification_traces
      else if(qName == "quantification_traces")
        {
          is_ok = endElement_quantification_traces();
        }
      // endElement_quantify
      else if(qName == "quantify")
        {
          is_ok = endElement_quantify();
        }
      // endElement_quantification
      else if(qName == "quantification")
        {
          is_ok = endElement_quantification();
        }
      // endElement_masschroq
      else if(qName == "masschroq")
        {
          is_ok = endElement_masschroq();
        }
      else if(qName == "peptide")
        {
          is_ok = endElement_peptide();
        }
    }
  catch(mcqError &exception_mcq)
    {
      _errorStr =
        QObject::tr(
          "ERROR in MasschroqmlParser::endElement tag %1, MCQ exception:\n%2")
          .arg(qName)
          .arg(exception_mcq.qwhat());
      return false;
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in MasschroqmlParser::endElement tag %1, "
                    "PAPPSO exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr =
        QObject::tr(
          "ERROR in MasschroqmlParser::endElement tag %1, std exception:\n%2")
          .arg(qName)
          .arg(exception_std.what());
      return false;
    }
  _currentText.clear();
  _tag_stack.pop_back();
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << qName;
  return is_ok;
}

bool
MasschroqmlParser::startElement_rawdata(const QXmlAttributes &attributes)
{

  QString time_dir = attributes.value("time_values_dir");
  if(!time_dir.isEmpty())
    {
      _read_time_values = true;
      _time_directory   = time_dir;
    }
  if(!QDir(time_dir).exists())
    {
      _errorStr = QObject::tr(
                    "The directory  :\n%1\n from the attribute "
                    "'time_values_dir'  in the element 'rawdata' does "
                    "not exists.")
                    .arg(time_dir);
      return false;
    }
  return true;
}

/// <data_file id=.../>
bool
MasschroqmlParser::startElement_data_file(const QXmlAttributes &attributes)
{

  QString filename = attributes.value("path");
  if(filename.isEmpty())
    {
      _errorStr = QObject::tr("The data_file tag must have a path attribute.");
      return false;
    }

  QString idname = attributes.value("id");
  if(idname.isEmpty())
    {
      _errorStr = QObject::tr("The data_file tag must have an id attribute.");
      return false;
    }

  QFileInfo filenameInfo(filename);
  if(!filenameInfo.exists())
    {
      _errorStr = QObject::tr("cannot find input file : %2 \n").arg(filename);
      return false;
    }
  else if(!filenameInfo.isReadable())
    {
      _errorStr = QObject::tr("cannot read input file : %2 \n").arg(filename);
      return false;
    }

  QString type = attributes.value("type");
  if(!type.isEmpty() && type == "srm")
    {
      _errorStr = QObject::tr(
        "The SRM mode is not yet implemented in MassChroQ, but it will: \n%1");
      return false;
    }

  try
    {
      m_msfileList.insert(QFileInfo(filename).absoluteFilePath());
      pappso::MsFileAccessor file_access(
        filename,
        QString("msfile%1")
          .arg(pappso::Utils::getLexicalOrderedString(m_msfileList.size())));

      pappso::MsRunReaderSPtr run_reader =
        file_access.getMsRunReaderSPtrByRunId("", idname);

      _p_my_chroq->newMsRun(run_reader, _read_time_values, _time_directory);
    }
  catch(pappso::PappsoException &pappso_error)
    {
      _errorStr = QObject::tr("problem creating msrun %1:\n%2")
                    .arg(idname, pappso_error.qwhat());
      return false;
    }
  return (true);
}

/// <group id="G1" data_ids="delumeau1 delumeau2"></group>
bool
MasschroqmlParser::startElement_group(const QXmlAttributes &attributes)
{
  QString idname    = attributes.value("id");
  QString msrun_ids = attributes.value("data_ids");
  if(idname.isEmpty())
    {
      _errorStr = QObject::tr("the group tag must have an id attribute.");
      return false;
    }
  if(msrun_ids.isEmpty())
    {
      _errorStr = QObject::tr("the group tag must have a data_ids attribute.");
      return false;
    }

  QStringList list_msrun_ids = msrun_ids.split(" ", QString::SkipEmptyParts);
  /// add this group of msruns to _groups (: map<group_id, msRunHashGroup *>)
  /// the first msrun in the group will be the reference one
  _p_my_chroq->newMsRunGroup(idname, list_msrun_ids);

  return (true);
}

/// <protein id="P1" desc=""/>
bool
MasschroqmlParser::startElement_protein(const QXmlAttributes &attributes)
{
  QString id   = attributes.value("id");
  QString desc = attributes.value("desc");
  if(id.isEmpty())
    {
      _errorStr = QObject::tr("the protein tag must have an id attribute.");
      return false;
    }
  /// create a new Protein object and set its description
  ProteinSp p_protein;
  try
    {
      Protein protein(id);
      protein.setDescription(desc);
      p_protein = std::make_shared<const Protein>(protein);
    }
  catch(mcqError &error)
    {
      _errorStr =
        QObject::tr("problem creating protein :\n%1").arg(error.what());
      return false;
    }
  /// add this protein to _p_proteins (: map<id, Protein *>)
  _p_my_chroq->addProtein(p_protein);

  return true;
}

/// <peptide id="pep1" seq="" mh="856.23" prot_ids="P1 P2">
bool
MasschroqmlParser::startElement_peptide(const QXmlAttributes &attributes)
{
  QString idname   = attributes.value("id");
  QString mh       = attributes.value("mh");
  QString seq      = attributes.value("seq");
  QString mods     = attributes.value("mods");
  QString prot_ids = attributes.value("prot_ids");
  if(idname.isEmpty())
    {
      _errorStr = QObject::tr("the peptide tag must have an id attribute.");
      return false;
    }
  if(mh.isEmpty())
    {
      _errorStr = QObject::tr("the peptide tag must have a mh attribute.");
      return false;
    }
  /// create a new Peptide object and set its members
  _p_current_peptide = new Peptide(idname);

  if(!mods.isEmpty())
    {
      _p_current_peptide->setMods(mods);
    }
  if(!seq.isEmpty())
    {
      try
        {
          pappso::Peptide peptide(seq);
          _current_peptide_sp = peptide.makeNoConstPeptideSp();
          _p_current_peptide->setPappsoPeptideSp(_current_peptide_sp);
        }
      catch(const pappso::PappsoException &e)
        {
          _errorStr = QObject::tr("error building peptide %1\n%2")
                        .arg(seq)
                        .arg(e.qwhat());
          return false;
        }
    }

  _current_mh = mh.toDouble();
  //_p_current_peptide->setMh(mh.toDouble());
  if(!prot_ids.isEmpty())
    {
      QStringList list;
      list = prot_ids.split(" ", QString::SkipEmptyParts);
      for(int i = 0; i < list.size(); ++i)
        {
          const Protein *p_prot = _p_my_chroq->findProtein(list.at(i));
          if(p_prot == NULL)
            {
              _errorStr =
                QObject::tr("protein id %1 not defined.").arg(list.at(i));
              return false;
            }
          else
            {
              _p_current_peptide->addProtein(p_prot);
            }
        }
    }
  /// add this peptide to _peptide_list (: PeptideList)
  _p_my_chroq->addPeptideInPeptideList(_p_current_peptide);
  return true;
}
bool
MasschroqmlParser::startElement_psimod(const QXmlAttributes &attributes)
{
  QString at  = attributes.value("at");
  QString acc = attributes.value("acc");
  if(at.isEmpty())
    {
      _errorStr = QObject::tr("the psimod tag must have an at attribute.");
      return false;
    }
  if(acc.isEmpty())
    {
      _errorStr = QObject::tr("the psimod tag must have an acc attribute.");
      return false;
    }
  try
    {
      pappso::AaModificationP modification =
        pappso::AaModification::getInstance(acc);
      _current_peptide_sp.get()->addAaModification(modification,
                                                   at.toUInt() - 1);
    }
  catch(pappso::PappsoException e)
    {
      _errorStr = QObject::tr("ERROR setting psimod:\n%1").arg(e.qwhat());
      return false;
    }
  return true;
}
/// <observed_in data="delumeau1" scan="33" z="2">
bool
MasschroqmlParser::startElement_observed_in(const QXmlAttributes &attributes)
{

  QString data = attributes.value("data");
  QString scan = attributes.value("scan");
  QString z    = attributes.value("z");

  if(data.isEmpty())
    {
      _errorStr = QObject::tr(
        "the observed_in tag must have a data attribute "
        "(reference on a valid msRun id).");
      return false;
    }
  if(scan.isEmpty())
    {
      _errorStr =
        QObject::tr("the observed_in tag must have a scan attribute.");
      return false;
    }
  if(z.isEmpty())
    {
      _errorStr = QObject::tr("the observed_in tag must have a z attribute.");
      return false;
    }

  Msrun *p_msrun = _p_my_chroq->findMsRun(data);
  if(p_msrun == NULL)
    {
      _errorStr = QObject::tr(
        "the observed_in tag contains a data attribute "
        "that does not reference any msrun...");
      return false;
    }
  /// add the observed in infos to the current peptide
  _p_current_peptide->observed_in(p_msrun, scan.toInt(), z.toInt());
  return true;
}

/// </peptide_list>
bool
MasschroqmlParser::endElement_peptide_list()
{
  _p_my_chroq->setPeptideListInMsruns();
  return true;
}

bool
MasschroqmlParser::endElement_peptide()
{
  _p_current_peptide->setMh(_current_mh);
  return true;
}

bool
MasschroqmlParser::startElement_isotope_label(const QXmlAttributes &attributes)
{
  /// create a new IsotopeLabel object and set the current one to it
  _p_isotope_label = new IsotopeLabel();
  QString id       = attributes.value("id");
  if(id.isEmpty())
    {
      _errorStr =
        QObject::tr("the isotope_label tag must have an id attribute.");
      return false;
    }
  else
    {
      _p_isotope_label->setXmlId(id);
    }
  return true;
}

/// isotope_label modification
bool
MasschroqmlParser::startElement_mod(const QXmlAttributes &attributes)
{
  QString value = attributes.value("value");
  QString at    = attributes.value("at");
  QString acc   = attributes.value("acc");
  if(value.isEmpty())
    {
      _errorStr = QObject::tr(
        "the mod tag must have a value attribute : the "
        "mass of this modification");
      return false;
    }

  if(at.isEmpty())
    {
      _errorStr = QObject::tr(
        "the mod tag must have an at attribute : the "
        "position of the modification given by AA one "
        "letter code or Cter or Nter");
      return false;
    }
  /// create a new IsotopeLabelModification and add it to the current isotope
  IsotopeLabelModification *p_mod = new IsotopeLabelModification();
  p_mod->setAA(at);
  p_mod->setMass(value.toDouble());

  if(!acc.isEmpty())
    {
      pappso::AaModificationP modification =
        pappso::AaModification::getInstance(acc);
      p_mod->setPappsoModification(modification);
    }
  _p_isotope_label->addIsotopeLabelModification(p_mod);
  return true;
}

bool
MasschroqmlParser::endElement_isotope_label()
{
  _p_my_chroq->addIsotopeLabel(_p_isotope_label);
  //  _p_isotope_label = NULL;
  return true;
}

/// <alignment_method id="obiwarp1">
bool
MasschroqmlParser::startElement_alignment_method(
  const QXmlAttributes &attributes)
{

  _current_alignment_method_id = attributes.value("id");
  if(_current_alignment_method_id.isEmpty())
    {
      _errorStr =
        QObject::tr("the alignment_method tag must have an id attribute.");
      return false;
    }

  /// we will set the current_alignment_method later (in the <obiwarp or
  /// <ms2> tag that follows this one )
  //  _p_current_alignment_method = NULL;
  return true;
}

/// <obiwarp>
bool
MasschroqmlParser::startElement_obiwarp(const QXmlAttributes &attributes)
{
  QString time_output_dir = attributes.value("write_time_values_output_dir");
  if(!time_output_dir.isEmpty())
    {
      // Print time value
      MonitorAlignmentTime *time_monitor = new MonitorAlignmentTime();
      time_monitor->setOutputDirectory(time_output_dir);
      _p_current_alignment_method = new AlignmentObiwarp(time_monitor);
    }
  else
    {
      // not print time value
      MonitorAlignmentBase *base_monitor = new MonitorAlignmentBase();
      _p_current_alignment_method        = new AlignmentObiwarp(base_monitor);
    }
  return true;
}

/// <lmat_precision>0.5</lmat_precision>
bool
MasschroqmlParser::endElement_lmat_precision()
{
  AlignmentObiwarp *p_obiwarp_meth(
    (AlignmentObiwarp *)_p_current_alignment_method);
  p_obiwarp_meth->setLmatPrecision(_currentText.toDouble());
  return true;
}

/// <mz_start>300</mz_start>
bool
MasschroqmlParser::endElement_mz_start()
{
  AlignmentObiwarp *p_obiwarp_meth(
    (AlignmentObiwarp *)_p_current_alignment_method);
  p_obiwarp_meth->setMassStart(_currentText.toDouble());
  return true;
}

/// <mz_stop>900</mz_stop>
bool
MasschroqmlParser::endElement_mz_stop()
{
  AlignmentObiwarp *p_obiwarp_meth(
    (AlignmentObiwarp *)_p_current_alignment_method);
  p_obiwarp_meth->setMassEnd(_currentText.toDouble());
  return true;
}

/// <ms2>
bool
MasschroqmlParser::startElement_ms2(const QXmlAttributes &attributes)
{
  QString time_output_dir = attributes.value("write_time_values_output_dir");
  if(!time_output_dir.isEmpty())
    {
      // Print time value
      MonitorAlignmentTime *time_monitor = new MonitorAlignmentTime();
      time_monitor->setOutputDirectory(time_output_dir);
      _p_current_alignment_method = new AlignmentMs2(time_monitor);
    }
  else
    {
      // not print time value
      MonitorAlignmentBase *base_monitor = new MonitorAlignmentBase();
      _p_current_alignment_method        = new AlignmentMs2(base_monitor);
    }
  return true;
}

/// <ms2_tendency_halfwindow>10</ms2_tendency_halfwindow>
bool
MasschroqmlParser::endElement_ms2_tendency_halfwindow()
{
  AlignmentMs2 *p_ms2_meth((AlignmentMs2 *)_p_current_alignment_method);
  p_ms2_meth->setMs2TendencyWindow(_currentText.toDouble());
  return true;
}

/// <ms2_smoothing_halfwindow>5</ms2_smoothing_halfwindow>
bool
MasschroqmlParser::endElement_ms2_smoothing_halfwindow()
{
  AlignmentMs2 *p_ms2_meth((AlignmentMs2 *)_p_current_alignment_method);
  p_ms2_meth->setMs2SmoothingWindow(_currentText.toDouble());
  return true;
}

/// <ms1_smoothing_halfwindow>15</ms1_smoothing_halfwindow>
bool
MasschroqmlParser::endElement_ms1_smoothing_halfwindow()
{
  AlignmentMs2 *p_ms2_meth((AlignmentMs2 *)_p_current_alignment_method);
  p_ms2_meth->setMs1SmoothingWindow(_currentText.toDouble());
  return true;
}

bool
MasschroqmlParser::endElement_alignment_method()
{
  _p_my_chroq->addAlignmentMethod(_current_alignment_method_id,
                                  _p_current_alignment_method);
  _current_alignment_method_id.clear();
  //  _p_current_alignment_method = NULL;
  return true;
}

/// <align group_id="G1" method_id="obiwarp1">
bool
MasschroqmlParser::startElement_align(const QXmlAttributes &attributes)
{

  QString group_id        = attributes.value("group_id");
  QString align_method_id = attributes.value("method_id");
  QString ref_msrun_id    = attributes.value("reference_data_id");

  if(group_id.isEmpty())
    {
      _errorStr = QObject::tr("the align tag must have a group_id attribute.");
      return false;
    }
  if(align_method_id.isEmpty())
    {
      _errorStr = QObject::tr("the align tag must have a method_id attribute.");
      return false;
    }
  if(ref_msrun_id.isEmpty())
    {
      _errorStr =
        QObject::tr("the align tag must have a ref_msrun_id attribute.");
      return false;
    }
  /// launch alignment
  _p_my_chroq->alignGroup(group_id, align_method_id, ref_msrun_id);
  return true;
}

/// <quantification_method id="q1">
bool
MasschroqmlParser::startElement_quantification_method(
  const QXmlAttributes &attributes)
{

  const QString quantification_id = attributes.value("id");

  if(quantification_id.isEmpty())
    {
      _errorStr =
        QObject::tr("the quantification_method tag must have an id attribute.");
      return false;
    }

  _p_current_quantification_method =
    new QuantificationMethod(quantification_id);
  return true;
}

/// <xic_extraction xic_type="sum">
bool
MasschroqmlParser::startElement_xic_extraction(const QXmlAttributes &attributes)
{
  QString xic_type = attributes.value("xic_type");
  if(xic_type == "sum")
    {
      _p_current_quantification_method->setXicExtractMethod(
        pappso::XicExtractMethod::sum);
      return true;
    }
  else if(xic_type == "max")
    {
      _p_current_quantification_method->setXicExtractMethod(
        pappso::XicExtractMethod::max);
      return true;
    }
  else
    {
      _errorStr = QObject::tr(
        "the xic_extraction tag must have a xic_type "
        "attribute whose value is 'sum' or 'max'.");
      return false;
    }
}

/// <xic_extraction>
/// <mz_range min="0.5" max="1.5"/>
bool
MasschroqmlParser::startElement_mz_range(const QXmlAttributes &attributes)
{
  QString min = attributes.value("min");
  QString max = attributes.value("max");
  if(min.isEmpty())
    {
      _errorStr = QObject::tr("the mz_range tag must have a min attribute.");
      return false;
    }
  if(max.isEmpty())
    {
      _errorStr = QObject::tr("the mz_range tag must have a max attribute.");
      return false;
    }

  _p_current_quantification_method->setXicExtractPrecisionUnit(
    pappso::PrecisionUnit::mz);
  _p_current_quantification_method->setXicExtractLowerPrecision(min.toDouble());
  _p_current_quantification_method->setXicExtractUpperPrecision(max.toDouble());

  return true;
}

/// <xic_extraction>
/// <ppm_range min="" max=""/>
bool
MasschroqmlParser::startElement_ppm_range(const QXmlAttributes &attributes)
{

  QString min = attributes.value("min");
  QString max = attributes.value("max");
  if(min.isEmpty())
    {
      _errorStr = QObject::tr("the ppm_range tag must have a min attribute.");
      return false;
    }
  if(max.isEmpty())
    {
      _errorStr = QObject::tr("the ppm_range tag must have a max attribute.");
      return false;
    }

  _p_current_quantification_method->setXicExtractPrecisionUnit(
    pappso::PrecisionUnit::ppm);
  _p_current_quantification_method->setXicExtractLowerPrecision(min.toDouble());
  _p_current_quantification_method->setXicExtractUpperPrecision(max.toDouble());

  return true;
}

/// <anti_spike half="8">
bool
MasschroqmlParser::startElement_anti_spike(const QXmlAttributes &attributes)
{

  QString half = attributes.value("half");
  if(half.isEmpty())
    {
      _errorStr = QObject::tr("the anti_spike tag must have a half attribute.");
      return false;
    }

  // pappso::FilterMorphoAntiSpike f_spike(half.toDouble());
  _p_current_quantification_method->addFilter(
    std::make_shared<pappso::FilterMorphoAntiSpike>(half.toDouble()));
  return true;
}

//<background half_mediane="5" half_min_max="20">
bool
MasschroqmlParser::startElement_background(const QXmlAttributes &attributes)
{
  QString half_mediane = attributes.value("half_mediane");
  QString half_min_max = attributes.value("half_min_max");

  if(half_mediane.isEmpty())
    {
      _errorStr =
        QObject::tr("the background tag must have a half_mediane attribute.");
      return false;
    }
  if(half_min_max.isEmpty())
    {
      _errorStr =
        QObject::tr("the background tag must have a half_min_max attribute.");
      return false;
    }

  pappso::FilterMorphoBackground f_bkg(half_mediane.toDouble(),
                                       half_min_max.toDouble());
  _p_current_quantification_method->addFilter(
    std::make_shared<pappso::FilterMorphoBackground>(f_bkg));
  return true;
}

//<smoothing half="5"></smoothing>
bool
MasschroqmlParser::startElement_smoothing(const QXmlAttributes &attributes)
{
  QString half = attributes.value("half");
  if(half.isEmpty())
    {
      _errorStr = QObject::tr("the smoothing tag must have a half attribute.");
      return false;
    }

  pappso::FilterMorphoMean f_smooth(half.toDouble());

  _p_current_quantification_method->addFilter(
    std::make_shared<pappso::FilterMorphoMean>(f_smooth));
  return true;
}

/// <detection_zivy>
bool
MasschroqmlParser::startElement_detection_zivy(const QXmlAttributes &attributes)
{
  msp_current_detection_zivy =
    std::make_shared<pappso::TraceDetectionZivy>(0, 2, 3, 1000, 100);
  return true;
}

/// <detection_zivy>
/// <mean_filter_half_edge>3</mean_filter_half_edge>
bool
MasschroqmlParser::endElement_mean_filter_half_edge()
{
  if(msp_current_detection_zivy != nullptr)
    {
      msp_current_detection_zivy->setFilterMorphoMean(
        pappso::FilterMorphoMean(_currentText.toDouble()));
    }
  else
    {
      return false;
    }
  return true;
}

/// <detection_zivy>
/// <minmax_half_edge>3</minmax_half_edge>
bool
MasschroqmlParser::endElement_minmax_half_edge()
{
  if(msp_current_detection_zivy != nullptr)
    {
      msp_current_detection_zivy->setFilterMorphoMinMax(
        pappso::FilterMorphoMinMax(_currentText.toDouble()));
    }
  else
    {
      return false;
    }
  return true;
}

/// <detection_zivy>
/// <maxmin_half_edge>3</maxmin_half_edge>
bool
MasschroqmlParser::endElement_maxmin_half_edge()
{
  if(msp_current_detection_zivy != nullptr)
    {
      msp_current_detection_zivy->setFilterMorphoMaxMin(
        pappso::FilterMorphoMaxMin(_currentText.toDouble()));
    }
  else
    {
      return false;
    }
  return true;
}

/// <detection_zivy>
/// <detection_threshold_on_max>5000</detection_threshold_on_max>
bool
MasschroqmlParser::endElement_detection_threshold_on_max()
{
  if(msp_current_detection_zivy != nullptr)
    {
      msp_current_detection_zivy->setDetectionThresholdOnMinmax(
        _currentText.toDouble());
    }
  else
    {
      return false;
    }
  return true;
}

/// <detection_zivy>
/// <detection_threshold_on_min>3000</detection_threshold_on_min>
bool
MasschroqmlParser::endElement_detection_threshold_on_min()
{
  if(msp_current_detection_zivy != nullptr)
    {
      msp_current_detection_zivy->setDetectionThresholdOnMaxmin(
        _currentText.toDouble());
    }
  else
    {
      return false;
    }
  return true;
}

/// <detection_moulon>
bool
MasschroqmlParser::startElement_detection_moulon(
  const QXmlAttributes &attributes)
{
  msp_current_detection_moulon =
    std::make_shared<pappso::TraceDetectionMoulon>(0, 100, 10);
  return true;
}

/// <detection_moulon>
/// <TIC_stop>4</TIC_stop>
bool
MasschroqmlParser::endElement_TIC_stop()
{
  if(msp_current_detection_moulon != nullptr)
    {
      msp_current_detection_moulon->setTicStop(_currentText.toDouble());
    }
  else
    {
      return false;
    }
  return true;
}

/// <detection_moulon>
/// <TIC_start>4</TIC_start>
bool
MasschroqmlParser::endElement_TIC_start()
{
  if(msp_current_detection_moulon != nullptr)
    {
      msp_current_detection_moulon->setTicStart(_currentText.toDouble());
    }
  else
    {
      return false;
    }
  return true;
}

/// <detection_moulon>
/// <smoothing_point>5</smoothing_point>
bool
MasschroqmlParser::endElement_smoothing_point()
{
  if(msp_current_detection_moulon != nullptr)
    {
      msp_current_detection_moulon->setFilterMorphoMean(
        _currentText.toDouble());
    }
  else
    {
      return false;
    }
  return true;
}

/// </peak_detection>
bool
MasschroqmlParser::endElement_detection_moulon()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  if(msp_current_detection_moulon == nullptr)
    return false;
  _p_current_quantification_method->setDetectionMethod(
    std::make_shared<const pappso::TraceDetectionMoulon>(
      *msp_current_detection_moulon));
  msp_current_detection_moulon = nullptr;
  // _p_current_detection_method = NULL;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  return true;
}
bool
MasschroqmlParser::endElement_detection_zivy()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  if(msp_current_detection_zivy == nullptr)
    return false;
  _p_current_quantification_method->setDetectionMethod(
    std::make_shared<const pappso::TraceDetectionZivy>(
      *msp_current_detection_zivy));
  msp_current_detection_zivy = nullptr;
  // _p_current_detection_method = NULL;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  return true;
}
bool
MasschroqmlParser::endElement_quantification_method()
{
  _p_my_chroq->addQuantificationMethod(_p_current_quantification_method);
  //_p_current_quantification_method = NULL;
  return true;
}

/// <quantification_results>
bool
MasschroqmlParser::startElement_quantification_results(
  const QXmlAttributes &attributes)
{
  return true;
}

bool
MasschroqmlParser::startElement_quantification_result(
  const QXmlAttributes &attributes)
{
  QString output_file = attributes.value("output_file");
  if(output_file.isEmpty())
    {
      _errorStr = QObject::tr(
        "the quantification_result tag must have an output_file attribute.");
      return false;
    }

  if(!ensureFilePathExists(output_file))
    {
      QDir file_dir(output_file);
      _errorStr = QObject::tr(
                    "problem writing output to directory %1\n Please "
                    "check output file path and permissions")
                    .arg(file_dir.absolutePath());
      return false;
    }

  QString format = attributes.value("format");
  if(format.isEmpty())
    {
      _errorStr = QObject::tr(
        "the quantification_result tag must have a format attribute.");
      return false;
    }

  bool with_traces;
  QString xic_traces = attributes.value("xic_traces");
  if(xic_traces == "true")
    {
      with_traces = true;
    }
  else
    {
      with_traces = false;
    }
  try
    {
      if(format == "gnumeric")
        {
          // return startGnumericResult(output_file);
        }
      else if(format == "xhtmltable")
        {
          // return startXhtmltableResult(output_file);
        }
      else if(format == "ods")
        {
          output_file = ensureFileExtension(output_file, "ods");
          MonitorSpeedInterface *p_monitor_speed =
            new MonitorOdsOutput(output_file);
          _p_current_monitor_speed_list->addMonitor(p_monitor_speed);
          return true;
        }
      else if(format == "tsv")
        {
          output_file = ensureFileExtension(output_file, "d");
          MonitorSpeedInterface *p_monitor_speed =
            new MonitorTsvOutput(output_file);
          _p_current_monitor_speed_list->addMonitor(p_monitor_speed);
          return true;
        }
      else if(format == "masschroqml")
        {
          return startXmlResult(output_file, with_traces);
        }
      else
        {
          _errorStr = QObject::tr(
            "value '%1' of attribute format in quantification_result tag is "
            "not "
            "supported.\n Supported values are : ods, tsv and masschroqml.");
          return false;
        }
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(QObject::tr("error creating result output file :\n%2")
                       .arg(ods_error.qwhat()));
    }
  return false;
}

bool
MasschroqmlParser::ensureFilePathExists(QString output_file)
{
  QFileInfo complete_path(output_file);
  //  qDebug()<< "MasschroqmlParser::ensureFilePathExists complete_path.dir()"
  //  << complete_path.dir().absolutePath() ;
  // qDebug() << QFileInfo("~/examples/191697/main.cpp").dir().absolutePath();

  // qDebug() << QFileInfo("~/examples/191697/main").dir().absolutePath();
  QDir only_dir(complete_path.dir().absolutePath());
  if(only_dir.exists())
    {
      return true;
    }
  else
    {
      // try to create it
      if(only_dir.mkpath(only_dir.absolutePath()) == false)
        {
          return false;
          qDebug() << "MasschroqmlParser::ensureFilePathExists FALSE "
                   << only_dir.absolutePath();
        }
      else
        {
          return true;
        }
    }
  return false;
}

bool
MasschroqmlParser::startElement_compar_result(const QXmlAttributes &attributes)
{
  QString output_file = attributes.value("output_file");
  if(output_file.isEmpty())
    {
      _errorStr = QObject::tr(
        "the compar_result tag must have an output_file attribute.");
      return false;
    }

  if(!ensureFilePathExists(output_file))
    {
      QDir file_dir(output_file);
      _errorStr = QObject::tr(
                    "problem writing output to directory %1\n Please "
                    "check output file path and permissions")
                    .arg(file_dir.absolutePath());
      return false;
    }
  QString format = attributes.value("format");
  if(format.isEmpty())
    {
      _errorStr =
        QObject::tr("the compar_result tag must have a format attribute.");
      return false;
    }

  try
    {
      if(format == "ods")
        {
          output_file = ensureFileExtension(output_file, "ods");
          MonitorSpeedInterface *p_monitor_speed =
            new MonitorComparOdsOutput(output_file);
          _p_current_monitor_speed_list->addMonitor(p_monitor_speed);
          return true;
        }
      else if(format == "tsv")
        {
          output_file = ensureFileExtension(output_file, "d");
          MonitorSpeedInterface *p_monitor_speed =
            new MonitorComparTsvOutput(output_file);
          _p_current_monitor_speed_list->addMonitor(p_monitor_speed);
          return true;
        }
      else
        {
          _errorStr = QObject::tr(
            "value '%1' of attribute format in compar_result tag is "
            "not supported.\n Supported values are : ods, tsv.");
          return false;
        }
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(QObject::tr("error creating compar output file :\n%2")
                       .arg(ods_error.qwhat()));
    }
  return false;
}

QString
MasschroqmlParser::ensureFileExtension(const QString &output_file,
                                       const QString &file_extension)
{
  QFileInfo filename(output_file);
  if(filename.suffix().isEmpty())
    {
      return QString(output_file + "." + file_extension);
    }
  return output_file;
}

bool
MasschroqmlParser::startXmlResult(const QString output_file,
                                  const bool with_traces)
{

  QString mcq_file = ensureFileExtension(output_file, "masschroqML");
  MasschroqWriter *p_results;
  QString input_file = _p_my_chroq->getXmlFilename();
  if(input_file.isEmpty())
    {
      _errorStr = QObject::tr(
        "problem creating masschroqML output results :\n; "
        "cannot find input masschroqML file");
      return false;
    }
  try
    {
      p_results = new MasschroqWriter(input_file, mcq_file, with_traces);
    }
  catch(mcqError &error)
    {
      _errorStr = QObject::tr("problem creating xml output results :\n%1")
                    .arg(error.what());
      return false;
    }
  _p_current_monitor_speed_list->addMonitor(p_results);
  return true;
}

/// <all_xics_traces output_dir="xics_traces" format="tsv"/>
bool
MasschroqmlParser::startElement_all_xics_traces(
  const QXmlAttributes &attributes)
{

  QString output_dir = attributes.value("output_dir");
  if(output_dir.isEmpty())
    {
      _errorStr = QObject::tr(
        "the all_xics_traces tag must have an output_dir attribute.");
      return false;
    }
  QString format = attributes.value("format");
  if(format.isEmpty())
    {
      format = "ods";
    }
  if(format != "tsv" && format != "ods")
    {
      _errorStr = QObject::tr(
                    "the value '%1' of the format attribute in "
                    "all_xics_traces tag is not supported.\n Supported "
                    "formats are : 'ods'(default) and 'tsv'.")
                    .arg(format);
      return false;
    }
  if(format == "ods")
    {
      _p_my_chroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::ods);
    }
  else
    {
      _p_my_chroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::tsv);
    }

  _p_my_chroq->allPeptideTrace();
  // TsvAllXics * p_results = new TsvAllXics(output_dir);
  //_p_current_monitor_list->addMonitor(p_results);
  return true;
}

/// <peptide_traces peptide_ids="pep0 pep6" output_dir="bla" format="tsv"/>
bool
MasschroqmlParser::startElement_peptide_traces(const QXmlAttributes &attributes)
{
  QString output_dir = attributes.value("output_dir");
  if(output_dir.isEmpty())
    {
      _errorStr =
        QObject::tr("the peptides_xics tag must have an output_dir attribute.");
      return false;
    }
  QString peptides = attributes.value("peptide_ids");
  if(peptides.isEmpty())
    {
      _errorStr =
        QObject::tr("the peptides_xics tag must have a peptide_ids attribute.");
      return false;
    }
  QString format = attributes.value("format");
  if(format.isEmpty())
    {
      format = "ods";
    }
  if(format != "tsv" && format != "ods")
    {
      _errorStr = QObject::tr(
                    "the value '%1' of the format attribute in "
                    "peptide_traces tag is not supported.\n Supported "
                    "formats are : 'ods'(default) and 'tsv'.")
                    .arg(format);
      return false;
    }
  if(format == "ods")
    {
      _p_my_chroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::ods);
    }
  else
    {
      _p_my_chroq->setTracesDirectory(QDir(output_dir), McqTsvFormat::tsv);
    }
  QStringList list_peptide_ids = peptides.split(" ", QString::SkipEmptyParts);
  foreach(const QString &peptide_id, list_peptide_ids)
    {
      const Peptide *peptide = _p_my_chroq->getPeptide(peptide_id);
      if(peptide == nullptr)
        {
          _errorStr =
            QObject::tr("the peptide '%1' is not defined.").arg(peptide_id);
          return false;
        }
      _p_my_chroq->addPeptideTrace(peptide);
    }

  // TsvPepListXics * p_results = new TsvPepListXics(output_dir);
  return true;
}

/// <mz_traces mz_values="6 4" output_dir="mz_xics_traces" format="tsv"/>
bool
MasschroqmlParser::startElement_mz_traces(const QXmlAttributes &attributes)
{
  QString output_dir = attributes.value("output_dir");
  if(output_dir.isEmpty())
    {
      _errorStr =
        QObject::tr("the mz_traces tag must have an output_dir attribute.");
      return false;
    }
  QString mz_values = attributes.value("mz_values");
  if(mz_values.isEmpty())
    {
      _errorStr =
        QObject::tr("the mz_traces tag must have a mz_values attribute.");
      return false;
    }
  QString format = attributes.value("format");
  if(format.isEmpty() || format != "tsv")
    {
      _errorStr = QObject::tr(
        "the value '%1' of the format attribute in "
        "mz_traces tag is not supported.\n Supported "
        "formats are : 'tsv'(default).");
      return false;
    }

  return true;
}

/// <mzrt_traces output_dir="mzrt_xics_traces" format="tsv"/>
bool
MasschroqmlParser::startElement_mzrt_traces(const QXmlAttributes &attributes)
{
  QString output_dir = attributes.value("output_dir");
  if(output_dir.isEmpty())
    {
      _errorStr =
        QObject::tr("the mzrt_traces tag must have an output_dir attribute.");
      return false;
    }

  QString format = attributes.value("format");
  if(format.isEmpty() || format != "tsv")
    {
      _errorStr = QObject::tr(
        "the value '%1' of the format attribute in "
        "mzrt_traces tag is not supported.\n Supported "
        "formats are : 'tsv'(default).");
      return false;
    }
  return true;
}

bool
MasschroqmlParser::startElement_mzrt_value(const QXmlAttributes &attributes)
{
  QString mz = attributes.value("mz");
  QString rt = attributes.value("rt");
  if(mz.isEmpty())
    {
      _errorStr = QObject::tr("the mzrt_value tag must have a mz attribute.");
      return false;
    }
  if(rt.isEmpty())
    {
      _errorStr = QObject::tr("the mzrt_value tag must have a rt attribute.");
      return false;
    }

  return true;
}

bool
MasschroqmlParser::endElement_mzrt_traces()
{
  return true;
}

bool
MasschroqmlParser::endElement_quantification_traces()
{
  return true;
}

/// <quantify id="q1" withingroup="G1" quantification_method_id="q1">
/// selection of masses to extract xic, detect peaks, quantify in this group
bool
MasschroqmlParser::startElement_quantify(const QXmlAttributes &attributes)
{
  // set the result/monitor list
  _p_my_chroq->setResults(_p_current_monitor_speed_list);

  _current_quantify_id     = attributes.value("id");
  QString quanti_group_id  = attributes.value("withingroup");
  QString quanti_method_id = attributes.value("quantification_method_id");

  if(_current_quantify_id.isEmpty())
    {
      _errorStr =
        QObject::tr("the quantify tag must have a non empty id attribute.");
      return false;
    }

  if(quanti_group_id.isEmpty())
    {
      _errorStr = QObject::tr(
        "the quantify tag must have a withingroup attribute "
        "containing a valid reference on a group.");
      return false;
    }

  if(quanti_method_id.isEmpty())
    {
      _errorStr = QObject::tr(
        "the quantify tag must have a withingroup attribute "
        "containing a valid reference on a group.");
      return false;
    }

  QuantificationMethod *quanti_method =
    _p_my_chroq->findQuantificationMethod(quanti_method_id);
  if(quanti_method == NULL)
    {
      _errorStr = QObject::tr("cannot find quantification method with id '%1'")
                    .arg(quanti_method_id);
      return false;
    }

  const msRunHashGroup *quanti_group = _p_my_chroq->findGroup(quanti_group_id);

  if(quanti_group == NULL)
    {
      _errorStr =
        QObject::tr("cannot find group with id '%1'").arg(quanti_group_id);
      return false;
    }

  Quantificator *current_quantificator =
    new Quantificator(_current_quantify_id, quanti_group, quanti_method);
  _p_my_chroq->addQuantificator(current_quantificator);
  return true;
}

// <peptides_in_peptide_list mode="mean">
bool
MasschroqmlParser::startElement_peptides_in_peptide_list(
  const QXmlAttributes &attributes)
{

  QString mode = attributes.value("mode");
  if(mode.isEmpty())
    {
      _errorStr = QObject::tr(
        "the peptides_in_peptide_list tag must have a mode attribute.");
      return false;
    }
  else if(mode == "realxic_or_mean")
    {
      _p_my_chroq->setQuantificatorMatchMode(_current_quantify_id,
                                             McqMatchingMode::realxic_or_mean);
    }
  else if(mode == "mean")
    {
      _p_my_chroq->setQuantificatorMatchMode(_current_quantify_id,
                                             McqMatchingMode::mean);
    }
  else if(mode == "real_or_mean")
    {
      _p_my_chroq->setQuantificatorMatchMode(_current_quantify_id,
                                             McqMatchingMode::real_or_mean);
    }
  else if(mode == "post_matching")
    {
      _p_my_chroq->setQuantificatorMatchMode(_current_quantify_id,
                                             McqMatchingMode::post_matching);
    }
  else if(mode == "no_matching")
    {
      _p_my_chroq->setQuantificatorMatchMode(_current_quantify_id,
                                             McqMatchingMode::no_matching);
    }
  else
    {
      _errorStr = QObject::tr(
        "in peptides_in_peptide_list tag the mode attribute must "
        "be 'mean', 'real_or_mean', 'realxic_or_mean' or 'no_matching'.");
      return false;
    }

  /// get the list of the isotopes
  QStringList list_isotope_refs;
  QString isotope_label_refs = attributes.value("isotope_label_refs");
  if(isotope_label_refs.isEmpty())
    {
    }
  else
    {
      list_isotope_refs =
        isotope_label_refs.split(" ", QString::SkipEmptyParts);
    }

  mcq_double ni_min_abundance = 0;
  if(attributes.value("ni_min_abundance").isEmpty())
    {
    }
  else
    {
      ni_min_abundance = attributes.value("ni_min_abundance").toDouble();
    }

  /********* to put in the quantificator *************************************/

  _p_my_chroq->addQuantificatorPeptideItems(
    _current_quantify_id, list_isotope_refs, ni_min_abundance);

  /********** end to put in the quantificator *******/
  return true;
}

/// <mzrt mz="354.45" rt="7549.25"/>
bool
MasschroqmlParser::startElement_mzrt(const QXmlAttributes &attributes)
{
  QString mz = attributes.value("mz");
  QString rt = attributes.value("rt");
  if(mz.isEmpty())
    {
      _errorStr = QObject::tr("the mzrt tag must have an mz attribute.");
      return false;
    }
  if(rt.isEmpty())
    {
      _errorStr = QObject::tr("the mzrt tag must have an rt attribute.");
      return false;
    }

  _p_my_chroq->addQuantificatorMzrtItem(_current_quantify_id, mz, rt);

  return true;
}

/// <mz_list>45.154 56.15465 0354.45</mz_list>
bool
MasschroqmlParser::endElement_mz_list()
{
  if(_tag_stack[_tag_stack.size() - 2] == "quantify")
    {
      QStringList str_list_mass =
        _currentText.split(" ", QString::SkipEmptyParts);

      _p_my_chroq->addQuantificatorMzItems(_current_quantify_id, str_list_mass);
    }
  return true;
}

bool
MasschroqmlParser::endElement_quantify()
{
  // time to launch quantification in the group :
  _p_my_chroq->executeQuantification(_current_quantify_id);

  _p_my_chroq->deleteQuantificator(_current_quantify_id);

  _current_quantify_id.clear();

  qDebug() << "MasschroqmlParser::endElement_quantify end ";

  return true;
}

bool
MasschroqmlParser::endElement_quantification()
{
  // calls debriefing method in the results/monitors
  // so the result file is finished writing
  //_p_my_chroq->debriefing();
  return true;
}

bool
MasschroqmlParser::endElement_masschroq()
{
  return true;
}

bool
MasschroqmlParser::characters(const QString &str)
{
  _currentText += str;
  return true;
}

bool
MasschroqmlParser::fatalError(const QXmlParseException &exception)
{
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2:\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());
  return false;
}

QString
MasschroqmlParser::errorString() const
{
  return _errorStr;
}

bool
MasschroqmlParser::endDocument()
{
  return true;
}
