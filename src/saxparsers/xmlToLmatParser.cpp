/**
 * \file xmlToLmatParser.cpp
 * \date July 25, 2011
 * \author Edlira Nano
 */

#include "xmlToLmatParser.h"

XmlToLmatParserHandlerInterface::XmlToLmatParserHandlerInterface(
  LMat *lmat, mcq_float mass_start, mcq_float mass_end, mcq_float precision)
{
  this->_lmat = lmat;
  _spectrum_map.clear();
  _precision = precision;

  std::vector<mcq_float> mass_list;

  for(mcq_float j = mass_start; j <= mass_end; j += precision)
    {
      mass_list.push_back(j);
    }

  mcq_float coeff = 1 / precision;
  mass_start      = ((mass_start - (precision / 2)) * coeff);
  for(unsigned int i = 0; i < mass_list.size(); i++, mass_start += coeff)
    {
      _mass_integrator[mass_start] = 0;
    }

  // Set the mz values:
  _lmat->_mz_vals = mass_list.size();
  delete _lmat->_mz;

  mcq_float *mz_tmp = new mcq_float[_lmat->_mz_vals];
  for(unsigned int i = 0; i < mass_list.size(); i++)
    {
      mz_tmp[i] = mass_list[i];
    }
  _lmat->_mz = new VecF(_lmat->_mz_vals, mz_tmp);
}
bool
XmlToLmatParserHandlerInterface::needPeakList() const
{
  return true;
}
void
XmlToLmatParserHandlerInterface::setQualifiedMassSpectrum(
  const pappso::QualifiedMassSpectrum &qspectrum)
{

  if(qspectrum.getMsLevel() == 1)
    {
      // reset mass integrator :
      std::map<int, mcq_float>::iterator it;
      for(it = _mass_integrator.begin(); it != _mass_integrator.end(); ++it)
        {
          (*it).second = 0;
        }

      mcq_float coeff = 1 / _precision;
      mcq_float half  = (_precision / 2);

      const pappso::MassSpectrum *spectrum =
        qspectrum.getMassSpectrumCstSPtr().get();
      for(auto &&peak : *spectrum)
        {
          int mass_indice = (int)((peak.x - half) * coeff);
          it              = _mass_integrator.find(mass_indice);
          if(it == _mass_integrator.end())
            {
              // mass indice not found
            }
          else
            {
              it->second += (mcq_float)peak.y;
              //_mass_integrator[(int)decoded_data[i]] += decoded_data[i+1];
            }
        }

      std::vector<mcq_float> mass_spectrum;
      mass_spectrum.resize(_mass_integrator.size());
      int i = 0;
      for(it = _mass_integrator.begin(); it != _mass_integrator.end();
          ++it, i++)
        {
          mass_spectrum[i] = (*it).second;
        }

      _spectrum_map[(mcq_float)qspectrum.getRtInSeconds()] = mass_spectrum;
    }
}

void
XmlToLmatParserHandlerInterface::debrief()
{
  // Set the time values:
  _lmat->_tm_vals = _spectrum_map.size();
  delete _lmat->_tm;

  mcq_float *tm_tmp = new mcq_float[_lmat->_tm_vals];

  // Read the matrix:
  int rows_by_cols = _lmat->_tm_vals * _lmat->_mz_vals;
  // printf("rbycools: %d\n", rows_by_cols);
  mcq_float *mat_tmp = new mcq_float[rows_by_cols];

  std::map<mcq_float, std::vector<mcq_float>>::iterator it;
  int i = 0;
  int j = 0;
  for(it = _spectrum_map.begin(); it != _spectrum_map.end(); ++it, i++)
    {
      tm_tmp[i]                        = ((*it).first);
      std::vector<mcq_float> &spectrum = (*it).second;
      for(unsigned int k = 0; k < spectrum.size(); k++, j++)
        {
          mat_tmp[j] = spectrum[k];
        }
      // write_spot(the_spot);
    }
  _lmat->_tm = new VecF(_lmat->_tm_vals, tm_tmp);

  _lmat->_mat = new MatF(_lmat->_tm_vals, _lmat->_mz_vals, mat_tmp);
}
