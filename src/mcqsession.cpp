
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file mcqsession.cpp
 * \date April 28, 2018
 * \author Olivier Langella
 * \brief singleton to store general informations on the current MassChroQ
 * session
 */

#include "mcqsession.h"
#include "lib/consoleout.h"
#include "lib/mcq_error.h"
#include <QDebug>

McqSession McqSession::m_instance = McqSession();

McqSession::McqSession()
{
  _m_retention_time_range = 300;
}
/*
rocksdb::DB* McqSession::getRocksDb() {
    //qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    if (!_unassigned_peaks_on_disk) {
        throw mcqError(QString("error in McqSession::getRocksDb() :\nrocks db
not opened"));
    }
    //qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " " <<
m_p_db;
    return m_p_db;
}
*/
McqSession::~McqSession()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // if (m_p_db != nullptr) {
  // m_p_db->close();
  // delete m_p_db;
  //}
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

McqSession &
McqSession::getInstance()
{
  return m_instance;
}

void
McqSession::setTmpDir(const QString &dir_name)
{
  _tmp_dir.setPath(dir_name);
  if(!_tmp_dir.exists())
    {
      throw mcqError(
        QObject::tr("error : cannot find the temporary directory : %1 \n")
          .arg(dir_name));
    }
  else
    {
      QFileInfo tmpDirInfo(_tmp_dir.absolutePath());
      if(!tmpDirInfo.isWritable())
        {
          throw mcqError(
            QObject::tr(
              "error : cannot write to the temporary directory : %1 \n")
              .arg(dir_name));
        }
    }
  if(_sp_session_tmp_dir.get() != nullptr)
    {
      _sp_session_tmp_dir = nullptr;
    }
  if(_sp_session_tmp_dir.get() == nullptr)
    {
      _sp_session_tmp_dir = std::make_shared<QTemporaryDir>(
        QString("%1/mcq_session_").arg(_tmp_dir.absolutePath()));
    }

  setUnassignedPeaksOnDisk(_unassigned_peaks_on_disk);
  QString name = getSessionTmpDirName();
}
pappso::pappso_double
McqSession::getRetentionTimeRange() const
{
  return _m_retention_time_range;
}
const QString
McqSession::getTmpDirName() const
{
  return _tmp_dir.absolutePath();
}

const QString
McqSession::getSessionTmpDirName() const
{
  return _sp_session_tmp_dir.get()->path();
}

void
McqSession::setUnassignedPeaksOnDisk(bool unassigned_peaks_on_disk)
{
  /*
  if (m_p_db== nullptr) {
      delete m_p_db;
      m_p_db = nullptr;
  }
  */
  _unassigned_peaks_on_disk = unassigned_peaks_on_disk;

  if(_unassigned_peaks_on_disk)
    {
      /*
      rocksdb::Options options;
      options.create_if_missing = true;
      options.max_open_files = 32768;
      rocksdb::Status status =
          rocksdb::DB::Open(options,
      QString("%1/rocksdb").arg(getSessionTmpDirName()).toStdString(), &m_p_db);
      if (!status.ok()) {
          throw mcqError(QString("error in McqSession::getRocksDb()
      :\nrocksdb::Status status=%1").arg(status.ToString().c_str()));
      }
      */
      mcqout() << QObject::tr("store unassigned peaks on disk to save memory")
               << endl;
    }
}

bool
McqSession::getUnassignedPeaksOnDisk() const
{
  return _unassigned_peaks_on_disk;
}
