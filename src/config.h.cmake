#ifndef _CONFIG_H
#define _CONFIG_H

#define SOFTWARE_NAME "MassChroQ"
#cmakedefine MASSCHROQ_VERSION "@MASSCHROQ_VERSION@"
#define MASSCHROQ_XSD "masschroq-@MASSCHROQ_SCHEMA_VERSION@.xsd"
#cmakedefine MASSCHROQ_SCHEMA_VERSION "@MASSCHROQ_SCHEMA_VERSION@"
#define MASSCHROQ_SCHEMA_FILE \
  ":/resources/schema/masschroq-@MASSCHROQ_SCHEMA_VERSION@.xsd"
#cmakedefine MASSCHROQ_ICON "@MASSCHROQ_ICON@"
#define QT_V_4_5 0x040500
#define QT_V_4_6 0x040600


#include <QDebug>
#include "mcq_types.h"


#endif /* _CONFIG_H */
