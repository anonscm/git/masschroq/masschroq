/*
 * treatment_box_xic_detect.h
 *
 *  Created on: 19 July 2012
 *      Author: valot
 */

#pragma once

#include "treatment_box.h"
#include <pappsomspp/processing/detection/tracedetectioninterface.h>

class TreatmentBoxXicDetect : public TreatmentBox
{

  Q_OBJECT

  friend class TreatmentChain;

  public:
  void setPeakDetectionBase(pappso::TraceDetectionInterfaceSPtr detect);
  const std::vector<pappso::TracePeak> &
  getAllXicPeak() const
  {
    return (_all_peaks_list);
  }

  signals:
  void detectedPeaks(const TreatmentBoxXicDetect *);

  protected:
  TreatmentBoxXicDetect(const TreatmentChain &chain);
  virtual ~TreatmentBoxXicDetect();

  private:
  void doTreatment();

  pappso::TraceDetectionInterfaceSPtr _p_detect;

  std::vector<pappso::TracePeak> _all_peaks_list;
};
