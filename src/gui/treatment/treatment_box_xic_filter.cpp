/*
 * treatment_box_xic_filter.cpp
 *
 *  Created on: 21 mai 2012
 *      Author: valot
 */

#include "treatment_box_xic_filter.h"
#include "../../lib/xic/xic_base.h"
#include <QDebug>
#include <pappsomspp/pappsoexception.h>

TreatmentBoxXicFilter::TreatmentBoxXicFilter(const TreatmentChain &chain)
  : TreatmentBox(chain)
{
  _p_filter = nullptr;
}

TreatmentBoxXicFilter::~TreatmentBoxXicFilter()
{
}

void
TreatmentBoxXicFilter::setFilter(pappso::FilterInterfaceSPtr filter)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  if(filter != nullptr)
    {
      _p_filter = filter;
      this->updatedAndEmitChanged();
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
TreatmentBoxXicFilter::doTreatment()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(_p_filter == nullptr)
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      return;
      //  throw pappso::PappsoException(QObject::tr("_p_filter == nullptr"));
    }
  if(this->getPrevious() != nullptr)
    {
      pappso::XicCstSPtr previous_filter_sp =
        this->getPrevious()->getCurrentXic();
      if(previous_filter_sp != nullptr)
        {
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                   << " _p_filter";
          pappso::Xic new_xic = pappso::Xic(*previous_filter_sp);
          _p_filter->filter(new_xic);
          _current_xic_sp = new_xic.makeXicSPtr();
          emit changedXic(this);
        }
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}
