/*
 * treatment_box_base.h
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#ifndef TREATMENT_BOX_H_
#define TREATMENT_BOX_H_

#include "../../lib/quanti_items/quantiItemBase.h"
#include "../../lib/quantifications/quantificationMethod.h"
#include <pappsomspp/xic/xic.h>
#include <qobject.h>

class TreatmentChain;

class TreatmentBox : public QObject
{

  Q_OBJECT

  friend class TreatmentChain;

  public:
  const pappso::XicSPtr
  getCurrentXic() const
  {
    return _current_xic_sp;
  }

  signals:
  void onDelete(TreatmentBox *);
  //	void createdXic(const TreatmentBox *) const;
  //	void detectedPeaks(const vector<xicPeak *> *) const;

  protected:
  TreatmentBox(const TreatmentChain &chain);
  virtual ~TreatmentBox();

  void
  setNext(TreatmentBox *next)
  {
    _next = next;
  }

  void
  setPrevious(TreatmentBox *previous)
  {
    _previous = previous;
  }

  TreatmentBox *
  getNext() const
  {
    return _next;
  }

  TreatmentBox *
  getPrevious() const
  {
    return _previous;
  }

  void updatedAndEmitChanged();

  pappso::XicSPtr _current_xic_sp;

  private:
  virtual void doTreatment() = 0;

  TreatmentBox *_previous = nullptr;
  TreatmentBox *_next     = nullptr;
  const TreatmentChain &_chain;
};

#endif /* TREATMENT_BOX_H_ */
