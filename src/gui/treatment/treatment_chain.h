/*
 * treatment_chain.h
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#pragma once

#include "treatment_box.h"
#include "treatment_box_xic_detect.h"
#include "treatment_box_xic_extract.h"
#include "treatment_box_xic_filter.h"
#include <list>

class TreatmentChain
{
  public:
  TreatmentChain();
  virtual ~TreatmentChain();

  void removedTreamentBox(TreatmentBox *toRemoved);

  void removedAll();

  TreatmentBoxXicExtract *addNewTreatmentBoxXicExtract();

  TreatmentBoxXicFilter *addNewTreatmentBoxXicFilter();

  TreatmentBoxXicDetect *addNewTreatmentBoxXicDetect();

  private:
  void updatedTreamentChain();
  std::list<TreatmentBox *> _chain;
};
