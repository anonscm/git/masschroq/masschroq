/*
 * treatment_box_xic_extract.h
 *
 *  Created on: 16 mai 2012
 *      Author: valot
 */

#ifndef TREATMENT_BOX_XIC_EXTRACT_H_
#define TREATMENT_BOX_XIC_EXTRACT_H_

#include "treatment_box.h"

class TreatmentBoxXicExtract : public TreatmentBox
{

  Q_OBJECT

  friend class TreatmentChain;

  public:
  void setQuantiItem(QuantiItemBase *quantiItem,
                     const QuantificationMethod *p_quantification_method);

  const QuantiItemBase *
  getQuantiItem() const
  {
    return _quantiItem;
  };

  signals:
  void createdXic(const TreatmentBoxXicExtract *);

  protected:
  TreatmentBoxXicExtract(const TreatmentChain &chain);
  virtual ~TreatmentBoxXicExtract();

  private:
  void doTreatment();

  QuantiItemBase *_quantiItem                          = nullptr;
  const QuantificationMethod *_p_quantification_method = nullptr;
  MsrunSp _msrun_sp;
};

#endif /* TREATMENT_BOX_XIC_EXTRACT_H_ */
