/*
 * treatment_box_xic_extract.cpp
 *
 *  Created on: 16 mai 2012
 *      Author: valot
 */

#include "treatment_box_xic_extract.h"
#include "../../lib/quanti_items/quantiItemBase.h"
#include "../engine/masschroq_gui_engin.h"

TreatmentBoxXicExtract::TreatmentBoxXicExtract(const TreatmentChain &chain)
  : TreatmentBox(chain)
{
}

TreatmentBoxXicExtract::~TreatmentBoxXicExtract()
{
  if(_quantiItem != nullptr)
    {
      delete(_quantiItem);
    }
}

void
TreatmentBoxXicExtract::setQuantiItem(
  QuantiItemBase *quantiItem,
  const QuantificationMethod *p_quantification_method)
{
  if(quantiItem != 0)
    {
      if(_quantiItem != 0)
        {
          delete(_quantiItem);
        }
      _quantiItem              = quantiItem;
      _p_quantification_method = p_quantification_method;
      this->updatedAndEmitChanged();
    }
}

void
TreatmentBoxXicExtract::doTreatment()
{
 qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  _msrun_sp = MasschroqGuiEngin::getInstance()->getCurrentLoadedMsrun();
  if(_quantiItem != nullptr)
    {
      // extract XIC
      if(_msrun_sp.get() != nullptr)
        {
          _msrun_sp.get()->setMsRunXicExtractorMethod(
            _p_quantification_method->getXicExtractMethod());
          _current_xic_sp =
            pappso::Xic(
              *_msrun_sp.get()->extractXicCstSPtr(
                _p_quantification_method->getMzRange(_quantiItem->getMz())))
              .makeXicSPtr();
          if(_current_xic_sp != nullptr)
            {
              //_currentXic->toStream(std::cout);
              emit createdXic(this);
            }
        }
    }
    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}
