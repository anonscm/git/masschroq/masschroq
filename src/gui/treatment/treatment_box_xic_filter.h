/*
 * treatment_box_xic_filter.h
 *
 *  Created on: 21 mai 2012
 *      Author: valot
 */

#pragma once

#include "treatment_box.h"
#include <pappsomspp/processing/filters/filterinterface.h>

class TreatmentBoxXicFilter : public TreatmentBox
{

  Q_OBJECT

  friend class TreatmentChain;

  public:
  void setFilter(pappso::FilterInterfaceSPtr filter);

  signals:
  void changedXic(const TreatmentBoxXicFilter *);

  protected:
  TreatmentBoxXicFilter(const TreatmentChain &chain);
  virtual ~TreatmentBoxXicFilter();

  private:
  void doTreatment();

  pappso::FilterInterfaceSPtr _p_filter;
};
