/*
 * treatment_box_base.cpp
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#include "treatment_box.h"

TreatmentBox::TreatmentBox(const TreatmentChain &chain) : _chain(chain)
{
  _current_xic_sp = nullptr;
}

TreatmentBox::~TreatmentBox()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  emit onDelete(this);
}

void
TreatmentBox::updatedAndEmitChanged()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  this->doTreatment();
  if(_next != nullptr)
    _next->updatedAndEmitChanged();
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}
