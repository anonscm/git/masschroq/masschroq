/*
 * obiwarp_alignment_widget.cpp
 *
 *  Created on: 31 juil. 2012
 *      Author: valot
 */

#include "obiwarp_alignment_widget.h"
#include "../engine/masschroq_gui_engin.h"
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include <pappsomspp/msfile/msfileaccessor.h>

ObiwarpAlignmentWidget::ObiwarpAlignmentWidget(QWidget *parent)
  : AlignmentWidget(parent),
    LMAT_PRECISION_WINDOW(1),
    MZ_START(500),
    MZ_STOP(1200)
{
  // kill including widget
  setAttribute(Qt::WA_DeleteOnClose);

  _mainLayout = new QVBoxLayout;
  this->addSelectionGroup();
  this->addParameterGroup();
  this->addAlignButton();

  this->setLayout(_mainLayout);

  this->initializeObiwarpMethod();
}

ObiwarpAlignmentWidget::~ObiwarpAlignmentWidget()
{
  qDebug() << "Delete Obiwarp widget";
  this->deleteLoadingThread();
  delete(_alignmentBase);
  delete(_monitor);
}

void
ObiwarpAlignmentWidget::addSelectionGroup()
{
  QGroupBox *_selection_group = new QGroupBox("MsRun selection");
  QVBoxLayout *groupLayout    = new QVBoxLayout;

  _text_edit_ref = new QLineEdit("No MsRun Ref Selected");
  _text_edit_ref->setReadOnly(true);
  groupLayout->addWidget(_text_edit_ref);

  QHBoxLayout *layout   = new QHBoxLayout;
  _loadingMsrunRefLabel = new RunningQLabel(this);
  layout->addWidget(_loadingMsrunRefLabel, 2);
  QPushButton *extractButton1 = new QPushButton(tr("&Load"));
  extractButton1->setDefault(true);
  layout->addWidget(extractButton1, 0);
  groupLayout->addLayout(layout);

  _text_edit_toAligned = new QLineEdit("No MsRun to Aligned Selected");
  _text_edit_toAligned->setReadOnly(true);
  groupLayout->addWidget(_text_edit_toAligned);

  QHBoxLayout *layout2        = new QHBoxLayout;
  _loadingMsruntoALignedLabel = new RunningQLabel(this);
  layout2->addWidget(_loadingMsruntoALignedLabel, 2);
  QPushButton *extractButton2 = new QPushButton(tr("&Load"));
  extractButton2->setDefault(true);
  layout2->addWidget(extractButton2, 0);
  groupLayout->addLayout(layout2);

  _selection_group->setLayout(groupLayout);
  _mainLayout->addWidget(_selection_group);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(extractButton1,
          &QPushButton::clicked,
          this,
          &ObiwarpAlignmentWidget::startLoadindMsrunRef);
  connect(extractButton2,
          &QPushButton::clicked,
          this,
          &ObiwarpAlignmentWidget::startLoadindMsrunAligned);
#else
  // Qt4 code
  connect(
    extractButton1, SIGNAL(clicked()), this, SLOT(startLoadindMsrunRef()));
  connect(
    extractButton2, SIGNAL(clicked()), this, SLOT(startLoadindMsrunAligned()));
#endif
}

void
ObiwarpAlignmentWidget::addParameterGroup()
{
  QGroupBox *_parameter_group = new QGroupBox("Obiwarp Parameters");
  QFormLayout *layout         = new QFormLayout;

  QDoubleSpinBox *lmat_box = new QDoubleSpinBox;
  lmat_box->setMaximum(20);
  lmat_box->setWrapping(true);
  lmat_box->setSingleStep(1);
  lmat_box->setDecimals(0);
  lmat_box->setValue(LMAT_PRECISION_WINDOW);
  layout->addRow(tr("Lmat precision window (Th)"), lmat_box);

  QDoubleSpinBox *mz_start_box = new QDoubleSpinBox;
  mz_start_box->setMaximum(10000);
  mz_start_box->setWrapping(true);
  mz_start_box->setSingleStep(1);
  mz_start_box->setDecimals(2);
  mz_start_box->setValue(MZ_START);
  layout->addRow(tr("m/z Start"), mz_start_box);

  QDoubleSpinBox *mz_stop_box = new QDoubleSpinBox;
  mz_stop_box->setMaximum(10000);
  mz_stop_box->setWrapping(true);
  mz_stop_box->setSingleStep(1);
  mz_stop_box->setDecimals(2);
  mz_stop_box->setValue(MZ_STOP);
  layout->addRow(tr("m/z stop"), mz_stop_box);

  _parameter_group->setLayout(layout);

  _mainLayout->addWidget(_parameter_group);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(lmat_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &ObiwarpAlignmentWidget::setLmatPrecision);

  connect(mz_start_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &ObiwarpAlignmentWidget::setMzStart);

  connect(mz_stop_box,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &ObiwarpAlignmentWidget::setMzStop);
#else
  // Qt4 code
  connect(lmat_box,
          SIGNAL(valueChanged(double)),
          this,
          SLOT(setLmatPrecision(double)));

  connect(
    mz_start_box, SIGNAL(valueChanged(double)), this, SLOT(setMzStart(double)));

  connect(
    mz_stop_box, SIGNAL(valueChanged(double)), this, SLOT(setMzStop(double)));

#endif
}

void
ObiwarpAlignmentWidget::addAlignButton()
{
  QHBoxLayout *layout = new QHBoxLayout;
  _runningQLabel      = new RunningQLabel(this);
  layout->addWidget(_runningQLabel, 2);
  QPushButton *extractButton = new QPushButton(tr("&Align"));
  extractButton->setDefault(true);
  layout->addWidget(extractButton, 0);
  _mainLayout->addLayout(layout);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(extractButton,
          &QPushButton::clicked,
          this,
          &ObiwarpAlignmentWidget::startAlignment);
#else
  // Qt4 code
  connect(extractButton, SIGNAL(clicked()), this, SLOT(startAlignment()));
#endif
}

void
ObiwarpAlignmentWidget::initializeObiwarpMethod()
{
  _monitor       = new MonitorAlignmentBase();
  _alignmentBase = new AlignmentObiwarp(_monitor);
  AlignmentObiwarp *align((AlignmentObiwarp *)_alignmentBase);
  align->setLmatPrecision(LMAT_PRECISION_WINDOW);
  align->setMassStart(MZ_START);
  align->setMassEnd(MZ_STOP);
  _msunToAligned = 0;
  _msrunRef      = 0;
}

void
ObiwarpAlignmentWidget::startLoadindMsrunRef()
{
  if(this->isAlignmentRunning())
    return;

  QString filename = this->getMsrunFilename();
  if(filename.isEmpty())
    return;


  pappso::MsFileAccessor file_access(filename, QString("msfile"));

  pappso::MsRunReaderSPtr run_reader =
    file_access.getMsRunReaderSPtrByRunId("", "msrun");

  MsrunSp msrun =
    std::make_shared<Msrun>(Msrun(run_reader.get()->getMsRunId()));

  // MasschroqGuiEngin::getInstance()->setCurrentMsrunRef(msrun);
  this->triggerLoadingMsRunRef(msrun);
}

void
ObiwarpAlignmentWidget::startLoadindMsrunAligned()
{
  if(this->isAlignmentRunning())
    return;
  QString filename = this->getMsrunFilename();

  if(filename.isEmpty())
    return;


  pappso::MsFileAccessor file_access(filename, QString("msfile"));
  pappso::MsRunReaderSPtr run_reader =
    file_access.getMsRunReaderSPtrByRunId("", "msrun_to_align");
  MsrunSp msrun =
    std::make_shared<Msrun>(Msrun(run_reader.get()->getMsRunId()));
  // MasschroqGuiEngin::getInstance()->setCurrentMsrunToAlign(msrun);
  triggerLoadingMsRunToAlign(msrun);
}

QString
ObiwarpAlignmentWidget::getMsrunFilename()
{
  QString filename =
    QFileDialog::getOpenFileName(this,
                                 tr("Open Msrun File"),
                                 QString::null,
                                 tr("mzXML or mzML files (*.mzXML *.mzML)"));

  if(!filename.isEmpty())
    {

      QFileInfo filenameInfo(filename);

      if(!filenameInfo.exists())
        {
          this->viewError(
            tr("The chosen MS run file '%1', does not exist..\nPlease, change "
               "the read permissions on this file or load another one. ")
              .arg(filename));
          return ("");
        }
      else if(!filenameInfo.isReadable())
        {
          this->viewError(
            tr("The chosen MS run file '%1', is not readable.\nPlease, change "
               "the read permissions on this file or load another one. ")
              .arg(filename));
          return ("");
        }
    }
  return (filename);
}

void
ObiwarpAlignmentWidget::setLmatPrecision(double precision)
{
  AlignmentObiwarp *align((AlignmentObiwarp *)_alignmentBase);
  align->setLmatPrecision(precision);
}

void
ObiwarpAlignmentWidget::setMzStart(double mz)
{
  AlignmentObiwarp *align((AlignmentObiwarp *)_alignmentBase);
  align->setMassStart(mz);
}

void
ObiwarpAlignmentWidget::completDataToMsrun(MsrunSp msrun)
{
  qDebug() << "Complete data to Msrun";
}

void
ObiwarpAlignmentWidget::emitSignalDoneAlignement()
{
  emit newAlignmentCurve();
  emit finishAlignment(_msunToAligned);
}

void
ObiwarpAlignmentWidget::setMzStop(double mz)
{
  AlignmentObiwarp *align((AlignmentObiwarp *)_alignmentBase);
  align->setMassEnd(mz);
}

void
ObiwarpAlignmentWidget::writeElement(MasschroqDomDocument *domDocument) const
{
  AlignmentObiwarp *align((AlignmentObiwarp *)_alignmentBase);
  domDocument->addObiwarpMethod(*align);
}
