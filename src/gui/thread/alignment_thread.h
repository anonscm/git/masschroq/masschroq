/*
 * obiwarp_thread.h
 *
 *  Created on: 31 juil. 2012
 *      Author: valot
 */

#pragma once

#include "../../lib/alignments/alignment_base.h"
#include "../../lib/msrun/msrun.h"
#include "mcqThread.h"
#include <QWaitCondition>

class AlignmentThread : public McqThread
{
  Q_OBJECT

  public:
  AlignmentThread(QObject *parent = 0, const unsigned int maxProgress = 0);
  virtual ~AlignmentThread();

  void performedAlignment(AlignmentBase *alignment,
                          MsrunSp p_msrun_ref,
                          MsrunSp p_msrun);

  void run();

  signals:

  void finishAlignment();

  void errorDuringAlignment(QString error);

  private:
  AlignmentBase *_alignment;
  MsrunSp _p_msrun_ref;
  MsrunSp _p_msrun;
  QMutex _mutex;
  QWaitCondition _condition;
};

