/*
 * msrun_selection_widget.h
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#pragma once

#include "../../lib/msrun/msrun.h"
#include "../masschroQWidget.h"
#include "../runningQLabel.h"
#include "../thread/msrunLoaderThread.h"
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QWidget>

class MsrunSelectionWidget : public MasschroQWidget
{

  Q_OBJECT

  public:
  MsrunSelectionWidget(QWidget *parent = 0);
  virtual ~MsrunSelectionWidget();

  signals:
  void resetData();

  private slots:
  void loadMsrun();
  void doneLoading(MsrunSp msrun);
  void ErrorLoading(QString error);

  private:
  QGroupBox *_selection_group;
  RunningQLabel *_text_message;
  QLineEdit *_text_edit;
  MsrunLoaderThread _msrun_loader_thread;
  MsrunSp _current_msrun;
};
