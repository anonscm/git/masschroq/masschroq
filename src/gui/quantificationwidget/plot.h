/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \file plot.h
 * \date November 23, 2011
 * \author Edlira Nano
 */

#pragma once

#include "../treatment/treatment_box.h"
#include "../treatment/treatment_box_xic_detect.h"
#include "../treatment/treatment_box_xic_extract.h"
#include "../treatment/treatment_box_xic_filter.h"
#include <qcustomplot.h>

class Plot : public QCustomPlot
{
  Q_OBJECT

  public:
  Plot(QWidget *parent);

  virtual ~Plot();

  void clear();

  public slots:
  void viewNewPlot(const TreatmentBoxXicExtract *);
  void updatedCurrentPlot(const TreatmentBoxXicFilter *);
  void updatedPeaks(const TreatmentBoxXicDetect *);
  void remove(TreatmentBox *);
  void clearPlot();

  protected:
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual void mousePressEvent(QMouseEvent *event);
  virtual void mouseReleaseEvent(QMouseEvent *event);
  virtual void keyPressEvent(QKeyEvent *event);
  virtual void keyReleaseEvent(QKeyEvent *event);

  private:
  const QColor getNewColors();

  std::map<const TreatmentBox *, QCPGraph *> _xicPlots;
  std::vector<const TreatmentBox *> m_layer_box;

  //pappso::XicSPtr _p_xic;
  std::vector<QCPGraph *> m_graph_trace_peak_list;
  bool _click       = false;
  bool _control_key = false;
  pappso::pappso_double _old_x;
  pappso::pappso_double _old_y;
  // QCPGraph * _p_graph_peak_border;

  //         QwtSymbol *  _p_symbol;
};

