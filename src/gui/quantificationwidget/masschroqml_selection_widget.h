/*
 * msrun_selection_widget.h
 *
 *  Created on: 24 juil. 2012
 *      Author: valot
 */

#pragma once

#include "../../lib/msrun/msrun.h"
#include "../engine/masschroq_gui_data.h"
#include "../masschroQWidget.h"
#include "../runningQLabel.h"
#include "../thread/masschroqmlLoaderThread.h"
#include "../thread/msrunLoaderThread.h"
#include <QComboBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QWidget>

class MasschroqmlSelectionWidget : public MasschroQWidget
{

  Q_OBJECT

  public:
  MasschroqmlSelectionWidget(QWidget *parent = 0);
  virtual ~MasschroqmlSelectionWidget();

  signals:
  void resetData();
  void updateMasschroqData();
  void updateMsRunData();

  private slots:
  void loadMasschroqml();
  void loadMsrun(int index);
  void doneLoadingMsRun(MsrunSp msrun);
  void doneLoadingMasschroqml(MasschroqGuiData *);
  void ErrorLoadingMasschroqml(QString error);
  void ErrorLoadingMsrun(QString error);

  private:
  QString strippedFilename(const QString &fullFilename);
  void addMasschroqmlSelectionGroup(QVBoxLayout *layout);
  void addMsRunSelectionGroup(QVBoxLayout *layout);
  void viewErrorMessage(QString error);

  RunningQLabel *_masschroqml_message;
  QLineEdit *_masschroqml_edit;
  QComboBox *_msrun_select;
  RunningQLabel *_msrun_message;
  MsrunLoaderThread _msrun_loader_thread;
  MasschroqmlLoaderThread _masschroqml_loader_thread;
};
