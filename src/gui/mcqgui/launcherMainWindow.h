/*
 * launcherMainWindow.h
 *
 *  Created on: 10 sept. 2012
 *      Author: valot
 */

#ifndef LAUNCHERMAINWINDOW_H_
#define LAUNCHERMAINWINDOW_H_

#include "../../lib/mass_chroq.h"
#include "../thread/masschroqRunningThread.h"
#include "guiworkerthread.h"
#include <QLineEdit>
#include <QMainWindow>
#include <QPushButton>
#include <QVBoxLayout>

namespace Ui
{
class GuiMainWindow;
}

class LauncherMainWindow : public QMainWindow
{
  Q_OBJECT

  public:
  explicit LauncherMainWindow(QWidget *parent = 0);
  ~LauncherMainWindow();
  public slots:
  void run();
  signals:
  void operateMassChroqFile(MassChroqRun mcq_run);

  protected:
  void closeEvent(QCloseEvent *event);

  private slots:
  void selectMassChroqFile();
  void selectTempDirectory();
  void startMassChroq();
  void AbordMassChroqRunning();
  void finishRunningMassChroq();
  void viewErrorDuringRunning(QString error);
  void about();
  void setNumberOfCpus(int z);
  void appendLogString(QString log);

  private:
  bool maybeSave();
  void initialiseMassChroqRunningThread();
  void viewError(QString error);

  private:
  MassChroq *_masschroq;
  MasschroqRunningThread *_masschroqRunningThread = nullptr;
  // LogQTextEdit * _logQTextEdit;
  // QPushButton * loadButton;
  // QLineEdit *_masschroqml_edit;
  // QLineEdit *_temp_edit;
  // bool _onlyPeptideParse;
  QDateTime _dt_begin;

  int _number_of_cpus;

  Ui::GuiMainWindow *ui;
  QThread _worker_thread;
};

#endif /* LAUNCHERMAINWINDOW_H_ */
