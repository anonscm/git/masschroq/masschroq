/*
 * masschroq_gui_engin.cpp
 *
 *  Created on: 11 mai 2012
 *      Author: valot
 */

#include "masschroq_gui_engin.h"
#include "../../lib/consoleout.h"
#include "../../lib/mcq_error.h"
#include "../../lib/msrun/msrun.h"
#include <cmath>

// Initialisation du singleton à NULL
MasschroqGuiEngin *MasschroqGuiEngin::_singleton = nullptr;

MasschroqGuiEngin::MasschroqGuiEngin() : _chain()
{
  _masschroqdata = new MasschroqGuiData();
}

MasschroqGuiEngin::~MasschroqGuiEngin()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(_masschroqdata != nullptr)
    delete(_masschroqdata);
  _masschroqdata = nullptr;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

MasschroqGuiEngin *
MasschroqGuiEngin::getInstance()
{
  if(_singleton == nullptr)
    {
      qDebug() << "creating MassChroqGuiEngin";
      _singleton = new MasschroqGuiEngin;
    }
  else
    {
      qDebug() << "MassChroqGuiEngin already exist";
    }
  return _singleton;
}

TreatmentChain &
MasschroqGuiEngin::getChain()
{
  return _chain;
}

MasschroqGuiData *
MasschroqGuiEngin::getMasschroqGuiData() const
{
  return _masschroqdata;
}

MsrunSp
MasschroqGuiEngin::getCurrentLoadedMsrun()
{
  return (_current_msrun_sp);
}
void
MasschroqGuiEngin::setCurrentLoadedMsrun(MsrunSp msrun)
{
  _current_msrun_sp = msrun;
}

void
MasschroqGuiEngin::setMasschroqGuiData(MasschroqGuiData *mcq_gui_data)
{
  // if (_masschroqdata != nullptr)
  //   delete (_masschroqdata);
  _masschroqdata = mcq_gui_data;
}
