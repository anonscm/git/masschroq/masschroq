/*
 * runningQLabel.cpp
 *
 *  Created on: 1 août 2012
 *      Author: valot
 */

#include "runningQLabel.h"

RunningQLabel::RunningQLabel(QWidget *parent) : QLabel(parent)
{
  _timer = new QTimer(this);
  _timer->setInterval(1000);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(_timer, &QTimer::timeout, this, &RunningQLabel::updateRunning);
#else
  // Qt4 code
  connect(_timer, SIGNAL(timeout()), this, SLOT(updateRunning()));
#endif
}

RunningQLabel::~RunningQLabel()
{
  delete(_timer);
}

void
RunningQLabel::startLoading(QString message)
{
  this->setText(message);
  _runningLabel = message;
  _timer->start();
  this->setCursor(Qt::WaitCursor);
}

void
RunningQLabel::stopLoading(QString message)
{
  this->setText(message);
  _timer->stop();
  this->setCursor(Qt::ArrowCursor);
}

void
RunningQLabel::updateRunning()
{
  _countTime++;
  if(_countTime > 4)
    _countTime = 0;
  QString label = _runningLabel;
  for(int i = 0; i < _countTime; i++)
    {
      label.append(" .");
    }
  this->setText(label);
}
