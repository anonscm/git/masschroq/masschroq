/**
 * \file ms_run_hash_group.cpp
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#include "ms_run_hash_group.h"
#include "../consoleout.h"
#include "../quantifications/quantificationMethod.h"
#include <QtConcurrentMap>

msRunHashGroup::msRunHashGroup()
{
  _p_msrun_ref  = nullptr;
  _group_xml_id = "root";
  _ms_run_map   = std::map<QString, MsrunSp>();

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << _ms_run_map.size();
}

msRunHashGroup::msRunHashGroup(const QString group_xml_id)
{
  _p_msrun_ref  = nullptr;
  _group_xml_id = group_xml_id;
  _ms_run_map   = std::map<QString, MsrunSp>();

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << _ms_run_map.size();
}

msRunHashGroup::msRunHashGroup(const msRunHashGroup &other)
{

  _p_msrun_ref  = other._p_msrun_ref;
  _group_xml_id = other._group_xml_id;
  _ms_run_map   = other._ms_run_map;
}
msRunHashGroup::~msRunHashGroup()
{
}

/// delete msruns and clear container
void
msRunHashGroup::clear()
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  _group_xml_id = "";
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  _p_msrun_ref = nullptr;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  _ms_run_map.clear();
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

/// returns true if this msrun group contains the given msrun
bool
msRunHashGroup::containsMsRun(const Msrun *p_msrun) const
{

  auto it =
    std::find_if(_ms_run_map.begin(),
                 _ms_run_map.end(),
                 [p_msrun](const std::pair<QString, MsrunSp> &pair_msrun) {
                   return (pair_msrun.second.get() == p_msrun);
                 });
  if(it == _ms_run_map.end())
    return false;
  return true;
}

/// returns a pointer to the msrun with the given id if it is part of
/// this group, otherwise NULL is returned
Msrun *
msRunHashGroup::getMsRun(const QString &idname) const
{
  auto it = _ms_run_map.find(idname);
  if(it == _ms_run_map.end())
    {
      return (nullptr);
    }
  else
    {
      return (it->second.get());
    }
}
MsrunSp
msRunHashGroup::getMsRunSp(const QString &idname) const
{

  auto it = _ms_run_map.find(idname);
  if(it == _ms_run_map.end())
    {
      MsrunSp msrun;
      return (msrun);
    }
  else
    {
      return (it->second);
    }
}
/// adds the given msrun to this group (if it is not already in)
/// the first msrun setted here will be the reference one
void
msRunHashGroup::addMsRun(MsrunSp p_msrun)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << p_msrun->getMsRunIdCstSPtr()->getXmlId();
  const QString idname(p_msrun->getMsRunIdCstSPtr()->getXmlId());
  auto ret = _ms_run_map.insert(std::pair<QString, MsrunSp>(idname, p_msrun));
  if(ret.second == true)
    {
      // OK this key does not exist
    }
  else
    {
      throw mcqError(
        QObject::tr("error in msRunHashGroup::setMsRun :\nthe msrun "
                    "with id %1 already exists")
          .arg(idname));
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << p_msrun->getMsRunIdCstSPtr()->getXmlId();
}

void
msRunHashGroup::setReferenceMsrun(const QString &ref_msrun_id)
{
  Msrun *ref_msrun = this->getMsRun(ref_msrun_id);
  if(ref_msrun == nullptr)
    {
      /// we have a problem
      throw mcqError(
        QObject::tr("error in msRunHashGroup::setReferenceMsrun :\nthe msrun "
                    "with id %1 does not exist or is not part of group %2")
          .arg(ref_msrun_id, _group_xml_id));
    }
  else
    {
      _p_msrun_ref = ref_msrun;
    }
}

const Msrun *
msRunHashGroup::getReferenceMsrun() const
{
  return (_p_msrun_ref);
}

struct mapAlign
{
  Msrun *_p_msrun_ref;
  AlignmentBase &_alignment_method;
  msRunHashGroup *_p_group;

  mapAlign(Msrun *msrun_ref,
           AlignmentBase &alignment_method,
           msRunHashGroup *p_group)
    : _alignment_method(alignment_method)
  {
    _p_msrun_ref = msrun_ref;
    _p_group     = p_group;
  }

  typedef int result_type;
  int
  operator()(MsrunSp p_msrun_to_align) const
  {
    try
      {

        qDebug() << "mapAlign begin";
        _alignment_method.alignTwoMsRuns(_p_group, p_msrun_to_align);
        qDebug() << "mapAlign end";
      }
    catch(mcqError &errorException)
      {
        // QObject::tr("%1 CPUs used").arg(cpu_number)
        // mcqerr() << "Oops! an error occurred in MassChroQ. Dont Panic :" <<
        // endl;
        // mcqerr() << error.qwhat() << endl;
        qDebug() << " int mapAlign::operator() " << errorException.qwhat();
        // windaube_exit();
        // return 1;
      }
    return 1;
  }
};
/// aligns the msruns of this group (one by one, towards the reference msrun)
void
msRunHashGroup::alignMsRuns(AlignmentBase &alignment_method)
{
  if(_ms_run_map.size() > 1)
    {

      // warperize_now.setRef(*p_ref_msrun);
      // p_ref_msrun->sleepOnDisk();

      alignment_method.printInfos(mcqout());

      for(auto it = _ms_run_map.begin(); it != _ms_run_map.end(); ++it)
        {
          alignment_method.alignTwoMsRuns(this, it->second);
        }

      alignment_method.clean();
    }
  else
    {
      mcqout() << "WARNING : Skipping alignment in group '"
               << (this->_group_xml_id)
               << "' : not enough msruns in this group.";
    }
}

std::size_t
msRunHashGroup::size() const
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return _ms_run_map.size();
}

/// detects peaks and quantifies
// void
// msRunHashGroup::detectAndQuantify(QuantificationMethod * quanti_method,
// 				  const vector<const QuantiItemBase *> *
// quanti_items,
// 				  MonitorBase & monitor) const {

//   qDebug() << "msRunHashGroup::detectAndQuantify begin on "
// 	   << this->_group_xml_id;

//   monitor.setCurrentGroup(this);

//   Msrun * current_msrun;
//   msRunHashGroup::const_iterator itmsrun;
//   unsigned int count_msruns(1);

//   for (itmsrun = this->begin();
//        itmsrun != this->end();
//        ++itmsrun, ++count_msruns)
//     {
//       current_msrun = itmsrun->second;

//       cout << "\tQuantifying in MS run '"
// 	   << (current_msrun->getXmlId()).toStdString()
// 	   << "' : " << count_msruns << "/" << this->size()
// 	   << " in group '" << _group_xml_id.toStdString()
// 	   << "'" << endl;

//       monitor.setCurrentMsrun(current_msrun);

//       QString quanti_type;
//       current_msrun->quantify(quanti_method,
// 			      quanti_items,
// 			      monitor);

//       monitor.setEndCurrentMsrun();
//     }
//   monitor.setEndCurrentGroup();

//   qDebug() << "void msRunHashGroup::detectAndQuantify end on "
// 	   << this->_group_xml_id;
// }
