
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file lib/peak_collections/alignedpeakcollectionrocksdb.cpp
 * \date January 06, 2018
 * \author Olivier Langella
 * \brief handles a collection of aligned peaks per MSruns on disk
 */

#include "alignedpeakcollectionrocksdb.h"

#include "../../mcqsession.h"
#include "../consoleout.h"
#include "../mass_chroq.h"
#include "../quanti_items/quantiItemBase.h"
#include <QDataStream>
#include <rocksdb/db.h>

AlignedPeakCollectionRocksDb::AlignedPeakCollectionRocksDb(
  const QuantiItemBase *p_quanti_item_base)
{
  _p_quanti_item_base = p_quanti_item_base;
}

AlignedPeakCollectionRocksDb::~AlignedPeakCollectionRocksDb()
{
}

AlignedPeakCollectionRocksDb::AlignedPeakCollectionRocksDb(
  const AlignedPeakCollectionRocksDb &other)
{
  _p_quanti_item_base = other._p_quanti_item_base;
}
void
AlignedPeakCollectionRocksDb::add(const Msrun *p_current_msrun,
                                  AlignedXicPeakSp &xic_peak_sp)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(_p_msrun_list.size() > 0)
    {
      if(_p_msrun_list.back() == p_current_msrun)
        {
        }
      else
        {
          if(std::find(_p_msrun_list.begin(),
                       _p_msrun_list.end(),
                       p_current_msrun) != _p_msrun_list.end())
            {
              throw mcqError(
                QString("error in AlignedPeakCollectionRocksDb::add "
                        ":\nMS run already exists %1")
                  .arg(p_current_msrun->getXmlId()));
            }
          _p_msrun_list.push_back(p_current_msrun);
        }
    }
  else
    {
      _p_msrun_list.push_back(p_current_msrun);
    }
  _tmp_aligned_peak_list.push_back(xic_peak_sp);

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
AlignedPeakCollectionRocksDb::clear(const Msrun *p_current_msrun)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  _tmp_aligned_peak_list.clear();

  QString key(QString("%1-%2")
                .arg(_p_quanti_item_base->getMzId())
                .arg(p_current_msrun->getXmlId()));
  rocksdb::Status s = McqSession::getInstance().getRocksDb()->Delete(
    rocksdb::WriteOptions(), key.toStdString());
  if(!s.ok())
    {
      throw mcqError(QString("error in AlignedPeakCollectionRocksDb::clear "
                             ":\nrocksdb::Status status=%1")
                       .arg(s.ToString().c_str()));
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

std::vector<const Msrun *>
AlignedPeakCollectionRocksDb::getMsRunList()
{
  return _p_msrun_list;
}
std::vector<AlignedXicPeakSp> &
AlignedPeakCollectionRocksDb::getMsRunAlignedPeakList(const Msrun *p_msrun)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  _tmp_aligned_peak_list.clear();
  QString key(QString("%1-%2")
                .arg(_p_quanti_item_base->getMzId())
                .arg(p_msrun->getXmlId()));
  std::string value;
  rocksdb::Status s = McqSession::getInstance().getRocksDb()->Get(
    rocksdb::ReadOptions(), key.toStdString(), &value);
  if(s.IsNotFound())
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " key not found:" << key;
      return _tmp_aligned_peak_list;
    }
  if(!s.ok())
    {
      throw mcqError(
        QString("error in "
                "AlignedPeakCollectionRocksDb::getMsRunAlignedPeakList "
                ":\nrocksdb::Status status=%1")
          .arg(s.ToString().c_str()));
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " value.size()=" << value.size();
  QByteArray in_buffer = QByteArray(value.c_str(), value.size());
  QDataStream instream(&in_buffer, QIODevice::ReadOnly);
  QString msrun_id;
  quint32 vector_size;

  while(!instream.atEnd())
    {
      instream >> msrun_id;
      instream >> vector_size;
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " vector_size=" << vector_size;

      for(quint32 i = 0; i < vector_size; i++)
        {

          if(instream.status() != QDataStream::Ok)
            {
              throw mcqError(
                QString("error in "
                        "AlignedPeakCollectionRocksDb::getMsRunAlignedPeakList "
                        ":\nread datastream failed status=%1")
                  .arg(instream.status()));
            }
          pappso::XicPeak peak;
          AlignedXicPeak mcq_peak(peak, p_msrun);
          if(mcq_peak.unserialize(&instream))
            {
              if(msrun_id == p_msrun->getXmlId())
                {
                  _tmp_aligned_peak_list.push_back(
                    std::make_shared<const AlignedXicPeak>(mcq_peak));
                }
            }
        }
      if(instream.status() != QDataStream::Ok)
        {
          throw mcqError(
            QString("error in "
                    "AlignedPeakCollectionRocksDb::getMsRunAlignedPeakList "
                    ":\nread datastream failed status=%1")
              .arg(instream.status()));
        }
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " _tmp_aligned_peak_list.size()="
           << _tmp_aligned_peak_list.size();
  return _tmp_aligned_peak_list;
}

void
AlignedPeakCollectionRocksDb::endMsrunQuantification(
  const Msrun *p_current_msrun)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // flush tmp vector content in file
  if(_tmp_aligned_peak_list.size() > 0)
    {
      if(_p_msrun_list.back() != p_current_msrun)
        {
          throw mcqError(
            QString("error in "
                    "AlignedPeakCollectionRocksDb::endMsrunQuantification "
                    ":\n_p_msrun_list.back() != p_current_msrun %1!=2")
              .arg(_p_msrun_list.back()->getXmlId())
              .arg(p_current_msrun->getXmlId()));
        }
      QByteArray buffer;
      QDataStream outstream(&buffer, QIODevice::WriteOnly);
      outstream << p_current_msrun->getXmlId();
      outstream << (quint32)_tmp_aligned_peak_list.size();
      for(AlignedXicPeakSp xic_peak_sp : _tmp_aligned_peak_list)
        {
          xic_peak_sp->serialize(&outstream);
          // outstream << *(xic_peak_sp.get());
        }
      std::string data(buffer.constData(), buffer.size());
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " data=" << data << " buffer.size()=" << buffer.size();
      if(buffer.size() == 0)
        {
          throw mcqError(
            QString("error in "
                    "AlignedPeakCollectionRocksDb::"
                    "endMsrunQuantification :\nbuffer.size() == 0"));
        }
      QString key(QString("%1-%2")
                    .arg(_p_quanti_item_base->getMzId())
                    .arg(p_current_msrun->getXmlId()));

      rocksdb::Status s = McqSession::getInstance().getRocksDb()->Put(
        rocksdb::WriteOptions(), key.toStdString(), data);
      if(!s.ok())
        {
          throw mcqError(
            QString("error in "
                    "AlignedPeakCollectionRocksDb::endMsrunQuantification "
                    ":\nrocksdb::Status status=%1")
              .arg(s.ToString().c_str()));
        }
    }
  _tmp_aligned_peak_list.clear();

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}
