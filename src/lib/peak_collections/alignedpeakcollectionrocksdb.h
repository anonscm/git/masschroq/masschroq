
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

/**
 * \file lib/peak_collections/alignedpeakcollectionrocksdb.h
 * \date January 06, 2018
 * \author Olivier Langella
 * \brief handles a collection of aligned peaks per MSruns on disk using rocksdb
 */

#ifndef ALIGNEDPEAKCOLLECTIONROCKSDB_H
#define ALIGNEDPEAKCOLLECTIONROCKSDB_H
#include "alignedpeakcollectionbase.h"

class AlignedPeakCollectionRocksDb : public AlignedPeakCollectionBase
{
  public:
  AlignedPeakCollectionRocksDb(const QuantiItemBase *p_quanti_item_base);
  AlignedPeakCollectionRocksDb(const AlignedPeakCollectionRocksDb &other);
  virtual ~AlignedPeakCollectionRocksDb();

  virtual void add(const Msrun *p_current_msrun,
                   AlignedXicPeakSp &xic_peak_sp) override;
  virtual void clear(const Msrun *p_current_msrun) override;
  virtual std::vector<const Msrun *> getMsRunList() override;
  virtual std::vector<AlignedXicPeakSp> &
  getMsRunAlignedPeakList(const Msrun *p_msrun) override;

  /** \brief signal of the end of quantification on this MSrun*/
  virtual void endMsrunQuantification(const Msrun *p_current_msrun);

  private:
  std::vector<const Msrun *> _p_msrun_list;

  const QuantiItemBase *_p_quanti_item_base;
  std::vector<AlignedXicPeakSp> _tmp_aligned_peak_list;
};

#endif // ALIGNEDPEAKCOLLECTIONROCKSDB_H
