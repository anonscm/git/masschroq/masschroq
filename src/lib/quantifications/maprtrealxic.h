
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#ifndef MAPRTREALXIC_H
#define MAPRTREALXIC_H

#include "../quanti_items/quantiitemrtdeterminator.h"
#include "../quantificator.h"
#include <pappsomspp/xic/detection/xicdetectioninterface.h>
#include <pappsomspp/xic/filters/xicfilterinterface.h>

struct MapRtRealXic
{
  Msrun *_p_msrun;
  McqXicType _xic_type;
  const pappso::XicFilterInterface *_p_xic_filter;
  const pappso::XicDetectionInterface *_p_detection;
  McqMatchingMode _matching_mode;
  Quantificator *_quantificator;

  MapRtRealXic(Msrun *current_msrun,
               McqXicType xic_type,
               Quantificator *quantificator,
               const pappso::XicFilterInterface *p_xic_filter,
               const pappso::XicDetectionInterface *p_detection);

  MapRtRealXic(const MapRtRealXic &copy);

  virtual ~MapRtRealXic();

  void operator()(QuantiItemRtDeterminator &rt_determinator);
};

#endif // MAPRTREALXIC_H
