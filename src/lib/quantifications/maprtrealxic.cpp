
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "maprtrealxic.h"

#include "../quanti_items/quantiItemBase.h"
//#include "../xic/xic_base.h"
#include "../consoleout.h"
#include "../xic/xicfilterdetectrealrt.h"
#include "../xic/xictracebase.h"

MapRtRealXic::MapRtRealXic(Msrun *current_msrun,
                           McqXicType xic_type,
                           Quantificator *quantificator,
                           const pappso::XicFilterInterface *p_xic_filter,
                           const pappso::XicDetectionInterface *p_detection)
  : _xic_type(xic_type)
{
  _p_msrun       = current_msrun;
  _p_xic_filter  = p_xic_filter;
  _p_detection   = p_detection;
  _matching_mode = quantificator->getMatchingMode();
  _quantificator = quantificator;
}

MapRtRealXic::MapRtRealXic(const MapRtRealXic &copy) : _xic_type(copy._xic_type)
{
  _p_msrun       = copy._p_msrun;
  _p_xic_filter  = copy._p_xic_filter;
  _p_detection   = copy._p_detection;
  _matching_mode = copy._matching_mode;
  _quantificator = copy._quantificator;
}

MapRtRealXic::~MapRtRealXic()
{
}

void
MapRtRealXic::operator()(QuantiItemRtDeterminator &rt_determinator)
{
  try
    {

      XicFilterDetectRealRt filter_detect_real_rt_on_xic(_p_xic_filter,
                                                         _p_detection);
      McqXicNoConstSp _p_xic = _p_msrun->extractXic(
        _xic_type,
        _quantificator->getXicExtractionMethod()->getPappsoMassRange(
          rt_determinator.getMz()));

      if(_p_xic != nullptr)
        {
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

          filter_detect_real_rt_on_xic.filterDetect(
            _p_xic.get(), &rt_determinator, _p_msrun);
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
        }
      else
        {
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                   << " _p_xic == nullptr";
        }
    }
  catch(mcqError &errorException)
    {
      // QObject::tr("%1 CPUs used").arg(cpu_number)
      // mcqerr() << "Oops! an error occurred in MassChroQ. Dont Panic :" <<
      // endl; mcqerr() << error.qwhat() << endl;
      qDebug() << " MapRtRealXic::operator " << errorException.qwhat();
      // windaube_exit();
      // return 1;
    }
}
