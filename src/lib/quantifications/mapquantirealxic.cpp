
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "mapquantirealxic.h"

#include "../quanti_items/quantiItemBase.h"
#include "../xic/xicfilterdetectmatchrealrt.h"
//#include "../xic/xic_base.h"
#include "../consoleout.h"
#include "../xic/xictracebase.h"

MapQuantiRealXic::MapQuantiRealXic(
  MonitorSpeedInterface &monitor,
  Msrun *current_msrun,
  Quantificator *quantificator,
  pappso::FilterInterfaceCstSPtr p_xic_filter,
  pappso::TraceDetectionInterfaceCstSPtr p_detection)
  : _monitor(monitor)
{
  _p_msrun       = current_msrun;
  _p_xic_filter  = p_xic_filter;
  _p_detection   = p_detection;
  _matching_mode = quantificator->getMatchingMode();
  _quantificator = quantificator;
}

MapQuantiRealXic::MapQuantiRealXic(const MapQuantiRealXic &copy)
  : _monitor(copy._monitor)
{
  _p_msrun       = copy._p_msrun;
  _p_xic_filter  = copy._p_xic_filter;
  _p_detection   = copy._p_detection;
  _matching_mode = copy._matching_mode;
  _quantificator = copy._quantificator;
}

MapQuantiRealXic::~MapQuantiRealXic()
{
}

void
MapQuantiRealXic::operator()(QuantiItemBase *p_currentSearchItem)
{
  try
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      const std::vector<MsMsRtIntensity> rt_list =
        p_currentSearchItem->getPeptide()->getPeptideRtSp()->getObservedRtList(
          _p_msrun);

      if(rt_list.size() > 0)
        {
          // MSMS observed
          // idea : add weighted mean retention time to the list ?

          XicTraceBase *p_xic_trace =
            p_currentSearchItem->newXicTrace(_quantificator, _p_msrun, "MSMS");
          // this was observed in MS/MS in this MS run

          // unsigned int current_z = *(p_currentSearchItem->getZ());
          // qDebug() << " MapQuantiRealXic::operator current_z=" << current_z;
          // std::vector<MsMsRtIntensity> rt_list_z(*p_rt_list);

          // std::remove_if( rt_list_z.begin(),rt_list_z.end(),
          // [current_z](const MsMsRtIntensity & rt) {
          //    return (rt.z != current_z);
          //});
          // qDebug() << " MapQuantiRealXic::operator rt_list_z.size()=" <<
          // rt_list_z.size();
          // if (rt_list_z.size() > 0) {
          // and it was observed with this charge
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          XicFilterDetectMatchRealRt filter_detect_match_real_xic(
            p_xic_trace,
            _monitor,
            _p_xic_filter,
            _p_detection,
            _quantificator,
            _matching_mode);
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          pappso::MzRange mass_range(
            p_currentSearchItem->getMz(),
            _quantificator->getQuantificationMethod()->getLowerPrecision(),
            _quantificator->getQuantificationMethod()->getUpperPrecision());
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

          pappso::XicCstSPtr msrun_xic_sp =
            _p_msrun->extractXicCstSPtr(mass_range, rt_list[0].rt);

          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          if(msrun_xic_sp != nullptr)
            {
              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

              filter_detect_match_real_xic.filterDetectQuantify(
                msrun_xic_sp.get(), &rt_list, p_currentSearchItem, _p_msrun);
              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

              if(p_xic_trace != nullptr)
                {
                  p_xic_trace->setMsMsRtList(rt_list);
                  p_xic_trace->setXic(mass_range, msrun_xic_sp);
                  p_xic_trace->close();
                  delete p_xic_trace;
                }
            }
          else
            {
              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
            }

          p_currentSearchItem->endMsrunQuantification(_p_msrun);
          // }
          // else {
          // this quanti item was not observed with this charge
          //}
        }
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    }
  catch(mcqError &errorException)
    {
      // QObject::tr("%1 CPUs used").arg(cpu_number)
      // mcqerr() << "Oops! an error occurred in MassChroQ. Dont Panic :" <<
      // endl; mcqerr() << error.qwhat() << endl;
      qDebug() << " MapQuantiRealXic::operator " << errorException.qwhat();
      // windaube_exit();
      // return 1;
    }
}
