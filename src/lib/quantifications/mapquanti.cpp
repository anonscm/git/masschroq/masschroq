
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "mapquanti.h"
#include "../consoleout.h"
#include "../quanti_items/quantiItemBase.h"
#include "../xic/xicfilterdetectmatch.h"
#include "../xic/xicfilterdetectmatchrealrt.h"
#include <cmath>
#include <pappsomspp/pappsoexception.h>

MapQuanti::MapQuanti(MonitorSpeedInterface &monitor,
                     Msrun *current_msrun,
                     Quantificator *quantificator,
                     pappso::FilterInterfaceCstSPtr p_xic_filter,
                     pappso::TraceDetectionInterfaceCstSPtr p_detection)
  : _monitor(monitor)
{
  _p_msrun       = current_msrun;
  _p_xic_filter  = p_xic_filter;
  _p_detection   = p_detection;
  _matching_mode = quantificator->getMatchingMode();
  _quantificator = quantificator;
}

void
MapQuanti::operator()(QuantiItemBase *p_currentSearchItem)
{
  qDebug();
  try
    {
      pappso::pappso_double rt_target = -1;
      // std::vector<MsMsRtIntensity> rt_list;

      PeptideRt *p_peptide_rt = p_currentSearchItem->getPeptideRtSp().get();

      if(p_peptide_rt != nullptr)
        {

          rt_target = p_peptide_rt->getRtTarget(_p_msrun, _matching_mode);


          if(std::isnan(rt_target))
            {
              qDebug() << " isnan(rt_target) "
                       << p_currentSearchItem->getQuantiItemId() << " "
                       << rt_target;
              throw mcqError(
                QObject::tr(
                  "Error in MapQuanti::operator() isnan(rt_target) %1")
                  .arg(p_currentSearchItem->getQuantiItemId()));
            }
        }

      if(rt_target == -1)
        {
          if(_matching_mode == McqMatchingMode::post_matching)
            {
              // it's Ok, just detect and keep peaks
            }
          else
            {
              qDebug() << " rt_target == -1 exit";
              return;
            }
        }
      qDebug() << p_currentSearchItem->getMz();
      pappso::MzRange mass_range(
        p_currentSearchItem->getMz(),
        _quantificator->getQuantificationMethod()->getLowerPrecision(),
        _quantificator->getQuantificationMethod()->getUpperPrecision());
      pappso::XicCstSPtr msrun_xic_sp;
      if(rt_target == -1)
        {
          if((_matching_mode == McqMatchingMode::no_matching) &&
             (p_peptide_rt != nullptr))
            {
              // don't extract peaks : no match between runs
            }
          else
            {
              msrun_xic_sp = _p_msrun->extractXicCstSPtr(mass_range);
            }
        }
      else
        {
          msrun_xic_sp = _p_msrun->extractXicCstSPtr(mass_range, rt_target);
        }

      if(msrun_xic_sp == nullptr)
        {
          qDebug();
        }
      else
        {
          qDebug() << " rt_target=" << rt_target;
          XicTraceBase *p_xic_trace = nullptr;

          // only take an rt reference to match peak
          p_xic_trace = p_currentSearchItem->newXicTrace(
            _quantificator, _p_msrun, "target");

          XicFilterDetectMatch filter_detect_match(p_xic_trace,
                                                   _monitor,
                                                   _p_xic_filter.get(),
                                                   _p_detection.get(),
                                                   _matching_mode);
          filter_detect_match.filterDetectQuantify(
            *(msrun_xic_sp.get()), rt_target, p_currentSearchItem, _p_msrun);

          if(p_xic_trace != nullptr)
            {
              p_xic_trace->setAlignedRtTarget(
                _p_msrun->getAlignedRtByOriginalRt(rt_target));
              p_xic_trace->setRtTarget(rt_target);
              p_xic_trace->setXic(mass_range, msrun_xic_sp);
              p_xic_trace->close();
              delete p_xic_trace;
            }
          p_currentSearchItem->endMsrunQuantification(_p_msrun);
        }
      qDebug();
    }

  catch(pappso::PappsoException &errorp)
    {
      mcqerr() << " MapQuanti::operator " << errorp.qwhat();
      throw mcqError(
        QObject::tr(
          "Error in MapQuanti::operator() pappso::PappsoException:\n %1")
          .arg(errorp.qwhat()));
    }

  catch(std::exception &e)
    {
      mcqerr() << " MapQuanti::operator " << e.what();
      throw mcqError(
        QObject::tr("Error in MapQuanti::operator() std::exception :\n %1")
          .arg(e.what()));
    }
}
