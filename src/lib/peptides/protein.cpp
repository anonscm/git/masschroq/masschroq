/**
 * \file protein.cpp
 * \date 10 nov. 2009
 * \author Olivier Langella
 */

#include "protein.h"

Protein::Protein(const QString &protein_xml_id)
  : _protein_xml_id(protein_xml_id)
{
}

Protein::Protein(const Protein &other) : _protein_xml_id(other._protein_xml_id)
{
  _description = other._description;
}
Protein::~Protein()
{
}

void
Protein::setDescription(const QString &description)
{
  _description = description;
}
