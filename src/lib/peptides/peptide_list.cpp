/**
 * \file peptide_list.cpp
 * \date 27 oct. 2009
 * \author Olivier Langella
 */

#include "peptide_list.h"
#include "../msrun/ms_run_hash_group.h"
#include "peptide.h"
#include "peptidert.h"

PeptideList::PeptideList()
{
  // TODO Auto-generated constructor stub
}

PeptideList::~PeptideList()
{
}

/// delete peptides and clear container
void
PeptideList::free()
{
  std::vector<Peptide *>::const_iterator it;
  for(it = this->begin(); it != this->end(); ++it)
    {
      delete(*it);
    }
  this->clear();
}

/// given e peptide id, try to get it from the PeptideList if it is in,
/// return a NULL pointer otherwise
const Peptide *
PeptideList::getPeptide(const QString &pepid) const
{
  std::vector<Peptide *>::const_iterator it;
  for(it = this->begin(); it != this->end(); ++it)
    {
      if((*it)->getXmlId() == pepid)
        return *it;
    }
  return (nullptr);
}

/// tests if the PeptideList contains a given peptide or not
bool
PeptideList::containsPeptide(const Peptide *pep) const
{
  std::vector<Peptide *>::const_iterator it;
  for(it = this->begin(); it != this->end(); ++it)
    {
      if(*it == pep)
        return true;
    }
  return false;
}

/// creates a new PeptideList containing only the peptides observed in the
/// given msrun group
const PeptideList
PeptideList::getPeptideListObservedInMsGroup(const msRunHashGroup &group) const
{
  PeptideList list_pep;
  std::vector<Peptide *>::const_iterator itpeplist;
  for(itpeplist = this->begin(); itpeplist != this->end(); ++itpeplist)
    {
      if((*itpeplist)->isObservedIn(group))
        {
          (*itpeplist)->setPeptideRtSp(std::make_shared<PeptideRt>());
          list_pep.push_back(*itpeplist);
        }
    }
  return (list_pep);
}

/// creates a new PeptideList containing only the peptides observed in the
/// given msrun
const PeptideList
PeptideList::getPeptideListObservedInMsrun(const Msrun *msrun) const
{

  PeptideList list_pep;
  std::vector<Peptide *>::const_iterator itpeplist;
  for(itpeplist = this->begin(); itpeplist != this->end(); ++itpeplist)
    {
      if((*itpeplist)->isObservedIn(msrun))
        {
          list_pep.push_back(*itpeplist);
        }
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " list_pep.size()=" << list_pep.size();
  return (list_pep);
}
