/**
 * \file mass_chroq.cpp
 * \date September 18, 2009
 * \author Olivier Langella
 */

#include "mass_chroq.h"
#include "mcq_error.h"
//#include "msrun/msrun_classic.h"
//#include "msrun/msrun_sliced.h"
#include "../lib/peptides/peptide_isotope.h"
#include "../mcqsession.h"
#include "../pepfileparser/masschroq_dom_engine.h"
#include "../saxparsers/masschroqmlParser.h"
#include "./consoleout.h"

#include <QFileInfo>
#include <math.h>
#include <pappsomspp/pappsoexception.h>

#if QT_VERSION >= QT_V_4_6
#include "../mcqsession.h"
#include "messageHandler.h"
#include "share/utilities.h"
#include <QUrl>
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#endif

QDateTime MassChroq::begin_date_time;

/// set the beginning of execution date and time member _begin_date_time
/// the main method sets it
void
MassChroq::setBeginDateTime(const QDateTime &dt_begin)
{
  begin_date_time = dt_begin;
}

QDateTime
MassChroq::getBeginDateTime()
{
  return begin_date_time;
}

MassChroq::MassChroq()
{
  qDebug() << "MassChroq::MassChroq begin";
  //_output_stream = new QTextStream(stdout, QIODevice::WriteOnly);
}

MassChroq::~MassChroq()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // McqSession::getInstance().setUnassignedPeaksOnDisk(false);

  // delete (_output_stream);

  // delete isotopes
  std::map<QString, const IsotopeLabel *>::iterator iti;
  for(iti = _map_p_isotope_labels.begin(); iti != _map_p_isotope_labels.end();
      ++iti)
    {
      delete(iti->second);
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  // delete peptides
  _peptide_list.free();

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  // delete alignment methods
  std::map<const QString, AlignmentBase *>::iterator ita;
  for(ita = _alignment_methods.begin(); ita != _alignment_methods.end(); ++ita)
    {
      delete(ita->second);
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  // //delete quantification methods
  std::map<const QString, QuantificationMethod *>::iterator itq;
  for(itq = _quantification_methods.begin();
      itq != _quantification_methods.end();
      ++itq)
    {
      delete(itq->second);
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  // delete all msruns
  _ms_run_collection.clear();

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  // delete groups
  std::map<QString, msRunHashGroup *>::iterator itg;
  for(itg = _groups.begin(); itg != _groups.end(); ++itg)
    {
      delete(itg->second);
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

/// Set the XML input filename and verify if it exists and if its readable
void
MassChroq::setXmlFilename(QString &fileName)
{
  _param_file_info.setFile(fileName);
  if(!_param_file_info.exists())
    {
      throw mcqError(
        QObject::tr("error : cannot find the XML input file : '%1' \n")
          .arg(fileName));
    }
  else if(!_param_file_info.isReadable())
    {
      throw mcqError(
        QObject::tr("error : cannot read the XML input file : '%1' \n")
          .arg(fileName));
    }
  // Defined current directory to be compatible with relative path on
  // MassChroqML
  QDir::setCurrent(_param_file_info.absolutePath());
  qDebug() << "Current Directory is " + _param_file_info.absolutePath();
}

void
MassChroq::setMasschroqDir(const QString &masschroq_dir_path)
{
  _masschroq_dir_path = masschroq_dir_path;
}

/**
 * This method validates both XSD schema and XML input file against it
 */

bool
MassChroq::validateXmlFile()
{
  qDebug() << "MassChroq::validateXmlFile begin : Qt local version "
           << QT_VERSION_STR << ", must be at least 4.6";
  mcqout() << QObject::tr("validating MassChroqML file %1...")
                .arg(_param_file_info.absoluteFilePath())
           << endl;

  QFile schema_file(MASSCHROQ_SCHEMA_FILE);
  if(!QFile::exists(MASSCHROQ_SCHEMA_FILE))
    {
      qDebug() << "MassChroQ XSD schema file '%1' does not exist.";
      throw mcqError(
        QObject::tr("MassChroQ XSD schema '%1' not found at '%2'.\n")
          .arg(MASSCHROQ_XSD)
          .arg(MASSCHROQ_SCHEMA_FILE));
    }
  if(!schema_file.open(QIODevice::ReadOnly))
    {
      throw mcqError(
        QObject::tr("error opening the file of the XSD schema : %1 \n Try make "
                    "install to install the schema file. \n")
          .arg(schema_file.fileName()));
    }
  qDebug() << "MassChroq::validateXmlFile enter >= 4.6";
  QXmlSchema schema;
  MessageHandler messHandler;
  schema.setMessageHandler(&messHandler);
  /// load verifies if the schema is valid before loading it
  // attention : if schema is invalid the behaviour is undefined (Qt)
  if(schema.load(&schema_file, QUrl::fromLocalFile(schema_file.fileName())))
    {
      QFile file(_param_file_info.filePath());
      file.open(QIODevice::ReadOnly);
      QXmlSchemaValidator validator(schema);
      if(validator.validate(&file, QUrl::fromLocalFile(file.fileName())))
        {
          qDebug() << "MassChroq::validateXmlFile instance document is valid";
          file.close();
          schema_file.close();
        }
      else
        {
          qDebug() << "MassChroq::validateXmlFile instance document is invalid";
          QString err_text(messHandler.description());
          QString err_line, err_column;
          err_line.setNum(messHandler.line());
          err_column.setNum(messHandler.column());
          file.close();
          schema_file.close();
          throw mcqError(
            QObject::tr("error validating %1 against XSD schema %2\n "
                        "at line %3, column %4 : %5\n")
              .arg(_param_file_info.absoluteFilePath())
              .arg(MASSCHROQ_SCHEMA_FILE)
              .arg(err_line)
              .arg(err_column)
              .arg(err_text));
        }
    }
  else
    {
      schema_file.close();
      throw mcqError(
        QObject::tr("error validating XSD schema : %1 : is invalid \n")
          .arg(schema_file.fileName()));
    }

  return true;
}

/** Method that parses the XML input file (using DOM) looking for
 * peptide text files (containing peptide identification information).
 * If there are such files it launches their parsing and then writes parsed
 *information
 * in the XML input file (in peptide_list form). It then sets this modified XML
 * file as the new input file to MassChroQ. If no peptide files are present,
 * the input file and its filename are not modified.
 **/
void
MassChroq::runDomParser()
{
  QString filepath = _param_file_info.filePath();
  mcqout() << "Running MassChroQ " << MASSCHROQ_VERSION << " with XML file '"
           << filepath << "'" << endl;

  QFile file(filepath);
  MassChroqDomEngine dom_parser(file);
  QString new_filename = dom_parser.parse();
  this->setXmlFilename(new_filename);
}

/**
 * this important method is the first one to be called in this class : it
 * launches the parsing of the masschroqML input file and it handles this
 * parsing till it ends
 */
void
MassChroq::runXmlFile()
{
  validateXmlFile();
  QFile file(_param_file_info.filePath());
  if(!file.open(QIODevice::ReadOnly))
    {
      throw mcqError(QObject::tr("Unable to open xml masschroqML file '%1' :\n")
                       .arg(_param_file_info.absoluteFilePath()));
    }
  MasschroqmlParser parser(this);

  QXmlSimpleReader reader;
  reader.setContentHandler(&parser);
  reader.setErrorHandler(&parser);

  mcqout() << "Parsing XML input file '" << _param_file_info.absoluteFilePath()
           << "'" << endl;

  QXmlInputSource xmlInputSource(&file);

  if(reader.parse(xmlInputSource))
    {
      file.close();
    }
  else
    {
      file.close();
      throw mcqError(
        QObject::tr("error reading masschroqML input file '%1' :\n")
          .arg(_param_file_info.absoluteFilePath())
          .append(parser.errorString()));
    }
  mcqout() << "MassChroQ : DONE on file '"
           << _param_file_info.absoluteFilePath() << "'" << endl;
}

/// create the msrun, launch its simple parsing, initialize its slicer if
/// possible
void
MassChroq::newMsRun(pappso::MsRunReaderSPtr run_reader,
                    const bool read_time_values,
                    const QString &time_dir)
{
  QString filename = run_reader.get()->getMsRunId().get()->getFileName();

  try
    {

      MsrunSp msrun =
        std::make_shared<Msrun>(Msrun(run_reader.get()->getMsRunId()));
      mcqout() << QObject::tr("MS run '%1' file '%2' : parsing begin")
                    .arg(run_reader.get()->getMsRunId().get()->getXmlId())
                    .arg(filename)
               << endl;
      msrun->readPrecursorsAndRetentionTimesInMzDataFile(read_time_values,
                                                         time_dir);
      _ms_run_collection.addMsRun(msrun);
      mcqout() << QObject::tr("MS run '%1' : added")
                    .arg(msrun->getMsRunIdCstSPtr()->getXmlId())
               << endl;
      // this->addMsRun(msrun);
    }
  catch(mcqError &error)
    {
      throw mcqError(
        QObject::tr("problem creating msrun %1:\n%2")
          .arg(run_reader.get()->getMsRunId().get()->getXmlId(), error.what()));
    }
}

void
MassChroq::addAlignmentMethod(const QString &method_id,
                              AlignmentBase *alignment_method)
{
  _alignment_methods[method_id] = alignment_method;
  mcqout() << "Alignment method '" << method_id << "' : added" << endl;
}

void
MassChroq::addQuantificationMethod(QuantificationMethod *quantification_method)
{
  QString quantification_id = quantification_method->getXmlId();
  _quantification_methods[quantification_id] = quantification_method;
  mcqout() << "Quantification method '" << quantification_id << "' : added"
           << endl;
}

void
MassChroq::addQuantificator(Quantificator *quantificator)
{
  QString quantify_id          = quantificator->getXmlId();
  _quantificators[quantify_id] = quantificator;
}

void
MassChroq::deleteQuantificator(const QString &quantify_id)
{
  _quantificators.erase(quantify_id);
}

void
MassChroq::setQuantificatorMatchMode(const QString &quantify_id,
                                     McqMatchingMode match_mode)
{

  Quantificator *quantificator = findQuantificator(quantify_id);
  if(quantificator == NULL)
    {
      throw mcqError(
        QObject::tr("error in MassChroQ::setQuantificatorMatchMode :\nthe "
                    "quantificator with id '%1' does not exist")
          .arg(quantify_id));
    }
  quantificator->setMatchingMode(match_mode);
}

void
MassChroq::addQuantificatorPeptideItems(const QString &quantify_id,
                                        const QStringList &isotope_labels_list,
                                        mcq_double ni_minimum_abundance)
{

  Quantificator *quantificator = findQuantificator(quantify_id);
  if(quantificator == NULL)
    {
      throw mcqError(
        QObject::tr("error in MassChroQ::addQuantificatorPeptideItems:\nthe "
                    "quantificator with id '%1' does not exist")
          .arg(quantify_id));
    }

  quantificator->setTracesOutput(_traces_directory, _traces_format);
  quantificator->setNiMinimumAbundance(ni_minimum_abundance);
  const QString group_id = quantificator->getMsRunGroup()->getXmlId();

  /// get the list of the peptides observed in the current msrun group
  PeptideList peptide_list = getPeptideListInGroup(group_id);

  qDebug() << "Yo2 peptide list size " << peptide_list.size();

  /// in this vector we will put both peptides and isotopes
  std::vector<const Peptide *> isotope_peptides;
  const IsotopeLabel *isotope_label;
  PeptideList::const_iterator it;
  std::vector<const Peptide *>::const_iterator itpep;
  for(it = peptide_list.begin(); it != peptide_list.end(); ++it)
    {
      // isotope_peptides.resize(0);
      // if no isotopes put only the peptides
      if(isotope_labels_list.size() == 0)
        {
          isotope_peptides.push_back(*it);
        }
      // if isotopes put only isotopes
      else
        {
          for(QStringList::const_iterator itstr = isotope_labels_list.begin();
              itstr != isotope_labels_list.end();
              ++itstr)
            {
              isotope_label           = getIsotopeLabel(*itstr);
              PeptideIsotope *isotope = new PeptideIsotope(*it, isotope_label);
              if(isotope->getMass() != (*it)->getMass())
                {
                  isotope_peptides.push_back(isotope);
                }
              else
                { // don't look for isotope if there is no difference with its
                  // original peptide
                  delete(isotope);
                }
            }
        }
    }

  qDebug() << "Yo 3 isotope peptides list size " << isotope_peptides.size();

  quantificator->add_peptide_quanti_items(isotope_peptides,
                                          _trace_peptide_list);
}

void
MassChroq::addQuantificatorMzrtItem(const QString &quantify_id,
                                    const QString &mz,
                                    const QString &rt)
{

  Quantificator *quantificator = findQuantificator(quantify_id);
  if(quantificator == NULL)
    {
      throw mcqError(
        QObject::tr("error in MassChroQ::addQuantificatorMzrtItem:\nthe "
                    "quantificator with id '%1' does not exist")
          .arg(quantify_id));
    }

  quantificator->add_mzrt_quanti_item(mz, rt);
}

void
MassChroq::addQuantificatorMzItems(const QString &quantify_id,
                                   const QStringList &mz_list)
{

  Quantificator *quantificator = findQuantificator(quantify_id);
  if(quantificator == NULL)
    {
      throw mcqError(
        QObject::tr("error in MassChroQ::addQuantificatorMzItems:\nthe "
                    "quantificator with id '%1' does not exist")
          .arg(quantify_id));
    }

  quantificator->add_mz_quanti_items(mz_list);
}

void
MassChroq::addIsotopeLabel(const IsotopeLabel *p_isotope_label)
{
  _map_p_isotope_labels[p_isotope_label->getXmlId()] = p_isotope_label;
}

const IsotopeLabel *
MassChroq::getIsotopeLabel(const QString &label_id)
{
  return (_map_p_isotope_labels[label_id]);
}

void
MassChroq::newMsRunGroup(const QString &group_id,
                         const QStringList &list_msrun_ids)
{

  std::map<QString, msRunHashGroup *>::iterator it;
  it = _groups.find(group_id);
  if(it == _groups.end())
    {
      /// OK this key does not exist, we can create it
      msRunHashGroup *newgroup = new msRunHashGroup(group_id);
      _groups[group_id]        = newgroup;
      MsrunSp p_msrun;

      QStringListIterator list_it(list_msrun_ids);
      QString msrun_id;
      while(list_it.hasNext())
        {
          msrun_id = list_it.next();
          p_msrun  = _ms_run_collection.getMsRunSp(msrun_id);
          if(p_msrun == nullptr)
            {
              /// we have a problem
              throw mcqError(
                QObject::tr("error in MassChroQ::newMsRunGroup :\nthe "
                            "msrun with id %1 does not exist")
                  .arg(msrun_id));
            }
          else
            {
              /// verify that this msrun is not already part of an existing
              /// group
              std::map<QString, msRunHashGroup *>::iterator itverif;
              for(itverif = _groups.begin(); itverif != _groups.end();
                  ++itverif)
                {
                  if((itverif->second)->containsMsRun(p_msrun.get()))
                    {
                      mcqout()
                        << QObject::tr(
                             "WARNING : MS run group '%1' : contains MS "
                             "run '%2' which is already part of '%3' "
                             "group")
                             .arg(group_id)
                             .arg(p_msrun->getMsRunIdCstSPtr()->getXmlId())
                             .arg(itverif->first)
                        << endl;
                    }
                }
              /// add this msrun to the new group
              newgroup->addMsRun(p_msrun);
            }
        }
    }
  else
    {
      throw mcqError(
        QObject::tr("error in MassChroQ::newMsRunGroup :\nthe group "
                    "with id %1 already exists")
          .arg(group_id));
    }
  QString coutList = list_msrun_ids.join(", ");
  mcqout() << "MS run group '" << group_id << "' = (" << coutList
           << ") : defined" << endl;
}

Msrun *
MassChroq::findMsRun(const QString &msrun_id) const
{
  return _ms_run_collection.getMsRun(msrun_id);
}

msRunHashGroup *
MassChroq::findGroup(const QString &group_id) const
{
  std::map<QString, msRunHashGroup *>::const_iterator it;
  it = _groups.find(group_id);
  if(it == _groups.end())
    {
      return (NULL);
    }
  return (it->second);
}

const Protein *
MassChroq::findProtein(const QString &protein_id) const
{
  std::map<QString, ProteinSp>::const_iterator it;
  it = _p_proteins.find(protein_id);
  if(it == _p_proteins.end())
    {
      return (NULL);
    }
  return (it->second.get());
}

AlignmentBase *
MassChroq::findAlignmentMethod(const QString &method_id) const
{
  std::map<const QString, AlignmentBase *>::const_iterator it;
  it = _alignment_methods.find(method_id);
  if(it == _alignment_methods.end())
    {
      return (NULL);
    }
  return (it->second);
}

QuantificationMethod *
MassChroq::findQuantificationMethod(const QString &method_id) const
{
  std::map<const QString, QuantificationMethod *>::const_iterator it;
  it = _quantification_methods.find(method_id);
  if(it == _quantification_methods.end())
    {
      return (NULL);
    }
  return (it->second);
}

Quantificator *
MassChroq::findQuantificator(const QString &quantify_id) const
{
  std::map<const QString, Quantificator *>::const_iterator it;
  it = _quantificators.find(quantify_id);
  if(it == _quantificators.end())
    {
      return (NULL);
    }
  return (it->second);
}

void
MassChroq::alignGroup(const QString &group_id,
                      const QString &method_id,
                      const QString &ref_msrun_id)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  mcqout() << QObject::tr(
                "MS run group '%1': alignment method '%2', reference "
                "msrun '%3': alignment begin")
                .arg(group_id)
                .arg(method_id)
                .arg(ref_msrun_id)
           << endl;

  msRunHashGroup *p_group;
  AlignmentBase *p_method;
  // find group
  p_group = findGroup(group_id);
  if(p_group == NULL)
    {
      throw mcqError(QObject::tr("error in MassChroQ::alignGroup :\nthe group "
                                 "with id %1 does not exist")
                       .arg(group_id));
    }

  p_method = findAlignmentMethod(method_id);
  if(p_method == NULL)
    {
      throw mcqError(QObject::tr("error in MassChroQ::alignGroup :\nthe "
                                 "alignment method with id %1 does not exist")
                       .arg(method_id));
    }
  p_group->setReferenceMsrun(ref_msrun_id);
  p_group->alignMsRuns(*p_method);
  mcqout() << QObject::tr(
                "MS run group '%1' : alignment method '%2', "
                "reference msrun '%3' : alignment finished")
                .arg(group_id)
                .arg(method_id)
                .arg(ref_msrun_id)
           << endl;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
MassChroq::executeQuantification(const QString &quantify_id)
{

  try
    {
      qDebug();
      Quantificator *quantificator = findQuantificator(quantify_id);
      if(quantificator == NULL)
        {
          throw mcqError(
            QObject::tr("the quantificator with id '%1' does not exist")
              .arg(quantify_id));
        }

      quantificator->setMonitor(_p_quantif_results);
      quantificator->quantify();

      quantificator->clear();
      qDebug();
    }
  catch(pappso::PappsoException &error)
    {
      throw mcqError(QObject::tr("error in MassChroQ::executeQuantification %1 "
                                 "PappsoException :\n%2")
                       .arg(quantify_id)
                       .arg(error.qwhat()));
    }
  catch(std::exception &std_error)
    {
      throw mcqError(
        QObject::tr(
          "error in MassChroQ::executeQuantification %1 std::exception :\n%2")
          .arg(quantify_id)
          .arg(std_error.what()));
    }
}

void
MassChroq::addProtein(ProteinSp p_protein)
{
  _p_proteins[p_protein->getXmlId()] = p_protein;
}

void
MassChroq::addPeptideInPeptideList(Peptide *p_peptide)
{
  _peptide_list.push_back(p_peptide);
}

const Peptide *
MassChroq::getPeptide(const QString pepid) const
{
  return _peptide_list.getPeptide(pepid);
}

const PeptideList
MassChroq::getPeptideListInGroup(const QString &group_xml_id) const
{

  const msRunHashGroup *group = findGroup(group_xml_id);
  if(group == NULL)
    {
      throw mcqError(QObject::tr("error in MassChroQ::alignGroup :\nthe group "
                                 "with id %1 does not exist")
                       .arg(group_xml_id));
    }
  return _peptide_list.getPeptideListObservedInMsGroup(*group);
}

void
MassChroq::setPeptideListInMsruns() const
{

  msRunHashGroup::const_iterator it;
  for(it = _ms_run_collection.begin(); it != _ms_run_collection.end(); ++it)
    {
      Msrun *msrun = it->second.get();
      msrun->setPeptideList(_peptide_list.getPeptideListObservedInMsrun(msrun));
    }
}

void
MassChroq::setResults(MonitorSpeedInterface *p_results)
{
  _p_quantif_results = p_results;
}

void
MassChroq::setTracesDirectory(QDir qdir, McqTsvFormat tsv_format)
{
  try
    {
      if(qdir.exists())
        {
        }
      else
        {
          if(qdir.mkpath(qdir.absolutePath()) == false)
            {
              throw mcqError(
                QObject::tr(
                  "error in MassChroq::setTracesDirectory with QDir path "
                  "%1 : unable to create directory")
                  .arg(qdir.absolutePath()));
            }
        }
    }
  catch(std::exception error)
    {
      throw mcqError(
        QObject::tr("error in MassChroq::setTracesDirectory with QDir path %1")
          .arg(qdir.absolutePath()));
    }

  _traces_directory = qdir;
  _traces_format    = tsv_format;
}
void
MassChroq::addPeptideTrace(const Peptide *p_peptide)
{
  _trace_peptide_list.push_back(p_peptide);
}

void
MassChroq::allPeptideTrace()
{
  for(const Peptide *p_peptide : _peptide_list)
    {
      _trace_peptide_list.push_back(p_peptide);
    }
}
