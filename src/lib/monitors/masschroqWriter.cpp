
/**
 * \file masschroqWriter.cpp
 * \date January 03, 2011
 * \author Edlira Nano
 */

#include "masschroqWriter.h"
#include "../../encode/decodeBinary.h"
#include "../consoleout.h"
#include "../mass_chroq.h"
#include "../msrun/ms_run_hash_group.h"
#include "../peptides/peptide_isotope.h"
#include "../quanti_items/quantiItemBase.h"
#include "../share/utilities.h"
#include "../xic/xic_base.h"

#include <QXmlStreamReader>

MasschroqWriter::MasschroqWriter(const QString &in_filename,
                                 const QString &out_filename,
                                 const bool with_traces)
  : _quanti_item_counter(0),
    _input_filename(in_filename),
    _enc_precision(64),
    _enc_little_endian("true")
{
  QFileInfo outFileInfo(out_filename);
  QString outDir(outFileInfo.path());
  QFileInfo outDirInfo(outDir);
  if(!outDirInfo.exists())
    {
      throw mcqError(
        QObject::tr(
          "cannot find the output directory '%1' of output file '%2' \n")
          .arg(outDir, out_filename));
    }

  QString complete_out_filename = out_filename;
  _output_file                  = new QFile(complete_out_filename);
  if(_output_file->exists())
    {
      mcqout() << "WARNING: XML output file '" << complete_out_filename
               << "' already exists, it will be overwrited." << endl;
    }

  if(_output_file->open(QIODevice::WriteOnly))
    {
      _output_stream = new MCQXmlStreamWriter();
      _output_stream->setDevice(_output_file);
    }
  else
    {
      throw mcqError(
        QObject::tr("error : cannot open the XML output file : %1\n")
          .arg(out_filename));
    }

  _output_stream->setAutoFormatting(true);
  writeInputParameters();
}

MasschroqWriter::~MasschroqWriter()
{
  // mcqout() << __FILE__ << __FUNCTION__ << __LINE__ << endl;
  debriefing();
  if(_output_stream != nullptr)
    {
      _output_stream->writeEndDocument();
    }
  // mcqout() << __FILE__ << __FUNCTION__ << __LINE__ << endl;

  if(_output_file != nullptr)
    {
      _output_file->close();
    }
  // mcqout() << __FILE__ << __FUNCTION__ << __LINE__ << endl;

  if(_output_stream != nullptr)
    {
      delete _output_stream;
    }
  // mcqout() << __FILE__ << __FUNCTION__ << __LINE__<< endl;

  if(_output_file != nullptr)
    {
      delete _output_file;
    }
  // mcqout() << __FILE__ << __FUNCTION__ << __LINE__ << endl;
}

void
MasschroqWriter::writeInputParameters()
{
  // parse MassChroQ's input XML file
  QFile infile(_input_filename);

  if(!infile.open(QIODevice::ReadOnly))
    {
      throw mcqError(
        QObject::tr(
          "MasschroqWriter : error reading MassChroQ XML input file '%1'\n")
          .arg(_input_filename));
    }

  QXmlStreamReader reader(&infile);
  while(!reader.atEnd() && !reader.hasError())
    {
      QXmlStreamReader::TokenType token = reader.readNext();
      if(token == QXmlStreamReader::StartDocument)
        {
          _output_stream->writeStartDocument();
        }
      else if(token == QXmlStreamReader::StartElement)
        {
          QString name = reader.name().toString();
          _output_stream->writeStartElement(reader.namespaceUri().toString(),
                                            name);
          _output_stream->writeAttributes(reader.attributes());
          if(name == "masschroq")
            {
              QString mcq_xml_version(MASSCHROQ_SCHEMA_VERSION);
              _output_stream->writeAttribute("version", mcq_xml_version);
              QString type("output");
              _output_stream->writeAttribute("type", type);
            }
        }
      else if(token == QXmlStreamReader::Comment)
        {
          _output_stream->writeComment(reader.text().toString());
        }
      else if(token == QXmlStreamReader::Characters)
        {
          _output_stream->writeCharacters(reader.text().toString());
        }
      else if(token == QXmlStreamReader::EndElement &&
              reader.name() != "masschroq")
        {
          _output_stream->writeEndElement();
        }
    }

  if(reader.hasError())
    {
      QString line, col;
      line.setNum(reader.lineNumber());
      col.setNum(reader.columnNumber());
      throw mcqError(
        QObject::tr("MasschroqWriter : error reading MassChroQ XML "
                    "input file '%1':\n at line %2, column %3 : ")
          .arg(_input_filename, line, col, reader.errorString()));
    }

  if(reader.atEnd())
    {
      initialiseOutput();
    }
}

void
MasschroqWriter::initialiseOutput()
{
  _output_stream->writeStartElement("results");
  QDateTime date_time(QDateTime::currentDateTime());
  _output_stream->writeAttribute("date", date_time);
  _output_stream->writeAttribute("input_file", _input_filename);
}

void
MasschroqWriter::printAlignmentResults(const Msrun *msrun)
{

  qDebug() << "MasschroqWriter::printAlignmentResults begin";
  // check  this was not already written
  bool already_written = (find(_written_align_results.begin(),
                               _written_align_results.end(),
                               msrun) != _written_align_results.end());
  if(already_written)
    {
    }
  else
    {

      _output_stream->writeStartElement("alignment_result");
      // get the time values for this msrun and encode them
      std::vector<mcq_double> v_time = msrun->getVectorOfTimeValues();
      QByteArray enc_time_array = DecodeBinary::base64_encode_doubles(v_time);
      const QString enc_time = QString::fromLatin1(enc_time_array.constData());
      _output_stream->writeAttribute("precision", _enc_precision);
      _output_stream->writeAttribute("little_endian", _enc_little_endian);
      _output_stream->writeCharacters(enc_time);
      _output_stream->writeEndElement(); // </alignment_result>

      _written_align_results.push_back(msrun);
    }
  qDebug() << "MasschroqWriter::printAlignmentResults end";
}

void
MasschroqWriter::writeQuantifyBegin(const Quantificator *p_quantificator)
{
  qDebug() << "MasschroqWriter::writeQuantifyBegin begin";
  _p_quantificator  = p_quantificator;
  _current_group_id = p_quantificator->getMsRunGroup()->getXmlId();

  _current_quantify_id = _p_quantificator->getXmlId();
  _output_stream->writeStartElement("result");
  _output_stream->writeAttribute("quantify_id", _p_quantificator->getXmlId());
  qDebug() << "MasschroqWriter::writeQuantifyBegin end";
}

void
MasschroqWriter::writeQuantifyEnd()
{
  qDebug() << "MasschroqWriter::writeQuantifyEnd begin";
  _current_group_id.clear();

  if(_p_msrun != nullptr)
    {
      setEndCurrentMsrun();
    }
  _output_stream->writeEndElement(); // </result>

  _written_align_results.clear();
  qDebug() << "MasschroqWriter::writeQuantifyEnd end";
}

void
MasschroqWriter::setCurrentMsrun(const Msrun *msrun)
{
  if(_p_msrun != msrun)
    {
      if(_p_msrun != nullptr)
        {
          setEndCurrentMsrun();
        }
      _p_msrun = msrun;
      _output_stream->writeStartElement("data");
      _output_stream->writeAttribute("id_ref",
                                     msrun->getMsRunIdCstSPtr()->getXmlId());
      if(msrun->hasBeenAligned())
        {
          printAlignmentResults(msrun);
        }
    }
}

void
MasschroqWriter::setEndCurrentMsrun()
{
  _output_stream->writeEndElement(); // </data>
  _p_msrun = nullptr;
}

void
MasschroqWriter::privWriteMatchedPeak(const Msrun *p_msrun,
                                      const QuantiItemBase *p_quanti_item,
                                      const AlignedXicPeak *peak)
{

  qDebug() << "MasschroqWriter::privWriteMatchedPeak begin";
  setCurrentMsrun(p_msrun);
  _output_stream->writeStartElement("quanti_item");

  QString quanti_item_id("quanti_item"), item_counter;
  item_counter.setNum(_quanti_item_counter);
  quanti_item_id.append(item_counter);
  _output_stream->writeAttribute("id", quanti_item_id);

  p_quanti_item->writeCurrentSearchItem(_output_stream);

  _quanti_item_counter++;
  // if il y a des xics à tracer
  // writeStartElement("xic_traces");

  mcq_double currentMz = p_quanti_item->getMz();
  const Peptide *current_peptide(p_quanti_item->getPeptide());
  QString pepId, isotopeLabel;
  unsigned int currentZ;

  if(peak != nullptr)
    {
      _output_stream->writeStartElement("quantification_data");

      _output_stream->writeAttribute("mz", currentMz);
      _output_stream->writeAttribute("rt", peak->getMaxXicElement().x);
      _output_stream->writeAttribute("max_intensity",
                                     peak->getMaxXicElement().y);
      _output_stream->writeAttribute("area", peak->getArea());
      _output_stream->writeAttribute("rt_begin", peak->getLeftBoundary().x);
      _output_stream->writeAttribute("rt_end", peak->getRightBoundary().x);

      if(current_peptide != 0)
        {
          pepId = current_peptide->getXmlId();
          _output_stream->writeAttribute("peptide", pepId);

          if(current_peptide->getIsotopeLabel() != nullptr)
            {
              isotopeLabel = current_peptide->getIsotopeLabel()->getXmlId();
              _output_stream->writeAttribute("isotope", isotopeLabel);
            }

          currentZ = *p_quanti_item->getZ();
          _output_stream->writeAttribute("z", currentZ);
        }
      _output_stream->writeEndElement(); // "</quantification_data>"
    }

  _output_stream->writeEndElement(); // </quanti_item>

  qDebug() << "MasschroqWriter::privWriteMatchedPeak end";
}

void
MasschroqWriter::debriefing()
{
  // write protein results

  qDebug() << "MasschroqWriter::debriefing begin";

  _output_stream->writeStartElement("summary");
  QString mcq_version(MASSCHROQ_VERSION);
  _output_stream->writeAttribute("masschroq_version", mcq_version);
  _output_stream->writeStartElement("execution_time");

  QDateTime dt_end_xml = QDateTime::currentDateTime();
  const unsigned int secs_elapsed =
    MassChroq::begin_date_time.secsTo(dt_end_xml);
  _output_stream->writeAttribute("seconds", secs_elapsed);
  const Duration dur =
    Utilities::getDurationFromDates(MassChroq::begin_date_time, dt_end_xml);
  QString days, hours, minutes, secs;
  days.setNum(Utilities::getDaysFromDuration(dur));
  hours.setNum(Utilities::getHoursFromDuration(dur));
  minutes.setNum(Utilities::getMinutesFromDuration(dur));
  secs.setNum(Utilities::getSecondsFromDuration(dur));
  QString xml_duration_format = QString("P0Y0M");
  xml_duration_format.append(days).append(QString("DT"));
  xml_duration_format.append(hours).append(QString("H"));
  xml_duration_format.append(minutes).append(QString("M"));
  xml_duration_format.append(secs).append(QString("S"));
  _output_stream->writeAttribute("duration", xml_duration_format);
  _output_stream->writeEndElement(); // </execution_time>
  _output_stream->writeEndElement(); // </summary>
  _output_stream->writeEndElement(); // </results>
  _output_stream->writeEndDocument();
  //_output_file->close();
  qDebug() << "MasschroqWriter::debriefing end";
}
