
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "monitorspeedlist.h"

MonitorSpeedList::MonitorSpeedList()
{
}

MonitorSpeedList::~MonitorSpeedList()
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
  for(auto &&p_monitor : _monitor_list)
    {
      delete p_monitor;
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " ";
}

void
MonitorSpeedList::writeQuantifyBegin(const Quantificator *p_quantificator)
{

  for(auto &&p_monitor : _monitor_list)
    {
      p_monitor->writeQuantifyBegin(p_quantificator);
    }
}

void
MonitorSpeedList::writeQuantifyEnd()
{

  for(auto &&p_monitor : _monitor_list)
    {
      p_monitor->writeQuantifyEnd();
    }
}

void
MonitorSpeedList::addMonitor(MonitorSpeedInterface *p_monitor_speed)
{
  _monitor_list.push_back(p_monitor_speed);
}

void
MonitorSpeedList::privWriteMatchedPeak(const Msrun *p_msrun,
                                       const QuantiItemBase *p_quanti_item,
                                       const AlignedXicPeak *peak)
{

  for(auto &&p_monitor : _monitor_list)
    {
      p_monitor->privWriteMatchedPeak(p_msrun, p_quanti_item, peak);
    }
}
