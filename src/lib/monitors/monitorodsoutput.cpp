
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "monitorodsoutput.h"
#include "../peptides/peptide.h"
#include "../quantificator.h"
#include <QDir>
#include <odsstream/writer/options/odstablesettings.h>

MonitorTsvOutput::MonitorTsvOutput(const QString &output_directory)
{
  try
    {
      _p_tsv_directory_writer = new TsvDirectoryWriter(QDir(output_directory));
    }
  catch(OdsException &error)
    {
      throw mcqError(
        QObject::tr("Error writing TSV ouput in directory %1 :\n%2")
          .arg(output_directory)
          .arg(error.qwhat()));
    }
  _p_writer = _p_tsv_directory_writer;
}

MonitorTsvOutput::~MonitorTsvOutput()
{

  qDebug() << "MonitorTsvOutput::~MonitorTsvOutput begin";

  _p_writer->close();

  delete _p_tsv_directory_writer;
}

MonitorOdsOutput::MonitorOdsOutput(const QString &ods_file)
{
  try
    {
      _p_ods_writer = new OdsDocWriter(ods_file);
    }
  catch(OdsException &error)
    {
      throw mcqError(QObject::tr("Error writing ODS file %1 :\n%2")
                       .arg(ods_file)
                       .arg(error.qwhat()));
    }
  _p_writer = _p_ods_writer;
}

MonitorOdsOutput::~MonitorOdsOutput()
{
  qDebug() << "MonitorOdsOutput::~MonitorOdsOutput begin";

  _p_writer->close();
  delete _p_ods_writer;
}

MonitorOdsInterfaceOutput::MonitorOdsInterfaceOutput()
{
}

MonitorOdsInterfaceOutput::~MonitorOdsInterfaceOutput()
{
  qDebug() << "MonitorOdsInterfaceOutput::~MonitorOdsInterfaceOutput begin";
}

void
MonitorOdsInterfaceOutput::writeQuantifyBegin(
  const Quantificator *p_quantificator)
{
  qDebug() << "MonitorOdsInterfaceOutput::writeQuantifyBegin begin";
  _current_group_id          = p_quantificator->getMsRunGroup()->getXmlId();
  _current_quantification_id = p_quantificator->getXmlId();
  try
    {

      _p_writer->writeSheet(
        QString("MassChroQ informations - %1").arg(_current_quantification_id));

      _p_writer->writeCell("MassChroQ version :");
      _p_writer->writeLine();
      _p_writer->writeCell(QString("%1").arg(MASSCHROQ_VERSION));
      _p_writer->writeLine();
      _p_writer->writeLine();


      const std::vector<const Peptide *> &peptide_list =
        p_quantificator->getPeptideList();
      if(!_peptide_list_written.contains(_current_group_id))
        {
          this->writePeptideList(peptide_list);
        }

      //_p_writer->writeLine();
      _p_writer->writeSheet(QString("peptides_%1_%2")
                              .arg(p_quantificator->getXmlId())
                              .arg(_current_group_id));

      OdsTableSettings settings;
      settings.setVerticalSplit(1);
      _p_writer->setCurrentOdsTableSettings(settings);

      _p_writer->setCellAnnotation("quantification XML id");
      _p_writer->writeCell("quantification");
      _p_writer->setCellAnnotation("group XML id (fraction name)");
      _p_writer->writeCell("group");
      _p_writer->setCellAnnotation("MS run XML id (sample id)");
      _p_writer->writeCell("msrun");
      _p_writer->setCellAnnotation("MS run file path");
      _p_writer->writeCell("msrunfile");
      _p_writer->setCellAnnotation("XIC m/z");
      _p_writer->writeCell("mz");
      _p_writer->setCellAnnotation("peak maximum intensity retention time");
      _p_writer->writeCell("rt");
      _p_writer->setCellAnnotation("peak maximum intensity");
      _p_writer->writeCell("maxintensity");
      _p_writer->setCellAnnotation("peak area");
      _p_writer->writeCell("area");
      _p_writer->setCellAnnotation("peak start retention time");
      _p_writer->writeCell("rtbegin");
      _p_writer->setCellAnnotation("peak stop retention time");
      _p_writer->writeCell("rtend");
      _p_writer->setCellAnnotation("peptide id");
      _p_writer->writeCell("peptide");
      _p_writer->setCellAnnotation("isotope label");
      _p_writer->writeCell("label");
      _p_writer->setCellAnnotation("peptide sequence");
      _p_writer->writeCell("sequence");
      _p_writer->setCellAnnotation("peptide charge");
      _p_writer->writeCell("z");
      _p_writer->setCellAnnotation("peptide modifications (free text)");
      _p_writer->writeCell("mods");
      if(p_quantificator->getNiMinimumAbundance() != 0)
        {
          _p_writer->setCellAnnotation("natural isotope number");
          _p_writer->writeCell("ninumber");
          _p_writer->setCellAnnotation("natural isotope rank");
          _p_writer->writeCell("nirank");
          _p_writer->setCellAnnotation("natural isotope theoretical ratio");
          _p_writer->writeCell("niratio");
        }
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(
        QObject::tr("error writing quantification for group %1 :\n%2")
          .arg(_current_group_id)
          .arg(ods_error.qwhat()));
    }
  qDebug() << "MonitorOdsOutput::writeQuantifyBegin end";
}

void
MonitorOdsInterfaceOutput::writePeptideList(
  const std::vector<const Peptide *> &isotope_peptides)
{
  qDebug() << "MonitorOdsInterfaceOutput::writePeptideList begin";
  try
    {
      _peptide_list_written.append(_current_group_id);
      _p_writer->writeSheet(QString("%1_proteins").arg(_current_group_id));
      //_p_writer->writeLine();
      _p_writer->writeCell("peptide");
      _p_writer->writeCell("protein");
      _p_writer->writeCell("protein_description");

      OdsTableSettings settings;
      settings.setVerticalSplit(1);
      _p_writer->setCurrentOdsTableSettings(settings);

      std::vector<std::pair<QString, QString>> prot_pep_ids;

      auto itpep    = isotope_peptides.begin();
      auto itpepend = isotope_peptides.end();
      for(; itpep != itpepend; ++itpep)
        {
          auto itprot    = (*itpep)->getProteinList().begin();
          auto itprotend = (*itpep)->getProteinList().end();
          for(; itprot != itprotend; ++itprot)
            {
              std::pair<QString, QString> prot_pep = {(*itpep)->getXmlId(),
                                                      (*itprot)->getXmlId()};
              if(std::find_if(
                   prot_pep_ids.begin(),
                   prot_pep_ids.end(),
                   [prot_pep](std::pair<QString, QString> &pair_pep_prot) {
                     return ((pair_pep_prot.first == prot_pep.first) &&
                             (pair_pep_prot.second == prot_pep.second));
                   }

                   ) == prot_pep_ids.end())
                {
                  _p_writer->writeLine();
                  _p_writer->writeCell((*itpep)->getXmlId());
                  _p_writer->writeCell((*itprot)->getXmlId());
                  _p_writer->writeCell((*itprot)->getDescription());
                  prot_pep_ids.push_back(prot_pep);
                }
            }
        }
    }
  catch(OdsException &ods_error)
    {
      throw mcqError(
        QObject::tr("error writing peptide list for group %1 :\n%2")
          .arg(_current_group_id)
          .arg(ods_error.qwhat()));
    }

  qDebug() << "MonitorOdsOutput::writePeptideList end";
}

void
MonitorOdsInterfaceOutput::writeQuantifyEnd()
{
}

// print results
void
MonitorOdsInterfaceOutput::privWriteMatchedPeak(
  const Msrun *p_msrun,
  const QuantiItemBase *p_quanti_item,
  const AlignedXicPeak *peak)
{
  try
    {
      if(peak != nullptr)
        {
          _p_writer->writeLine();
          _p_writer->writeCell(_current_quantification_id);
          _p_writer->writeCell(_current_group_id);
          _p_writer->writeCell(p_msrun->getMsRunIdCstSPtr()->getXmlId());
          _p_writer->writeCell(
            QFileInfo(p_msrun->getMsRunIdCstSPtr()->getFileName()).baseName());
          _p_writer->writeCell(p_quanti_item->getMz());
          _p_writer->writeCell(peak->getMaxXicElement().x);
          _p_writer->writeCell(peak->getMaxXicElement().y);

          _p_writer->writeCell(peak->getArea());
          _p_writer->writeCell(peak->getLeftBoundary().x);
          _p_writer->writeCell(peak->getRightBoundary().x);

          p_quanti_item->writeOdsPeptideLine(*_p_writer);
        }
    }
  catch(OdsException &error)
    {
      throw mcqError(
        QObject::tr(
          "Error in MonitorOdsInterfaceOutput::privWriteMatchedPeak :\n%1")
          .arg(error.qwhat()));
    }
}
