/**
 * \file lmatreader.cpp
 * \author Olivier Langella
 */

#include "lmatreader.h"
#include "../consoleout.h"
#include "../mcq_error.h"

#include "../../saxparsers/xmlToLmatParser.h"
#include <QTime>
#include <iostream>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/pappsoexception.h>

lmatReader::lmatReader() : LMat()
{
}

lmatReader::~lmatReader()
{
}

bool
lmatReader::set_from_xml(const Msrun &msrun,
                         mcq_float mass_start,
                         mcq_float mass_end,
                         mcq_float precision)
{

  QString file(msrun.getMsRunIdCstSPtr().get()->getFileName());
  qDebug() << "Obiwarp align, begin set from xml : " << file << endl;


  pappso::MsRunReaderSPtr msrun_reader =
    pappso::MsFileAccessor::buildMsRunReaderSPtr(msrun.getMsRunIdCstSPtr());

  try
    {
      XmlToLmatParserHandlerInterface handler(
        this, mass_start, mass_end, precision);

      msrun_reader.get()->readSpectrumCollection(handler);
      handler.debrief();
    }

  catch(pappso::PappsoException &errorp)
    {
      throw mcqError(QObject::tr("error converting file '%1' to lmat :\n%2")
                       .arg(file)
                       .arg(errorp.qwhat()));

      return (false);
    }

  mcqout() << QObject::tr(
                "MS run '%1' : xml file '%2' :  obiwarp parsing for lmat: OK")
                .arg(msrun.getMsRunIdCstSPtr()->getXmlId())
                .arg(file)
           << endl;

  return (true);
}
