/**
 * \file alignment_base.cpp
 * \date 23 sept. 2009
 * \author: Olivier Langella
 */

#include "alignment_base.h"
#include "../msrun/ms_run_hash_group.h"

AlignmentBase::AlignmentBase(MonitorAlignmentBase *monitor)
{
  _monitorAlignment = monitor;
}

AlignmentBase::~AlignmentBase()
{
}

/** method that aligns two msruns (the first one, p_msrun_ref,
 being the reference msrun, group being the group they belong to)
 */
void
AlignmentBase::alignTwoMsRuns(const msRunHashGroup *group,
                              MsrunSp msrun_to_align)
{
  try
    {
      _mutex.lock();
      qDebug() << "Alignment of two MsRuns";
      /// set the current alignment group
      _current_group         = group;
      const Msrun *msrun_ref = group->getReferenceMsrun();
      if(msrun_ref == nullptr)
        {
          qDebug() << "Alignment of two MsRuns : different input reference";
          _mutex.unlock();
          throw mcqError(
            QObject::tr("error in AlignmentBase::alignTwoMsRuns :\nno "
                        "MS run reference found for group %1")
              .arg(group->getXmlId()));
        }

      /// If this msrun is being realigned the .time values eventually read
      /// before have no importance (they will be overwrited after the
      /// alignment). But in order to consider the original rt times for the
      /// alignment, we clear the effects of the reading of this obsolete .time
      /// (we clear _map_aligned_rt).
      msrun_to_align->resetAlignmentTimeValues();
      this->privPrepareMsRunReference(msrun_ref);
      _mutex.unlock();
      /// virtual method that aligns the two msruns
      this->privAlignTwoMsRuns(msrun_ref, msrun_to_align.get());

      _mutex.lock();
      /// write the aligned values in a .time file for the aligned msrun if
      /// asked by the user
      _monitorAlignment->setTimeValues(msrun_to_align.get());
      _mutex.unlock();
      //		p_msrun->write_time_values();
    }
  catch(mcqError &error)
    {
      throw mcqError(
        QObject::tr("error aligning MSrun %1 with reference MSrun %2 :\n"
                    "AlignmentBase::alignTwoMsRuns mcqError %3")
          .arg(msrun_to_align->getMsRunIdCstSPtr()->getXmlId())
          .arg(group->getReferenceMsrun()->getMsRunIdCstSPtr()->getXmlId())
          .arg(error.qwhat()));
    }
  catch(std::exception &std_error)
    {
      throw mcqError(
        QObject::tr("error aligning MSrun %1 with reference MSrun %2 :\n"
                    "AlignmentBase::alignTwoMsRuns std::exception %3")
          .arg(msrun_to_align->getMsRunIdCstSPtr()->getXmlId())
          .arg(group->getReferenceMsrun()->getMsRunIdCstSPtr()->getXmlId())
          .arg(std_error.what()));
    }
}
