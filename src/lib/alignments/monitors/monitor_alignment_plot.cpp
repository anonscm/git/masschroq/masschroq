/*
 * \file monitor_alignment_plot.cpp
 * \date September 14, 2012
 * \author valot
 */

#include "monitor_alignment_plot.h"

MonitorAlignmentPlot::MonitorAlignmentPlot()
{
}

MonitorAlignmentPlot::~MonitorAlignmentPlot()
{
}

void
MonitorAlignmentPlot::setTimeValues(const Msrun *pmsrun)
{
}

void
MonitorAlignmentPlot::setTraceValues(
  const Msrun *pmsrun,
  const std::map<mcq_double, mcq_double> *map_bestRt_deltaRt,
  const std::map<mcq_double, mcq_double> *corrected_map_bestRt_deltaRt,
  const std::map<mcq_double, mcq_double> *corrected_mean_map_bestRt_deltaRt,
  const std::map<mcq_double, mcq_double> *map_oldRt_smoothedDelta)
{
  _mutex.lock();
  qDebug() << "Add Trace Value to Monitor ALignement Plot";
  _map_bestRt_deltaRt.clear();
  std::map<mcq_double, mcq_double>::const_iterator it =
    map_bestRt_deltaRt->begin();
  for(; it != map_bestRt_deltaRt->end(); it++)
    {
      _map_bestRt_deltaRt.insert(
        std::pair<mcq_double, mcq_double>(it->first, it->second));
    }
  _mutex.unlock();
}
