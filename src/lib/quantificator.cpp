/**
 * \file quantificator.cpp
 * \date August 03, 2011
 * \author Edlira Nano
 */

#include "quantificator.h"
#include "./consoleout.h"
//#include "xic/xic_base.h"
#include "peptides/peptide_list.h"
#include "quanti_items/quantiItemMzRt.h"
#include "quanti_items/quantiItemPeptide.h"
#include "quanti_items/quantiitempeptidenaturalisotope.h"
#include "quantifications/mapquanti.h"
#include "quantifications/mapquantirealxic.h"
#include "quantifications/quantificationMethod.h"
#include "xic/xicfilterdetectmatch.h"
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include "share/utilities.h"

#include "peptides/peptidert.h"
#include <QDir>
#include <QStringList>
#include <QtConcurrentMap>
#include <algorithm>
#include <pappsomspp/processing/detection/tracedetectioninterface.h>
#include <pappsomspp/processing/filters/filterinterface.h>

uint Quantificator::_cpu_number = 1;

void
Quantificator::setCpuNumber(uint cpu_number)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " Quantificator::setCpuNumber" << cpu_number;
  uint ideal_number_of_thread = (uint)QThread::idealThreadCount();
  if(cpu_number > ideal_number_of_thread)
    {
      cpu_number = ideal_number_of_thread;
    }
  else
    {
      QThreadPool::globalInstance()->setMaxThreadCount(cpu_number);
    }
  Quantificator::_cpu_number = cpu_number;
  mcqout() << QObject::tr("%1 CPUs used").arg(cpu_number) << endl;
}

Quantificator::Quantificator(const QString &xml_id,
                             const msRunHashGroup *quanti_group,
                             QuantificationMethod *quanti_method)
  : _xml_id(xml_id)
{
  set_quanti_group(quanti_group);
  set_quanti_method(quanti_method);
}

Quantificator::~Quantificator()
{
  clear();
}

void
Quantificator::setTracesOutput(QDir traces_directory,
                               McqTsvFormat traces_format)
{
  _traces_directory = traces_directory;
  _traces_format    = traces_format;
}

void
Quantificator::set_quanti_group(const msRunHashGroup *quanti_group)
{
  _group_to_quantify = quanti_group;
}

void
Quantificator::set_quanti_method(QuantificationMethod *quanti_method)
{
  _quanti_method = quanti_method;
}

void
Quantificator::setMatchingMode(McqMatchingMode match_mode)
{
  _matching_mode = match_mode;
}

const msRunHashGroup *
Quantificator::getMsRunGroup() const
{
  return (_group_to_quantify);
}

void
Quantificator::computeRetentionTimeReferences()
{
  //_map_peptides_2_retention_times.clear();
  //_accumulator.clear();
  for(const Peptide *p_peptide : _peptide_list)
    {

      PeptideRt *_p_peptide_rt = p_peptide->getPeptideRtSp().get();
      auto find_it             = std::find(
        _peptide_rt_list.begin(), _peptide_rt_list.end(), _p_peptide_rt);
      if(find_it == _peptide_rt_list.end())
        {
          _peptide_rt_list.push_back(_p_peptide_rt);
          // auto it_new =
          // _map_peptides_2_retention_times.insert(std::pair<const Peptide *,
          // std::map< const Msrun * , std::vector< MsMsRtIntensity > >
          // >(p_peptide,std::map< const Msrun * , std::vector< MsMsRtIntensity
          // >
          // >())).first;
          // auto it_charge_new = map_peptides_charge_set.insert(std::pair<const
          // Peptide *, std::set<unsigned int>>(p_peptide, std::set<unsigned
          // int>())).first;

          for(auto &pair : *_group_to_quantify)
            {
              const Msrun *p_msrun = pair.second.get();
              // auto it_rt_list = it_new->second.insert(std::pair<const Msrun *
              // , std::vector< MsMsRtIntensity >>
              //                                        (p_msrun, std::vector<
              //                                        MsMsRtIntensity
              //                                        >())).first;

              // qDebug() << "Quantificator::computeRetentionTimeReferences
              // getObservedInList size" <<
              // p_peptide->getObservedInList().size();
              for(auto &&obs_in : p_peptide->getObservedInList())
                {
                  if(obs_in->getPmsRun() == p_msrun)
                    {
                      MsMsObservation observation(p_msrun);
                      observation.intensity = obs_in->getIntensity();
                      observation.rt        = obs_in->getRt();
                      observation.z         = obs_in->getZ();
                      observation.mz        = p_peptide->getMz(observation.z);
                      // it_rt_list->second.push_back(observation);

                      // it_charge_new->second.insert(observation.z);

                      _p_peptide_rt->addMsMsRtIntensity(observation);
                    }
                }
            }
        }
    }

  if((_matching_mode == McqMatchingMode::mean) ||
     (_matching_mode == McqMatchingMode::real_or_mean) ||
     (_matching_mode == McqMatchingMode::post_matching))
    {
      // accumulatorToRetentionTimeReferences();
      for(auto &&p_peptide_rt : _peptide_rt_list)
        {
          p_peptide_rt->computeRetentionTimeReferences(_matching_mode);
        }
    }
}

void
Quantificator::add_peptide_quanti_items(
  std::vector<const Peptide *> &peptide_list,
  std::vector<const Peptide *> &trace_peptide_list)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << peptide_list.size();

  _peptide_list = peptide_list;

  /** \brief peptides charge to survey
   * */
  // std::map<const Peptide *, std::set<unsigned int>> map_peptides_charge_set;

  computeRetentionTimeReferences();

  // const XicExtractionMethodBase * extraction_method =
  //   _quanti_method->getXicExtractionMethod();

  pappso::PrecisionPtr extraction_precision_mean =
    _quanti_method->getMeanPrecision();

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << extraction_precision_mean->toString();
  QuantiItemBase *quanti_item;
  /// for each peptide (or isotope in the vector), we will create a peptide
  /// quanti item
  for(const Peptide *p_peptide : peptide_list)
    {
      // mcqout() << "Quantificator::add_peptide_quanti_items for " << debug_i
      // << " " << isotope_peptides.size() << endl;
      bool trace_peptide = false;
      const Peptide *original_peptide_p =
        p_peptide->getOriginalPeptidePointer();
      if(std::find(trace_peptide_list.begin(),
                   trace_peptide_list.end(),
                   original_peptide_p) != trace_peptide_list.end())
        {
          trace_peptide = true;
        }

      pappso::PeptideSp peptide_sp = p_peptide->getPappsoPeptideSp();
      // mcqout() << (*itpep)->getXmlId() << " " <<
      // peptide_sp.get()->toAbsoluteString() << endl;
      // mcqout().flush();

      /// get the set of charges z the peptide is observed in the current group,
      /// and construct a quantification item for each peptide/z

      // std::set<unsigned int> * s_charges = (*itpep)->getCharges(
      //      *_group_to_quantify);
      std::vector<unsigned int> v_charges =
        p_peptide->getPeptideRtSp()->getCharges();

      // mcqout() << "charge OK " << endl;
      // mcqout().flush();

      // if minimum natural isotope is set :
      // compute with natural isotope
      pappso::PeptideNaturalIsotopeList *p_isotope_list = nullptr;
      if(_minimum_isotope_abundance > 0)
        {

          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                   << "if (_minimum_isotope_abundance > 0)";
          p_isotope_list = new pappso::PeptideNaturalIsotopeList(peptide_sp);
          // mcqout() << "PeptideNaturalIsotopeList OK " << endl;
          // mcqout().flush();
        }

      // for each z do
      for(unsigned int charge : v_charges)
        {
          if(p_isotope_list != nullptr)
            {

              qDebug()
                << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                << "Quantificator::add_peptide_quanti_items (p_isotope_list "
                   "!= nullptr) charge="
                << charge
                << " p_isotope_list->size()=" << p_isotope_list->size()
                << " pep xml id=" << p_peptide->getXmlId()
                << " peptide=" << peptide_sp.get()->toAbsoluteString();
              std::vector<pappso::PeptideNaturalIsotopeAverageSp>
                natural_isotope_average_list =
                  p_isotope_list->getByIntensityRatio(
                    charge,
                    extraction_precision_mean,
                    _minimum_isotope_abundance);
              // mcqout() << "natural_isotope_average_list ok" << endl;
              // mcqout().flush();


              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                       << " natural_isotope_average_list";
              for(pappso::PeptideNaturalIsotopeAverageSp isotope_average_sp :
                  natural_isotope_average_list)
                {
                  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                           << " average isotope "
                           << isotope_average_sp.get()->getIntensityRatio()
                           << " level="
                           << isotope_average_sp.get()->getIsotopeNumber()
                           << " rank="
                           << isotope_average_sp.get()->getIsotopeRank()
                           << endl;
                  quanti_item = new QuantiItemPeptideNaturalIsotope(
                    trace_peptide, p_peptide, isotope_average_sp);
                  _quanti_item_list.push_back(quanti_item);
                }
            }
          else
            {
              quanti_item =
                new QuantiItemPeptide(trace_peptide, p_peptide, charge);
              _quanti_item_list.push_back(quanti_item);
            }
        }

      if(p_isotope_list != nullptr)
        {
          delete p_isotope_list;
          p_isotope_list = nullptr;
        }
    }


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " Quantificator::add_peptide_quanti_items end";
}

void
Quantificator::add_mz_quanti_items(const QStringList &mz_list)
{
  QuantiItemBase *quanti_item;

  QStringList::const_iterator it;

  for(it = mz_list.begin(); it != mz_list.end(); ++it)
    {
      quanti_item = new QuantiItemBase(false, it->toDouble());
      _quanti_item_list.push_back(quanti_item);
    }
}

void
Quantificator::add_mzrt_quanti_item(const QString &mz, const QString &rt)
{

  QuantiItemBase *quanti_item =
    new QuantiItemMzRt(false, mz.toDouble(), rt.toDouble());
  _quanti_item_list.push_back(quanti_item);
}

void
Quantificator::setMonitor(MonitorSpeedInterface *monitor)
{
  _p_monitor_speed = monitor;
}

void
Quantificator::sortQuantiItems()
{
  sort(_quanti_item_list.begin(),
       _quanti_item_list.end(),
       [](const QuantiItemBase *l1, const QuantiItemBase *l2) {
         const mcq_double mz1 = l1->getMz();
         const mcq_double mz2 = l2->getMz();
         return (mz1 < mz2);
       });
}

void
Quantificator::quantify()
{


  qDebug();
  sortQuantiItems();

  QString group_id         = _group_to_quantify->getXmlId();
  QString quanti_method_id = _quanti_method->getXmlId();
  pappso::XicExtractMethod xic_extract_method =
    _quanti_method->getXicExtractMethod();
  //_p_xic_filter
  pappso::FilterInterfaceCstSPtr csp_filter_suite =
    std::make_shared<const pappso::FilterSuite>(
      _quanti_method->getXicFilters());

  pappso::TraceDetectionInterfaceCstSPtr detection_method =
    _quanti_method->getDetectionMethod();

  mcqout() << "MS run group '" << group_id << "': quantification method '"
           << quanti_method_id << "': quantification begin" << endl;
  _quanti_method->printInfos(mcqout());

  qDebug();
  _p_monitor_speed->writeQuantifyBegin(this);
  //_p_monitor_speed->setCurrentGroup(_group_to_quantify);

  // launch quantification in group
  Msrun *current_msrun;
  msRunHashGroup::const_iterator itmsrun;
  unsigned int count_msruns(1);
  std::vector<QuantiItemBase *>::const_iterator itmz;

  if((_matching_mode == McqMatchingMode::realxic_or_mean) ||
     (_matching_mode == McqMatchingMode::no_matching))
    {

      count_msruns = 1;
      mcqout() << "_quanti_item_list.size() '" << _quanti_item_list.size()
               << endl;
      for(itmsrun = _group_to_quantify->begin();
          itmsrun != _group_to_quantify->end();
          ++itmsrun, ++count_msruns)
        {
          current_msrun = itmsrun->second.get();

          mcqout() << QObject::tr(
                        "Quantification '%1' : preparing MS run '%2' to "
                        "extract and quantify %3 XICs (%4)")
                        .arg(_xml_id)
                        .arg(current_msrun->getMsRunIdCstSPtr()->getXmlId())
                        .arg(_quanti_item_list.size())
                        .arg(Utilities::toString(_matching_mode))
                   << endl;
          current_msrun->buildMsRunXicExtractor(xic_extract_method);

          mcqout() << QObject::tr(
                        "quantifying MS run '%1' : %2/%3 in group '%4' (%5)")
                        .arg(current_msrun->getMsRunIdCstSPtr()->getXmlId())
                        .arg(count_msruns)
                        .arg(_group_to_quantify->size())
                        .arg(group_id)
                        .arg(Utilities::toString(_matching_mode))
                   << endl;

          MapQuantiRealXic mapQuanti(*_p_monitor_speed,
                                     current_msrun,
                                     this,
                                     csp_filter_suite,
                                     detection_method);
          QFuture<void> res = QtConcurrent::map(
            _quanti_item_list.begin(), _quanti_item_list.end(), mapQuanti);
          res.waitForFinished();

          current_msrun->deleteMsRunXicExtractor();
          mcqout() << "\t_peptide_list.size() '" << _peptide_list.size()
                   << endl;
        }

      // compute retention time references
      for(auto &&p_peptide_rt : _peptide_rt_list)
        {
          p_peptide_rt->computeRetentionTimeReferences(_matching_mode);
        }
    }
  else
    {
      count_msruns = 1;
      for(itmsrun = _group_to_quantify->begin();
          itmsrun != _group_to_quantify->end();
          ++itmsrun, ++count_msruns)
        {
          current_msrun = itmsrun->second.get();
          mcqout() << QObject::tr(
                        "Quantification '%1' : preparing MS run '%2' to "
                        "extract and quantify %3 XICs")
                        .arg(_xml_id)
                        .arg(current_msrun->getMsRunIdCstSPtr()->getXmlId())
                        .arg(_quanti_item_list.size())
                   << endl;

          current_msrun->buildMsRunXicExtractor(xic_extract_method);

          mcqout() << QObject::tr(
                        "quantifying MS run '%1' : %2/%3 in group '%4'")
                        .arg(current_msrun->getMsRunIdCstSPtr()->getXmlId())
                        .arg(count_msruns)
                        .arg(_group_to_quantify->size())
                        .arg(group_id)
                   << endl;

          MapQuanti mapQuanti(*_p_monitor_speed,
                              current_msrun,
                              this,
                              csp_filter_suite,
                              detection_method);

          QFuture<void> res = QtConcurrent::map(
            _quanti_item_list.begin(), _quanti_item_list.end(), mapQuanti);
          res.waitForFinished();
          current_msrun->deleteMsRunXicExtractor();
        }
    }

  if(_matching_mode == McqMatchingMode::post_matching)
    {
      // compute new rt references
      mcqout() << QObject::tr(
                    "post matching : computing retention time references ")
               << endl;
      QFuture<void> res =
        QtConcurrent::map(_peptide_rt_list.begin(),
                          _peptide_rt_list.end(),
                          [](PeptideRt *p_peptide_rt) {
                            p_peptide_rt->computePostMatchingTimeReferences();
                          });
      res.waitForFinished();

      // rematch peaks stored in quanti items
      mcqout() << QObject::tr(
                    "post matching : trying to match peaks using new "
                    "retention time references ")
               << endl;
      MonitorSpeedInterface *p_monitor_speed = _p_monitor_speed;
      res = QtConcurrent::map(_quanti_item_list.begin(),
                              _quanti_item_list.end(),
                              [p_monitor_speed](QuantiItemBase *p_quanti_item) {
                                p_quanti_item->doPostMatching(*p_monitor_speed);
                              });
      res.waitForFinished();
    }

  _p_monitor_speed->writeQuantifyEnd();

  qDebug() << "Quantificator::quantify() end";
}

void
Quantificator::addMissedQuantiItem(const Msrun *_p_current_msrun,
                                   const QuantiItemBase *_p_current_search_item)
{
  QMutexLocker locker(&_mutex);
  auto it_missed = _missed_quanti_item.insert(
    std::pair<const Msrun *, std::vector<const QuantiItemBase *>>(
      _p_current_msrun, std::vector<const QuantiItemBase *>()));
  it_missed.first->second.push_back(_p_current_search_item);
}

bool
Quantificator::isInMissedQuantiItem(
  const Msrun *_p_msrun, const QuantiItemBase *p_currentSearchItem) const
{
  auto it = _missed_quanti_item.find(_p_msrun);
  if(it == _missed_quanti_item.end())
    {

      qDebug() << "Quantificator::isInMissedQuantiItem false no msrun end ";
      return false;
    }
  else
    {
      auto itquanti =
        std::find(it->second.begin(), it->second.end(), p_currentSearchItem);
      if(itquanti == it->second.end())
        {

          qDebug()
            << "Quantificator::isInMissedQuantiItem false no quanti item end ";
          return false;
        }
    }
  return true;
}

void
Quantificator::clear()
{
  for(auto p_item : _quanti_item_list)
    {
      delete p_item;
    }
  _quanti_item_list.clear();
}
