/**
 * \file quantiItemPeptide.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#include "quantiItemPeptide.h"
#include "../mcq_error.h"
#include "../peptides/isotope_label.h"
#include "../quantificator.h"
#include "../share/utilities.h"
#include "../xic/xictracepeptide.h"
#include <odsstream/calcwriterinterface.h>
#include <odsstream/tsvdirectorywriter.h>

#include <QStringList>

QuantiItemPeptide::QuantiItemPeptide(bool trace_peptide_on,
                                     const Peptide *p_peptide)
  : QuantiItemBase(trace_peptide_on), _p_peptide(p_peptide)
{
}

PeptideRtSp
QuantiItemPeptide::getPeptideRtSp() const
{
  return _p_peptide->getPeptideRtSp();
}
QuantiItemPeptide::QuantiItemPeptide(bool trace_peptide_on,
                                     const Peptide *p_peptide,
                                     unsigned int z)
  : QuantiItemBase(trace_peptide_on), _p_peptide(p_peptide), _z(z)
{
  _mz = p_peptide->getPappsoPeptideSp().get()->getMz(z);
  _p_post_matched_aligned_peaks = AlignedPeakCollectionBase::newInstance(this);
}

QuantiItemPeptide::~QuantiItemPeptide()
{
  // delete _p_post_matched_aligned_peaks;
}

const Peptide *
QuantiItemPeptide::getPeptide() const
{
  return (_p_peptide);
}

const unsigned int *
QuantiItemPeptide::getZ() const
{
  return (&_z);
}

void
QuantiItemPeptide::printInfos(QTextStream &out) const
{
  out << "peptide quantification item :" << endl;
  _p_peptide->printInfos(out);
  out << "_z = " << _z << endl;
  out << "_mz = " << this->getMz() << endl;
}

const QString
QuantiItemPeptide::getQuantiItemId() const
{
  return (_p_peptide->getXmlId());
}

const QString
QuantiItemPeptide::getMzId() const
{
  if(_p_peptide->getIsotopeLabel() != nullptr)
    {
      return (QString("%1_%2+%3")
                .arg(_p_peptide->getXmlId())
                .arg(_p_peptide->getIsotopeLabel()->getXmlId())
                .arg(_z));
    }
  return (QString("%1+%2").arg(_p_peptide->getXmlId()).arg(_z));
}

void
QuantiItemPeptide::writeCurrentSearchItem(
  MCQXmlStreamWriter *_output_stream) const
{

  _output_stream->writeAttribute("item_id_ref", _p_peptide->getXmlId());

  _output_stream->writeAttribute("z", QString::number(_z, 'f', 0));
  const IsotopeLabel *p_isotopeLabel = _p_peptide->getIsotopeLabel();
  if(p_isotopeLabel != nullptr)
    {
      _output_stream->writeAttribute("isotope", p_isotopeLabel->getXmlId());
    }
}

void
QuantiItemPeptide::writeOdsPeptideLine(CalcWriterInterface &writer) const
{

  writer.writeCell(_p_peptide->getXmlId());

  const IsotopeLabel *isotope = _p_peptide->getIsotopeLabel();
  if(isotope != NULL)
    {
      writer.writeCell(isotope->getXmlId());
    }
  else
    {
      writer.writeEmptyCell();
    }
  // writer.writeCell(_p_peptide->getXmlId());
  writer.writeCell(_p_peptide->getSequence());

  // writer.writeCell(_p_peptide->getSequence());

  writer.writeCell(_z);

  writer.writeCell(_p_peptide->getMods());
}

void
QuantiItemPeptide::writeOdsComparHeaderLine(CalcWriterInterface &writer) const
{

  //_p_writer->writeCell("peptide");
  writer.writeCell(_p_peptide->getXmlId());
  //_p_writer->writeCell("m/z");
  writer.writeCell(getMz());
  //_p_writer->writeCell("rt reference");
  writer.writeCell(
    this->_p_peptide->getPeptideRtSp().get()->getAlignedReferenceRt());
  //_p_writer->writeCell("z");
  writer.writeCell(_z);
  //_p_writer->writeCell("isotope number");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope rank");
  writer.writeEmptyCell();
  writer.writeEmptyCell();
  //_p_writer->writeCell("sequence");
  writer.writeCell(_p_peptide->getPappsoPeptideSp().get()->toString());
  //_p_writer->writeCell("isotope");
  if(_p_peptide->getIsotopeLabel() == nullptr)
    {
      writer.writeEmptyCell();
    }
  else
    {
      writer.writeCell(_p_peptide->getIsotopeLabel()->getXmlId());
    }
  //_p_writer->writeCell("mods");
  writer.writeCell(_p_peptide->getMods());
  //_p_writer->writeCell("proteins");
  QStringList protein_list;
  for(const Protein *p_protein : _p_peptide->getProteinList())
    {
      protein_list << p_protein->getXmlId();
    }
  writer.writeCell(protein_list.join(" "));
}

CalcWriterInterface *
QuantiItemPeptide::newCalcWriterInterface(const Quantificator *quantificator,
                                          const Msrun *p_msrun,
                                          const QString &prefix) const
{
  QDir trace_dir(QString("%1/%2/%3")
                   .arg(quantificator->getTracesDirectory().absolutePath())
                   .arg(this->getPeptide()->getXmlId())
                   .arg(p_msrun->getMsRunIdCstSPtr().get()->getXmlId()));
  trace_dir.mkpath(trace_dir.absolutePath());
  CalcWriterInterface *_p_writer = nullptr;
  const IsotopeLabel *p_label    = this->getPeptide()->getIsotopeLabel();
  QString begin_path;
  if(p_label == nullptr)
    {
      begin_path = QString("%1/%2_%3-%4-z%5")
                     .arg(trace_dir.absolutePath())
                     .arg(prefix)
                     .arg(quantificator->getXmlId())
                     .arg(this->getPeptide()->getXmlId())
                     .arg(this->_z);
    }
  else
    {
      begin_path = QString("%1/%2_%3-%4-%5-z%6")
                     .arg(trace_dir.absolutePath())
                     .arg(prefix)
                     .arg(quantificator->getXmlId())
                     .arg(this->getPeptide()->getXmlId())
                     .arg(p_label->getXmlId())
                     .arg(this->_z);
    }
  if(quantificator->getTracesFormat() == McqTsvFormat::tsv)
    {
      _p_writer = new TsvDirectoryWriter(begin_path.append(".d"));
    }
  else
    {
      _p_writer = new OdsDocWriter(begin_path.append(".ods"));
    }
  return _p_writer;
}

XicTraceBase *
QuantiItemPeptide::newXicTrace(Quantificator *quantificator,
                               Msrun *p_msrun,
                               const QString &prefix) const
{
  if(_trace_peptide_on == false)
    return nullptr;

  return new XicTracePeptide(
    newCalcWriterInterface(quantificator, p_msrun, prefix), this, p_msrun);
}

void
QuantiItemPeptide::storeAlignedXicPeakForPostMatching(
  const Msrun *p_current_msrun, AlignedXicPeakSp &xic_peak_sp)
{
  qDebug() << "QuantiItemPeptide::storeAlignedXicPeakForPostMatching begin";
  // std::vector<AlignedXicPeakSp> vec_peak;
  // vec_peak.push_back(xic_peak_sp);
  _p_post_matched_aligned_peaks->add(p_current_msrun, xic_peak_sp);
  // std::pair<std::map<const Msrun*, std::vector<AlignedXicPeakSp>>::iterator,
  // bool> ret = _map_post_matched_aligned_peaks.insert(std::pair<const Msrun*,
  // std::vector<AlignedXicPeakSp>> (p_current_msrun,vec_peak));
  // if (ret.second == false) {
  //    ret.first->second.push_back(xic_peak_sp);
  //}

  qDebug() << "QuantiItemPeptide::storeAlignedXicPeakForPostMatching end";
}
void
QuantiItemPeptide::clearAlignedXicPeakForPostMatching(
  const Msrun *p_current_msrun)
{
  qDebug() << "QuantiItemPeptide::clearAlignedXicPeakForPostMatching begin";
  _p_post_matched_aligned_peaks->clear(p_current_msrun);
  // std::map<const Msrun*, std::vector<AlignedXicPeakSp>>::iterator it =
  // _map_post_matched_aligned_peaks.find(p_current_msrun);
  // if (it != _map_post_matched_aligned_peaks.end()) {
  //    _map_post_matched_aligned_peaks.erase (it);
  //}

  qDebug() << "QuantiItemPeptide::clearAlignedXicPeakForPostMatching end";
}

void
QuantiItemPeptide::doPostMatching(MonitorSpeedInterface &monitor_speed)
{

  qDebug() << "QuantiItemPeptide::doPostMatching begin";
  // _p_monitor_speed.writeMatchedPeak();
  pappso::pappso_double rt_target =
    _p_peptide->getPeptideRtSp().get()->getPostMatchedRtTarget();
  std::vector<const Msrun *> msrun_list =
    _p_post_matched_aligned_peaks->getMsRunList();
  for(const Msrun *p_msrun : msrun_list)
    {
      bool matched = false;
      std::vector<AlignedXicPeakSp> &peak_list =
        _p_post_matched_aligned_peaks->getMsRunAlignedPeakList(p_msrun);
      for(AlignedXicPeakSp &peak : peak_list)
        {
          if(peak.get()->containsRt(rt_target))
            {
              matched = true;
              monitor_speed.writeMatchedPeak(p_msrun, this, peak.get());
              break;
            }
        }
      if(matched == false)
        {
          monitor_speed.writeMatchedPeak(p_msrun, this, nullptr);
        }

      _p_post_matched_aligned_peaks->clear(p_msrun);
    }

  qDebug() << "QuantiItemPeptide::doPostMatching end";
}

void
QuantiItemPeptide::endMsrunQuantification(const Msrun *p_current_msrun)
{

  _p_post_matched_aligned_peaks->endMsrunQuantification(p_current_msrun);
}
