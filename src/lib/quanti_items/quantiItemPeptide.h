/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantiItemPeptide.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#pragma once

#include "../peak_collections/alignedpeakcollectionbase.h"
#include "../peptides/peptide.h"
#include "quantiItemBase.h"

class xicPeak;

/**
 * \class QuantiItemPeptide
 * \brief Class representing a peptide item to be quantified.
 */

class QuantiItemPeptide : public QuantiItemBase
{

  protected:
  QuantiItemPeptide(bool trace_peptide_on, const Peptide *p_peptide);

  public:
  QuantiItemPeptide(bool trace_peptide_on,
                    const Peptide *p_peptide,
                    unsigned int z);

  virtual ~QuantiItemPeptide();

  virtual const Peptide *getPeptide() const;

  virtual const unsigned int *getZ() const;

  virtual const QString getQuantiItemId() const final;

  virtual void printInfos(QTextStream &out) const;

  virtual void
  writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const override;

  /** get a unique identifier of the extracted xic mz or mz/rt or peptide+z or
   * peptide+z+isotopenumber**/
  virtual const QString getMzId() const override;
  virtual void writeOdsPeptideLine(CalcWriterInterface &writer) const override;
  virtual void
  writeOdsComparHeaderLine(CalcWriterInterface &writer) const override;

  virtual XicTraceBase *newXicTrace(Quantificator *quantificator,
                                    Msrun *p_msrun,
                                    const QString &prefix) const override;

  virtual PeptideRtSp getPeptideRtSp() const override;
  virtual void
  storeAlignedXicPeakForPostMatching(const Msrun *p_current_msrun,
                                     AlignedXicPeakSp &xic_peak_sp) override;
  virtual void
  clearAlignedXicPeakForPostMatching(const Msrun *p_current_msrun) override;

  virtual void doPostMatching(MonitorSpeedInterface &monitor_speed) override;

  /** \brief signal of the end of quantification on this MSrun*/
  virtual void endMsrunQuantification(const Msrun *p_current_msrun) override;

  protected:
  virtual CalcWriterInterface *
  newCalcWriterInterface(const Quantificator *quantificator,
                         const Msrun *p_msrun,
                         const QString &prefix) const override;

  protected:
  const Peptide *_p_peptide;
  unsigned int _z;
  // std::map< const Msrun*, std::vector<AlignedXicPeakSp>>
  // _map_post_matched_aligned_peaks;
  AlignedPeakCollectionBaseSp _p_post_matched_aligned_peaks = nullptr;
};
