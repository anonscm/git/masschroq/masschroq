/**
 * \file quantiItemMzRt.cpp
 * \date August 01, 2011
 * \author Edira Nano
 *
 */

#include "quantiItemMzRt.h"

#include <QStringList>

QuantiItemMzRt::QuantiItemMzRt(bool trace_peptide_on,
                               const pappso::pappso_double &mz,
                               const mcq_double rt)
  : QuantiItemBase(trace_peptide_on, mz), _rt(rt)
{
}

QuantiItemMzRt::~QuantiItemMzRt()
{
}

const mcq_double
QuantiItemMzRt::getRt() const
{
  return (_rt);
}

void
QuantiItemMzRt::printInfos(QTextStream &out) const
{
  out << " Mz-Rt quantification item :" << endl;
  out << "_mz = " << this->getMz() << endl;
  out << "_rt = " << _rt << endl;
}

void
QuantiItemMzRt::writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const
{

  QuantiItemBase::writeCurrentSearchItem(_output_stream);

  _output_stream->writeAttribute("rt", QString::number(getRt(), 'g', 8));
}

const QString
QuantiItemMzRt::getMzId() const
{
  return (QString("%1-%2")
            .arg(QString::number(getMz(), 'f', 5))
            .arg(QString::number(getRt(), 'f', 5)));
}
