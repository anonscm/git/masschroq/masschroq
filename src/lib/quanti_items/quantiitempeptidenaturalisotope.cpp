
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "quantiitempeptidenaturalisotope.h"
#include "../peptides/isotope_label.h"
#include "../xic/xictracepeptidenaturalisotope.h"
#include "QStringList"
#include <odsstream/calcwriterinterface.h>
#include <odsstream/tsvdirectorywriter.h>

QuantiItemPeptideNaturalIsotope::QuantiItemPeptideNaturalIsotope(
  bool trace_peptide_on,
  const Peptide *p_peptide,
  const pappso::PeptideNaturalIsotopeAverageSp
    &peptide_natural_isotope_average_sp)
  :

    QuantiItemPeptide(trace_peptide_on, p_peptide)
{
  _mz = peptide_natural_isotope_average_sp.get()->getMz();

  _z = peptide_natural_isotope_average_sp.get()->getCharge();
  _peptide_natural_isotope_average_sp = peptide_natural_isotope_average_sp;

  _p_post_matched_aligned_peaks = AlignedPeakCollectionBase::newInstance(this);
}

QuantiItemPeptideNaturalIsotope::~QuantiItemPeptideNaturalIsotope()
{
}

const QString
QuantiItemPeptideNaturalIsotope::getMzId() const
{
  return (QString("%1+%2-%3")
            .arg(QuantiItemPeptide::getMzId())
            .arg(_peptide_natural_isotope_average_sp.get()->getIsotopeNumber())
            .arg(_peptide_natural_isotope_average_sp.get()->getIsotopeRank()));
}
void
QuantiItemPeptideNaturalIsotope::writeCurrentSearchItem(
  MCQXmlStreamWriter *_output_stream) const
{

  QuantiItemPeptide::writeCurrentSearchItem(_output_stream);

  _output_stream->writeAttribute(
    "ninumber",
    QString::number(
      _peptide_natural_isotope_average_sp.get()->getIsotopeNumber(), 'f', 0));
  _output_stream->writeAttribute(
    "nirank",
    QString::number(
      _peptide_natural_isotope_average_sp.get()->getIsotopeRank(), 'f', 0));
  _output_stream->writeAttribute(
    "niratio",
    QString::number(
      _peptide_natural_isotope_average_sp.get()->getIntensityRatio(), 'f', 5));
}

void
QuantiItemPeptideNaturalIsotope::writeOdsPeptideLine(
  CalcWriterInterface &writer) const
{
  QuantiItemPeptide::writeOdsPeptideLine(writer);

  writer.writeCell(
    _peptide_natural_isotope_average_sp.get()->getIsotopeNumber());

  writer.writeCell(_peptide_natural_isotope_average_sp.get()->getIsotopeRank());

  writer.writeCell(
    _peptide_natural_isotope_average_sp.get()->getIntensityRatio());
}

void
QuantiItemPeptideNaturalIsotope::writeOdsComparHeaderLine(
  CalcWriterInterface &writer) const
{

  //_p_writer->writeCell("peptide");
  writer.writeCell(_p_peptide->getXmlId());
  //_p_writer->writeCell("m/z");
  writer.writeCell(getMz());
  //_p_writer->writeCell("rt reference");
  writer.writeCell(
    this->_p_peptide->getPeptideRtSp().get()->getAlignedReferenceRt());
  //_p_writer->writeCell("z");
  writer.writeCell(_z);
  //_p_writer->writeCell("isotope number");
  writer.writeCell(
    _peptide_natural_isotope_average_sp.get()->getIsotopeNumber());
  //_p_writer->writeCell("isotope rank");
  writer.writeCell(_peptide_natural_isotope_average_sp.get()->getIsotopeRank());
  writer.writeCell(
    _peptide_natural_isotope_average_sp.get()->getIntensityRatio());
  //_p_writer->writeCell("sequence");
  writer.writeCell(_p_peptide->getPappsoPeptideSp().get()->toString());
  //_p_writer->writeCell("isotope");
  if(_p_peptide->getIsotopeLabel() == nullptr)
    {
      writer.writeEmptyCell();
    }
  else
    {
      writer.writeCell(_p_peptide->getIsotopeLabel()->getXmlId());
    }
  //_p_writer->writeCell("mods");
  writer.writeCell(_p_peptide->getMods());
  //_p_writer->writeCell("proteins");
  QStringList protein_list;
  for(const Protein *p_protein : _p_peptide->getProteinList())
    {
      protein_list << p_protein->getXmlId();
    }
  writer.writeCell(protein_list.join(" "));
}

pappso::PeptideNaturalIsotopeAverageSp
QuantiItemPeptideNaturalIsotope::getPeptideNaturalIsotopeAverageSp() const
{
  return _peptide_natural_isotope_average_sp;
}

CalcWriterInterface *
QuantiItemPeptideNaturalIsotope::newCalcWriterInterface(
  const Quantificator *quantificator,
  const Msrun *p_msrun,
  const QString &prefix) const
{
  QDir trace_dir(QString("%1/%2/%3")
                   .arg(quantificator->getTracesDirectory().absolutePath())
                   .arg(this->getPeptide()->getXmlId())
                   .arg(p_msrun->getMsRunIdCstSPtr().get()->getXmlId()));
  trace_dir.mkpath(trace_dir.absolutePath());
  CalcWriterInterface *_p_writer = nullptr;

  const IsotopeLabel *p_label = this->getPeptide()->getIsotopeLabel();
  QString begin_path;
  if(p_label == nullptr)
    {
      begin_path =
        QString("%1/%2_%3-%4-z%5-%6-%7")
          .arg(trace_dir.absolutePath())
          .arg(prefix)
          .arg(quantificator->getXmlId())
          .arg(this->getPeptide()->getXmlId())
          .arg(this->_z)
          .arg(this->getPeptideNaturalIsotopeAverageSp()->getIsotopeNumber())
          .arg(this->getPeptideNaturalIsotopeAverageSp()->getIsotopeRank());
    }
  else
    {
      begin_path =
        QString("%1/%2_%3-%4-%5-z%6-%7-%8")
          .arg(trace_dir.absolutePath())
          .arg(prefix)
          .arg(quantificator->getXmlId())
          .arg(this->getPeptide()->getXmlId())
          .arg(p_label->getXmlId())
          .arg(this->_z)
          .arg(this->getPeptideNaturalIsotopeAverageSp()->getIsotopeNumber())
          .arg(this->getPeptideNaturalIsotopeAverageSp()->getIsotopeRank());
    }
  if(quantificator->getTracesFormat() == McqTsvFormat::tsv)
    {
      _p_writer = new TsvDirectoryWriter(begin_path.append(".d"));
    }
  else
    {
      _p_writer = new OdsDocWriter(begin_path.append(".ods"));
    }
  return _p_writer;
}

XicTraceBase *
QuantiItemPeptideNaturalIsotope::newXicTrace(Quantificator *quantificator,
                                             Msrun *p_msrun,
                                             const QString &prefix) const
{
  if(_trace_peptide_on == false)
    return nullptr;

  return new XicTracePeptideNaturalIsotope(
    newCalcWriterInterface(quantificator, p_msrun, prefix), this, p_msrun);
}
