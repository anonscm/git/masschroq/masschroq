/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file quantiItemBase.h
 * \date August 01, 2011
 * \author Edira Nano
 */

#pragma once

#include "../monitors/mcq_qxmlstreamwriter.h"
#include "../quantificator.h"
#include <iostream>
#include <odsstream/calcwriterinterface.h>
#include <pappsomspp/peptide/peptidenaturalisotopeaverage.h>

class xicPeak;
class Msrun;
class Peptide;
class XicTraceBase;

/**
 * \class QuantiItemBase
 * \brief Base class representing a given mz value to be quantified.
 *
 * The items to be quantified can be:
 * - an mz value,
 * - a pair of mz, rt values,
 * - a peptide.
 */

class QuantiItemBase
{

  protected:
  QuantiItemBase(bool trace_peptide_on);

  public:
  QuantiItemBase(bool trace_peptide_on, const pappso::pappso_double &mz);

  virtual ~QuantiItemBase();

  bool operator<(const QuantiItemBase &other) const;

  virtual const unsigned int *
  getZ() const
  {
    return (NULL);
  }

  virtual const Peptide *
  getPeptide() const
  {
    return (NULL);
  }

  virtual pappso::PeptideNaturalIsotopeAverageSp
  getPeptideNaturalIsotopeAverageSp() const;

  virtual const mcq_double
  getRt() const
  {
    return -1;
  }

  // virtual QString getSimpleName(const Msrun * msrun, McqMatchingMode
  // matching_mode) const;

  // virtual QStringList getTracesTitle(const Msrun * msrun, McqMatchingMode
  // matching_mode) const;

  virtual const QString getQuantiItemId() const;

  /** @brief get a unique ID for this particular mass to quantify
   * get a unique identifier of the extracted xic mz or mz/rt or peptide+z or
   * peptide+z+isotopenumber
   * or even the same peptide with different labels
   */
  virtual const QString getMzId() const;

  virtual void printInfos(QTextStream &out) const;

  // virtual void concatList(QStringList & list, std::map< QString,
  // std::map<QString, QString> > & map_peptide_protein) const;
  // virtual void concatComparList(QStringList & list) const;
  virtual void writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const;
  // virtual void getPepHeaders(QStringList & tmp_list) const ;
  // virtual mcq_double getMatchingRt(const Msrun * msrun, McqMatchingMode
  // match_mode) const;
  virtual void writeOdsPeptideLine(CalcWriterInterface &writer) const;
  virtual void writeOdsComparHeaderLine(CalcWriterInterface &writer) const;
  bool isTraceOn() const;

  virtual XicTraceBase *newXicTrace(Quantificator *quantificator,
                                    Msrun *p_msrun,
                                    const QString &prefix) const;

  const pappso::pappso_double &getMz() const;
  virtual PeptideRtSp getPeptideRtSp() const;
  virtual void
  storeAlignedXicPeakForPostMatching(const Msrun *p_current_msrun,
                                     AlignedXicPeakSp &xic_peak_sp){};
  virtual void
  clearAlignedXicPeakForPostMatching(const Msrun *p_current_msrun){};
  virtual void doPostMatching(MonitorSpeedInterface &_p_monitor_speed){};

  /** \brief signal of the end of quantification on this MSrun*/
  virtual void endMsrunQuantification(const Msrun *p_current_msrun){};

  protected:
  virtual CalcWriterInterface *
  newCalcWriterInterface(const Quantificator *quantificator,
                         const Msrun *p_msrun,
                         const QString &prefix) const;

  protected:
  pappso::pappso_double _mz;

  // McqQuantiItemType _type;
  bool _trace_peptide_on = false;
};
