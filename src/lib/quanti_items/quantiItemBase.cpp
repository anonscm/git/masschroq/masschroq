/**
 * \file quantiItemBase.cpp
 * \date August 01, 2011
 * \author Edira Nano
 *
 */

#include "quantiItemBase.h"
#include <QStringList>
#include <odsstream/calcwriterinterface.h>
#include <odsstream/tsvdirectorywriter.h>

QuantiItemBase::QuantiItemBase(bool trace_peptide_on,
                               const pappso::pappso_double &mz)
  : _mz(mz)
{
  // this->setType();

  _trace_peptide_on = trace_peptide_on;
}

QuantiItemBase::QuantiItemBase(bool trace_peptide_on)
{
  _trace_peptide_on = trace_peptide_on;
}

QuantiItemBase::~QuantiItemBase()
{
}

PeptideRtSp
QuantiItemBase::getPeptideRtSp() const
{
  PeptideRtSp rtSp;
  return rtSp;
}
bool
QuantiItemBase::isTraceOn() const
{
  return (_trace_peptide_on);
}

const pappso::pappso_double &
QuantiItemBase::getMz() const
{
  return (_mz);
}

bool
QuantiItemBase::operator<(const QuantiItemBase &other) const
{
  const mcq_double mz       = this->getMz();
  const mcq_double mz_other = other.getMz();
  return (mz < mz_other);
}

void
QuantiItemBase::printInfos(QTextStream &out) const
{
  out << "Mz quantification item :" << endl;
  out << "_mz = " << this->getMz() << endl;
}

const QString
QuantiItemBase::getQuantiItemId() const
{
  return QString("");
}

const QString
QuantiItemBase::getMzId() const
{
  return (QString::number(getMz(), 'f', 5));
}

void
QuantiItemBase::writeCurrentSearchItem(MCQXmlStreamWriter *_output_stream) const
{
  _output_stream->writeAttribute("mz", QString::number(getMz(), 'f', 0));
}

void
QuantiItemBase::writeOdsPeptideLine(CalcWriterInterface &writer) const
{
}

void
QuantiItemBase::writeOdsComparHeaderLine(CalcWriterInterface &writer) const
{

  //_p_writer->writeCell("peptide");
  writer.writeEmptyCell();
  //_p_writer->writeCell("m/z");
  writer.writeCell(getMz());
  //_p_writer->writeCell("rt reference");
  writer.writeEmptyCell();
  //_p_writer->writeCell("z");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope number");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope rank");
  writer.writeEmptyCell();
  // isotope ratio
  writer.writeEmptyCell();
  //_p_writer->writeCell("sequence");
  writer.writeEmptyCell();
  //_p_writer->writeCell("isotope");
  writer.writeEmptyCell();
  //_p_writer->writeCell("mods");
  writer.writeEmptyCell();
  //_p_writer->writeCell("proteins");
  writer.writeEmptyCell();
}

pappso::PeptideNaturalIsotopeAverageSp
QuantiItemBase::getPeptideNaturalIsotopeAverageSp() const
{
  pappso::PeptideNaturalIsotopeAverageSp emptySp;
  return emptySp;
}

CalcWriterInterface *
QuantiItemBase::newCalcWriterInterface(const Quantificator *quantificator,
                                       const Msrun *p_msrun,
                                       const QString &prefix) const
{
  QDir trace_dir(QString("%1/%2")
                   .arg(quantificator->getTracesDirectory().absolutePath())
                   .arg(p_msrun->getMsRunIdCstSPtr().get()->getXmlId()));
  trace_dir.mkpath(trace_dir.absolutePath());
  CalcWriterInterface *_p_writer = nullptr;
  if(quantificator->getTracesFormat() == McqTsvFormat::tsv)
    {
      _p_writer = new TsvDirectoryWriter(QString("%1/%2_%3.d")
                                           .arg(trace_dir.absolutePath())
                                           .arg(prefix)
                                           .arg(this->getMzId()));
    }
  else
    {
      _p_writer = new OdsDocWriter(QString("%1/%2_%3.ods")
                                     .arg(trace_dir.absolutePath())
                                     .arg(prefix)
                                     .arg(this->getMzId()));
    }
  return _p_writer;
}
XicTraceBase *
QuantiItemBase::newXicTrace(Quantificator *quantificator,
                            Msrun *p_msrun,
                            const QString &prefix) const
{
  return nullptr;
}
