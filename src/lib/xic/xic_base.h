/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file xic_base.h
 * \date 21 sept. 2009
 * \author Olivier Langella
 */

#pragma once

#include <iostream>
#include <vector>

#include "../msrun/msrun.h"
#include <pappsomspp/processing/detection/tracedetectioninterface.h>
#include <pappsomspp/processing/filters/filterinterface.h>
#include <pappsomspp/xic/xic.h>

/**
 * \class xicBase
 * \brief Virtual base class representing a XIC
 * (eXtracted Ion Chromatogram : retention times / intesity )

 * A Xic is associated to :
 * - the msrun it belongs to,
 * - the selection method used for its extraction
 * It contains :
 * - the vector of its rt-s (the x-axis)
 * - the vector of its intesities (the y-axis)
 */

class xicBase : public pappso::Xic
{

  public:
  xicBase(const Msrun *p_ms_run,
          const pappso::MzRange &mz_range,
          pappso::XicExtractMethod xic_type);
  xicBase(const xicBase &xic_base);

  virtual ~xicBase();

  const Msrun *getMsRun() const;

  std::vector<mcq_double> getAlignedRetentionTimes() const;

  /// print the p_v_rt and p_v_intensity space-separated values
  /// into a stream
  void toStream(QTextStream &to_stream) const;

  pappso::XicExtractMethod
  getXicExtractMethod() const
  {
    return _xic_type;
  };
  const std::vector<mcq_double> getIntensities() const;
  void
  fillDataArray(mcq_double *x1, mcq_double *y1, unsigned int plotsize) const;
  void applyFilter(const pappso::FilterInterface &filter);

  const pappso::MzRange &
  getPappsoMassRange() const
  {
    return _mz_range;
  };

  protected:
  /// the MSrun this Xic belongs to
  const Msrun *_p_ms_run;

  const pappso::MzRange _mz_range;

  pappso::XicExtractMethod _xic_type;
};
