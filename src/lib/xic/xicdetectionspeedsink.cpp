
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xicdetectionspeedsink.h"

XicDetectionSpeedSink::XicDetectionSpeedSink(
  QuantiItemBase *p_currentSearchItem,
  const Msrun *p_msrun,
  pappso::pappso_double rt_reference,
  MonitorSpeedInterface &monitor)
  : _monitor(monitor)
{
  _p_currentSearchItem = p_currentSearchItem;
  _p_msrun             = p_msrun;
  _rt_reference        = rt_reference;
}

XicDetectionSpeedSink::~XicDetectionSpeedSink()
{
}
