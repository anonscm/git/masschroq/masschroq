
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../monitors/monitorspeedinterface.h"
#include "xictracebase.h"
#include <pappsomspp/processing/detection/tracedetectioninterface.h>
#include <pappsomspp/processing/filters/filterinterface.h>
#include <pappsomspp/xic/xic.h>

class XicFilterDetectMatch : public pappso::TraceDetectionSinkInterface
{
  public:
  XicFilterDetectMatch(XicTraceBase *p_trace,
                       MonitorSpeedInterface &monitor,
                       const pappso::FilterInterface *p_xic_filter,
                       const pappso::TraceDetectionInterface *p_detection,
                       McqMatchingMode matching_mode);
  ~XicFilterDetectMatch();

  virtual void setTracePeak(pappso::TracePeak &xic_peak) override;

  void filterDetectQuantify(const pappso::Xic &xic,
                            pappso::pappso_double rt_target,
                            QuantiItemBase *p_currentSearchItem,
                            const Msrun *p_msrun);

  private:
  MonitorSpeedInterface &_monitor;
  const pappso::TraceDetectionInterface *_p_detection;
  const pappso::FilterInterface *_p_xic_filter;
  const pappso::Xic *_p_current_xic;
  XicTraceBase *_p_trace = nullptr;

  pappso::pappso_double _rt_target;
  QuantiItemBase *_p_current_search_item = nullptr;
  const Msrun *_p_current_msrun;
  bool _peak_detected;
  McqMatchingMode _matching_mode;

  std::vector<pappso::TracePeakCstSPtr> _p_log_peaks;
  pappso::pappso_double _thin_peak_rt_tolerance = 0;
};
