
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "xicfilterdetectmatch.h"
#include "../consoleout.h"
#include <cmath>
#include <pappsomspp/processing/detection/tracedetectioninterface.h>

XicFilterDetectMatch::XicFilterDetectMatch(
  XicTraceBase *p_trace,
  MonitorSpeedInterface &monitor,
  const pappso::FilterInterface *p_xic_filter,
  const pappso::TraceDetectionInterface *p_detection,
  McqMatchingMode matching_mode)
  : _monitor(monitor)
{
  _p_detection   = p_detection;
  _p_xic_filter  = p_xic_filter;
  _p_trace       = p_trace;
  _matching_mode = matching_mode;
  if(_matching_mode == McqMatchingMode::post_matching)
    {
      _thin_peak_rt_tolerance = 0;
    }
  else
    {
      _thin_peak_rt_tolerance = 5;
    }
}

XicFilterDetectMatch::~XicFilterDetectMatch()
{
}

void
XicFilterDetectMatch::setTracePeak(pappso::TracePeak &xic_peak)
{
  // qDebug() << "XicFilterDetectMatch::setXicPeak begin" ;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(_p_trace != nullptr)
    {
      _p_trace->addXicPeak(xic_peak);
    }
  if(_peak_detected)
    return;

  if(_matching_mode == McqMatchingMode::post_matching)
    {
      // AlignedXicPeak aligned_peak(xic_peak, _p_current_msrun);
      AlignedXicPeakSp aligned_peak_sp = std::make_shared<const AlignedXicPeak>(
        AlignedXicPeak(xic_peak, _p_current_msrun));
      _p_current_search_item->storeAlignedXicPeakForPostMatching(
        _p_current_msrun, aligned_peak_sp);
      if(_rt_target == -1)
        {
          // the peptide was not observed in MSMS : we only need to store
          // detected peaks to postmatch them
          return;
        }
    }
  if(_rt_target == -1)
    {
      if(_matching_mode == McqMatchingMode::no_matching)
        {
          AlignedXicPeak aligned_peak(xic_peak, _p_current_msrun);
          _monitor.writeMatchedPeak(
            _p_current_msrun, _p_current_search_item, &aligned_peak);

          _peak_detected = true;
        }
    }
  else
    {
      if(xic_peak.containsRt(_rt_target))
        {

          // qDebug() << "XicFilterDetectMatch::setXicPeak writeMatchedPeak" ;
          AlignedXicPeak aligned_peak(xic_peak, _p_current_msrun);
          _monitor.writeMatchedPeak(
            _p_current_msrun, _p_current_search_item, &aligned_peak);

          _peak_detected = true;
        }
      else
        {

          if(_matching_mode != McqMatchingMode::post_matching)
            {
              if((xic_peak.getLeftBoundary().x - _rt_target) < 10)
                {
                  unsigned int distance_point =
                    _p_current_xic->getMsPointDistance(
                      _rt_target, xic_peak.getLeftBoundary().x);
                  if(distance_point == 0)
                    {
                      AlignedXicPeak aligned_peak(xic_peak, _p_current_msrun);
                      _monitor.writeMatchedPeak(_p_current_msrun,
                                                _p_current_search_item,
                                                &aligned_peak);
                      _peak_detected = true;
                    }
                }
            }
        }
    }
  if(_peak_detected)
    {
      if(_p_trace != nullptr)
        {
          _p_trace->setMatchedPeak(xic_peak);
          AlignedXicPeak aligned_peak(xic_peak, _p_current_msrun);
          _p_trace->setAlignedMatchedPeak(aligned_peak);
        }
      if(_matching_mode == McqMatchingMode::post_matching)
        {
          AlignedXicPeak aligned_peak(xic_peak, _p_current_msrun);
          // store aligned rt of this matched peak
          _p_current_search_item->getPeptideRtSp()
            .get()
            ->postMatchingAlignedPeak(aligned_peak);
        }
    }
  else
    {
      if(std::abs(xic_peak.getMaxXicElement().y - _rt_target) <
         _thin_peak_rt_tolerance)
        {
          // if this peak is nearby, in a 5 second range
          _p_log_peaks.push_back(xic_peak.makeTracePeakCstSPtr());
        }
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // qDebug() << "XicFilterDetectMatch::setXicPeak end" ;
}

void
XicFilterDetectMatch::filterDetectQuantify(const pappso::Xic &xic,
                                           pappso::pappso_double rt_target,
                                           QuantiItemBase *p_currentSearchItem,
                                           const Msrun *p_msrun)
{


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  _p_current_search_item = p_currentSearchItem;
  _p_current_msrun       = p_msrun;
  _rt_target             = rt_target;
  _peak_detected         = false;

  _p_current_xic = &xic;
  pappso::Xic newxic(xic);
  if(_p_xic_filter != nullptr)
    {
      _p_xic_filter->filter(newxic);
      _p_current_xic = &newxic;
    }


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << _rt_target;
  _p_detection->detect(*_p_current_xic, *this);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " after detect";
  if(_peak_detected == false)
    {
      if(_p_log_peaks.size() > 0)
        {
          // take the nearest peak : this should be a small peak, that does not
          // cover the RT target
          const pappso::TracePeak *peak_i = _p_log_peaks.at(0).get();
          pappso::pappso_double rt_distance =
            std::abs(peak_i->getMaxXicElement().x - _rt_target);
          for(unsigned int i = 1; i < _p_log_peaks.size(); i++)
            {

              pappso::pappso_double new_rt_distance =
                std::abs(_p_log_peaks.at(i)->getMaxXicElement().x - _rt_target);
              if(new_rt_distance < rt_distance)
                {
                  peak_i      = _p_log_peaks.at(i).get();
                  rt_distance = new_rt_distance;
                }
            }

          _peak_detected = true;
          AlignedXicPeak aligned_peak(*peak_i, _p_current_msrun);
          _monitor.writeMatchedPeak(
            _p_current_msrun, _p_current_search_item, &aligned_peak);
        }
    }

  if(_peak_detected)
    {
      if(_matching_mode == McqMatchingMode::post_matching)
        {
          // if a peak was detected, that was from an observed peptide in this
          // msrun, we don't need to store it to postmatch it
          _p_current_search_item->clearAlignedXicPeakForPostMatching(
            _p_current_msrun);
        }
    }
  else
    {
      if(_matching_mode != McqMatchingMode::post_matching)
        {
          _monitor.writeMatchedPeak(
            _p_current_msrun, _p_current_search_item, nullptr);
        }
    }


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}
