
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once

#include "../monitors/monitorspeedinterface.h"
#include "../quantificator.h"
#include "xictracebase.h"
#include <QMutex>
#include <pappsomspp/processing/detection/tracedetectioninterface.h>
#include <pappsomspp/processing/filters/filterinterface.h>

class XicFilterDetectMatchRealRt : public pappso::TraceDetectionSinkInterface
{
  public:
  XicFilterDetectMatchRealRt(XicTraceBase *p_trace,
                             MonitorSpeedInterface &monitor,
                             pappso::FilterInterfaceCstSPtr p_xic_filter,
                             pappso::TraceDetectionInterfaceCstSPtr p_detection,
                             Quantificator *p_quantificator,
                             McqMatchingMode matching_mode);
  ~XicFilterDetectMatchRealRt();

  virtual void setTracePeak(pappso::TracePeak &xic_peak) override;

  void filterDetectQuantify(const pappso::Xic *xic,
                            const std::vector<MsMsRtIntensity> *p_real_rt,
                            QuantiItemBase *p_currentSearchItem,
                            const Msrun *p_msrun);

  private:
  MonitorSpeedInterface &_monitor;
  Quantificator *_p_quantificator;
  pappso::TraceDetectionInterfaceCstSPtr _p_detection;
  pappso::FilterInterfaceCstSPtr _p_xic_filter;
  std::map<pappso::TracePeakCstSPtr, unsigned int> _peak_counter_map;

  const std::vector<MsMsRtIntensity> *_p_real_rt;
  QuantiItemBase *_p_current_search_item = nullptr;
  const Msrun *_p_current_msrun;

  std::vector<pappso::TracePeakCstSPtr> *_p_log_peaks = nullptr;
  XicTraceBase *_p_trace                              = nullptr;
  McqMatchingMode _matching_mode;

  // QMutex _mutex;
};
