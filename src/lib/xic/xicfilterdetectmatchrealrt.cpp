
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the MassChroQ software.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#include "xicfilterdetectmatchrealrt.h"
#include "../quanti_items/quantiItemBase.h"

XicFilterDetectMatchRealRt::XicFilterDetectMatchRealRt(
  XicTraceBase *p_trace,
  MonitorSpeedInterface &monitor,
  pappso::FilterInterfaceCstSPtr p_xic_filter,
  pappso::TraceDetectionInterfaceCstSPtr p_detection,
  Quantificator *p_quantificator,
  McqMatchingMode matching_mode)
  : _monitor(monitor)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  _p_detection     = p_detection;
  _p_xic_filter    = p_xic_filter;
  _p_quantificator = p_quantificator;
  _p_trace         = p_trace;
  _matching_mode   = matching_mode;
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

XicFilterDetectMatchRealRt::~XicFilterDetectMatchRealRt()
{
  qDebug() << "XicFilterDetectMatchRealRt::~XicFilterDetectMatchRealRt "
           << this;
}

void
XicFilterDetectMatchRealRt::setTracePeak(pappso::TracePeak &xic_peak)
{
  qDebug() << "XicFilterDetectMatchRealRt::setXicPeak begin " << this;

  if(_p_log_peaks == nullptr)
    {

      if(_p_trace != nullptr)
        {
          _p_trace->addXicPeak(xic_peak);
        }
      pappso::TracePeakCstSPtr xic_peak_sp;
      for(auto real_rt : *_p_real_rt)
        {
          if(xic_peak.containsRt(real_rt.rt))
            {
              if(xic_peak_sp == nullptr)
                {
                  xic_peak_sp = xic_peak.makeTracePeakCstSPtr();
                }
              //_mutex.lock();
              auto it_new = _peak_counter_map.insert(
                std::pair<pappso::TracePeakCstSPtr, unsigned int>(xic_peak_sp,
                                                                  0));
              it_new.first->second++;
              //_mutex.unlock();
            }
        }
    }
  else
    {
      _p_log_peaks->push_back(xic_peak.makeTracePeakCstSPtr());
    }
  qDebug() << "XicFilterDetectMatchRealRt::setXicPeak end " << this;
}

void
XicFilterDetectMatchRealRt::filterDetectQuantify(
  const pappso::Xic *p_origxic,
  const std::vector<MsMsRtIntensity> *p_real_rt,
  QuantiItemBase *p_currentSearchItem,
  const Msrun *p_msrun)
{

  qDebug() << "XicFilterDetectMatchRealRt::filterDetectQuantify begin " << this;
  if(p_origxic == nullptr)
    {
      throw mcqError(
        QObject::tr("error in "
                    "XicFilterDetectMatchRealRt::"
                    "filterDetectQuantify: p_origxic == nullptr"));
    }

  _peak_counter_map.clear();
  _p_current_search_item = p_currentSearchItem;
  _p_current_msrun       = p_msrun;
  _p_real_rt             = p_real_rt;

  const pappso::Xic *p_xic = p_origxic;
  pappso::Xic newxic(*p_origxic);
  if(_p_xic_filter != nullptr)
    {

      qDebug() << "XicFilterDetectMatchRealRt::filterDetectQuantify filter "
               << p_origxic->size() << " " << newxic.size() << " " << this;
      _p_xic_filter->filter(newxic);
      qDebug() << "XicFilterDetectMatchRealRt::filterDetectQuantify filter 2 "
               << newxic.size() << " " << this;
      p_xic = &newxic;
    }
  // qDebug() << "XicFilterDetectMatch::filterDetectQuantify detect " <<
  // _rt_target;
  _p_detection->detect(*p_xic, *this);
  qDebug() << "XicFilterDetectMatchRealRt::filterDetectQuantify after detect"
           << " " << this;

  if(_peak_counter_map.size() > 0)
    {
      if(_peak_counter_map.size() == 1)
        {
          qDebug()
            << "XicFilterDetectMatchRealRt::filterDetectQuantify NO CHOICE "
               "_peak_counter_map.size() "
            << _peak_counter_map.size() << " " << this;
          // const pappso::XicPeak * p_peak =
          // _peak_counter_map.begin()->first.get();
          AlignedXicPeak aligned_peak(*(_peak_counter_map.begin()->first.get()),
                                      _p_current_msrun);
          _monitor.writeMatchedPeak(
            _p_current_msrun, _p_current_search_item, &aligned_peak);

          if(_matching_mode == McqMatchingMode::post_matching)
            {
              // store aligned rt of this matched peak
              _p_current_search_item->getPeptideRtSp()
                .get()
                ->postMatchingAlignedPeak(aligned_peak);
            }
          else
            {
              _p_current_search_item->getPeptide()
                ->getPeptideRtSp()
                ->addWeightedRealXicRt(aligned_peak,
                                       _peak_counter_map.begin()->second);
            }

          if(_p_trace != nullptr)
            {
              _p_trace->setMatchedPeak(
                *(_peak_counter_map.begin()->first.get()));
              _p_trace->setAlignedMatchedPeak(aligned_peak);
            }
        }
      else
        {
          // we have to choose among Xic peaks :/
          qDebug() << "XicFilterDetectMatchRealRt::filterDetectQuantify "
                      "_peak_counter_map.size() "
                   << _peak_counter_map.size() << " " << this;
          auto it                          = _peak_counter_map.begin();
          auto itend                       = _peak_counter_map.end();
          pappso::TracePeakCstSPtr peak_sp = it->first;
          it++;
          while(it != itend)
            {
              if(peak_sp.get()->getArea() < it->first.get()->getArea())
                {
                  peak_sp = it->first;
                }
              it++;
            }

          AlignedXicPeak aligned_peak(*(peak_sp.get()), _p_current_msrun);
          _monitor.writeMatchedPeak(
            _p_current_msrun, _p_current_search_item, &aligned_peak);

          if(_matching_mode == McqMatchingMode::post_matching)
            {
              // store aligned rt of this matched peak
              _p_current_search_item->getPeptideRtSp()
                .get()
                ->postMatchingAlignedPeak(aligned_peak);
            }
          else
            {
              _p_current_search_item->getPeptide()
                ->getPeptideRtSp()
                ->addWeightedRealXicRt(aligned_peak, 1);
            }

          if(_p_trace != nullptr)
            {
              _p_trace->setMatchedPeak(*(peak_sp.get()));
              _p_trace->setAlignedMatchedPeak(aligned_peak);
            }
        }
    }
  else
    {
      qDebug()
        << "XicFilterDetectMatchRealRt::filterDetectQuantify not detected "
        << " " << this;
      _monitor.writeMatchedPeak(
        _p_current_msrun, _p_current_search_item, nullptr);

      _p_log_peaks = new std::vector<pappso::TracePeakCstSPtr>();
      _p_detection->detect(*p_xic, *this);
      if(_p_log_peaks->size() > 0)
        {
          unsigned int nbpeaks_near = 0;
          for(unsigned int i = 0; i < p_real_rt->size(); i++)
            {
              pappso::pappso_double rt = p_real_rt->at(i).rt;
              for(unsigned int j = 0; j < _p_log_peaks->size(); j++)
                {
                  pappso::TracePeakCstSPtr peak_sp = _p_log_peaks->at(j);
                  if(peak_sp.get()->getMaxXicElement().x < rt)
                    {
                      if((rt - peak_sp.get()->getRightBoundary().x) < 10)
                        {
                          unsigned int distance_point =
                            p_origxic->getMsPointDistance(
                              rt, peak_sp.get()->getRightBoundary().x);
                          if(distance_point < 10)
                            {
                              nbpeaks_near++;
                            }
                        }
                    }
                  else
                    {
                      if((peak_sp.get()->getLeftBoundary().x - rt) < 10)
                        {
                          unsigned int distance_point =
                            p_origxic->getMsPointDistance(
                              rt, peak_sp.get()->getLeftBoundary().x);
                          if(distance_point == 0)
                            {
                              AlignedXicPeak aligned_peak(*(peak_sp.get()),
                                                          _p_current_msrun);
                              _monitor.writeMatchedPeak(_p_current_msrun,
                                                        _p_current_search_item,
                                                        &aligned_peak);

                              if(_matching_mode ==
                                 McqMatchingMode::post_matching)
                                {
                                  // store aligned rt of this matched peak
                                  // warning : this could be risky
                                  _p_current_search_item->getPeptideRtSp()
                                    .get()
                                    ->postMatchingAlignedPeak(aligned_peak);
                                }
                              else
                                {
                                  _p_current_search_item->getPeptide()
                                    ->getPeptideRtSp()
                                    ->addWeightedRealXicRt(aligned_peak, 1);
                                }

                              if(_p_trace != nullptr)
                                {
                                  _p_trace->setMatchedPeak(*(peak_sp.get()));
                                  _p_trace->setAlignedMatchedPeak(aligned_peak);
                                }
                              return;
                            }
                          nbpeaks_near++;
                        }
                    }
                }
            }
          pappso::PeptideNaturalIsotopeAverageSp peptide_natural_sp =
            _p_current_search_item->getPeptideNaturalIsotopeAverageSp();
          if(nbpeaks_near > 0)
            {
              if((peptide_natural_sp == nullptr) ||
                 (peptide_natural_sp.get()->getIsotopeNumber() == 0))
                {
                  //                 _monitor.logMissedObservedRealXic(_p_current_search_item,
                  //                 p_origxic, p_real_rt,_p_log_peaks);
                }

              // it seems to be better to forget quanti items that was not
              // matched here : leave it in comments
              //_p_quantificator->addMissedQuantiItem(_p_current_msrun,
              //_p_current_search_item);
            }
        }
      else
        {
          // no peaks : nothing to do
        }
    }

  qDebug() << "XicFilterDetectMatchRealRt::filterDetectQuantify end"
           << " " << this;
}
