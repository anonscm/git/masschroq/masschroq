; MassChroQ Inno Setup file to build setup installer for windows
;
; MassChroQ: Mass Chromatogram Quantification software.
; Copyright (C) 2010 Benoit Valot, Olivier Langella, Edlira Nano, Michel Zivy.
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

; Definition of precompilation variables
#define MyAppName "MassChroQ"
#define MyAppVersion "1.2"
#define MyAppPublisher "PAPPSO"
#define MyAppURL "http://pappso.inra.fr/bioinfo/masschroq/"
#define MyAppExeName "masschroq.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{2FEBE499-EE5A-48EE-9C31-D9190A7301EA}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
LicenseFile=C:\developpement\masschroq\win32\LICENCE
OutputDir=C:\developpement\masschroq\win32
OutputBaseFilename=masschroq_setup
; set this to true if you want to perform file associations in the registry
ChangesAssociations=true
; set this to true if you want to change environment variables
ChangesEnvironment=true
; permissions to run/write/read everyone
PrivilegesRequired=none

Compression=lzma
SolidCompression=yes

; Changes in registry
[Registry]
; File association
Root: HKCR; Subkey: ".masschroqML"; ValueType: string; ValueName: ""; ValueData: "masschroqMLFile "; Flags: uninsdeletevalue; Tasks: addextension
Root: HKCR; Subkey: "masschroqMLFile"; ValueType: string; ValueName: ""; ValueData: "masschroqML File"; Flags: uninsdeletekey; Tasks: addextension
Root: HKCR; Subkey: "masschroqMLFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\{#MyAppExeName}"" ""%1"""; Flags: uninsdeletevalue; Tasks: addextension
Root: HKCR; Subkey: "masschroqMLFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\masschroq.ico,0"; Flags: uninsdeletevalue; Tasks: addextension

;Setup choice languages
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "czech"; MessagesFile: "compiler:Languages\Czech.isl"
Name: "danish"; MessagesFile: "compiler:Languages\Danish.isl"
Name: "dutch"; MessagesFile: "compiler:Languages\Dutch.isl"
Name: "finnish"; MessagesFile: "compiler:Languages\Finnish.isl"
Name: "french"; MessagesFile: "compiler:Languages\French.isl"
Name: "german"; MessagesFile: "compiler:Languages\German.isl"
Name: "hebrew"; MessagesFile: "compiler:Languages\Hebrew.isl"
Name: "hungarian"; MessagesFile: "compiler:Languages\Hungarian.isl"
Name: "italian"; MessagesFile: "compiler:Languages\Italian.isl"
Name: "japanese"; MessagesFile: "compiler:Languages\Japanese.isl"
Name: "norwegian"; MessagesFile: "compiler:Languages\Norwegian.isl"
Name: "polish"; MessagesFile: "compiler:Languages\Polish.isl"
Name: "portuguese"; MessagesFile: "compiler:Languages\Portuguese.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "slovak"; MessagesFile: "compiler:Languages\Slovak.isl"
Name: "slovenian"; MessagesFile: "compiler:Languages\Slovenian.isl"
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[Tasks]
; Propose choice of these tasks to the user during setup. Put Flags: unchecked if you 
; want teh boxes to be unchecked by default. Put nothing if you want them checked. 
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: modifypath; Description: Add application directory to your environmental path (recommended)
Name: addextension; Description: Associate .masschroqML file extension with MassChroQ (recommended)


; Dirs
[Dirs]
Name: "{app}\"; Permissions: everyone-modify 

; Files to install
[Files]
Source: "C:\developpement\masschroq\win32\src\Release\masschroq.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\win32\src\Release\QtCore4.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\win32\src\Release\QtNetwork4.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\win32\src\Release\QtXml4.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\win32\src\Release\QtXmlPatterns4.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\doc\schema\masschroq-1.2.xsd"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\doc\logo\masschroq.ico"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\win32\masschroq_readme.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\developpement\masschroq\doc\examples\*.*"; DestDir: "{app}\examples\"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\developpement\masschroq\doc\manual\masschroq_manual.pdf"; DestDir: "{app}"; Flags: ignoreversion
;NOTE: Don't use "Flags: ignoreversion" on any shared system files

; Shortcuts to add 
[Icons]
; Start shortcuts
Name: "{group}\masschroq_readme.pdf"; Filename: "{app}\masschroq_readme.pdf"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{group}\examples"; Filename: "{app}\examples"
Name: "{group}\masschroq_manual.pdf"; Filename: "{app}\masschroq_manual.pdf"
; Desktop shortcuts 
Name: "{commondesktop}\masschroq_readme.pdf"; Filename: "{app}\masschroq_readme.pdf"; Tasks: desktopicon
Name: "{commondesktop}\masschroq_examples"; Filename: "{app}\examples"; Tasks: desktopicon

[Run]
Filename: "{app}\masschroq_readme.pdf"; Flags: postinstall shellexec skipifsilent; Description: "View the MassChroQ README file"


; Code to modify PATH env. variable. Uses file modpath.iss which you should provide 
[Code]
const
    ModPathName = 'modifypath';
    ModPathType = 'user';

function ModPathDir(): TArrayOfString;
begin
    setArrayLength(Result, 1)
    Result[0] := ExpandConstant('{app}');
end;
#include "modpath.iss"
