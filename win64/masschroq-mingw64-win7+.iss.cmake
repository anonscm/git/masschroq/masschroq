[Setup]
AppName=MassChroQ

; Set version number below
#define public version "${MASSCHROQ_VERSION}"
AppVersion={#version}

#define public arch "mingw64"
#define public platform "win7+"
#define sourceDir "C:\msys64\home\polipo\devel\masschroq"
#define cmake_build_dir "C:\msys64\home\polipo\devel\masschroq\build"

; Set version number below
AppVerName=MassChroQ version {#version}
DefaultDirName={pf}\masschroq
DefaultGroupName=masschroq
OutputDir="C:\msys64\home\polipo\devel\masschroq\win64"

; Set version number below
OutputBaseFilename=masschroq-{#arch}-{#platform}-v{#version}-setup

; Set version number below
OutputManifestFile=masschroq-{#arch}-{#platform}-v{#version}-setup-manifest.txt
ArchitecturesAllowed=x64
ArchitecturesInstallIn64BitMode=x64

LicenseFile="{#sourceDir}\COPYING"
AppCopyright="Copyright (C) 2009-2018 Benoit Valot, Edlira Nano, Olivier Langella"

AllowNoIcons=yes
AlwaysShowComponentsList=yes
AllowRootDirectory=no
AllowCancelDuringInstall=yes
AppComments="MassChroQ, mass chromatogram quantifier"
AppContact="Olivier Langella, research engineer at CNRS, France"
CloseApplications=yes
CreateUninstallRegKey=yes
DirExistsWarning=yes
WindowResizable=yes
;WizardImageFile="{#sourceDir}\images\splashscreen-innosetup.bmp"
WizardImageStretch=yes

[Dirs]
Name: "{app}"

[Files]
Source: "C:\xtpcpp-libdeps\*"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\masschroq\build\src\libmasschroq.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\libodsstream\build\src\libodsstream-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\libpappsomspp-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;
Source: "C:\msys64\home\polipo\devel\pappsomspp\build\src\pappsomspp\widget\libpappsomspp-widget-qt5.so"; DestDir: {app}; Flags: ignoreversion recursesubdirs;

Source: "{#sourceDir}\README"; DestDir: {app}; Flags: isreadme; Components: mcqComp
Source: "{#sourceDir}\COPYING"; DestDir: {app}; Flags: isreadme; Components: mcqComp

Source: "{#cmake_build_dir}\src\masschroq.exe"; DestDir: {app}; Components: mcqComp 
Source: "{#cmake_build_dir}\src\masschroq_gui.exe"; DestDir: {app}; Components: mcqComp
Source: "{#cmake_build_dir}\src\masschroq_studio.exe"; DestDir: {app}; Components: mcqComp

[Icons]
Name: "{group}\masschroq"; Filename: "{app}\masschroq_gui.exe"; WorkingDir: "{app}"
Name: "{group}\masschroq_studio"; Filename: "{app}\masschroq_studio.exe"; WorkingDir: "{app}"
Name: "{group}\Uninstall masschroq"; Filename: "{uninstallexe}"

[Types]
Name: "mcqType"; Description: "Full installation"

[Components]
Name: "mcqComp"; Description: "MassChroQ files and related documentation"; Types: mcqType

[Run]
;Filename: "{app}\README"; Description: "View the README file"; Flags: postinstall shellexec skipifsilent
Filename: "{app}\masschroq_gui.exe"; Description: "Launch MassChroQ GUI"; Flags: postinstall nowait unchecked
