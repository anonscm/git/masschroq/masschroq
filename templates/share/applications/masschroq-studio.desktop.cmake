[Desktop Entry]
Name=MassChroQ Studio ${MASSCHROQ_VERSION}
Categories=Education;Science;Math;
Comment=MassChroQ parameters editor
Exec=masschroq_studio %U
Icon=${CMAKE_INSTALL_PREFIX}/share/masschroq/masschroq.svg
Terminal=false
Type=Application
StartupNotify=true